-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 15, 2019 at 05:42 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce_pkk`
--

-- --------------------------------------------------------

--
-- Table structure for table `akunBank`
--

CREATE TABLE `akunBank` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idBank` int(11) NOT NULL,
  `noRek` varchar(20) NOT NULL,
  `pemilikRek` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `namaBank` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `namaBank`) VALUES
(1, 'PT. BANK DKI');

-- --------------------------------------------------------

--
-- Table structure for table `buktiPembayaran`
--

CREATE TABLE `buktiPembayaran` (
  `id` int(11) NOT NULL,
  `idPembayaran` int(11) NOT NULL,
  `idAkunBank` int(11) NOT NULL,
  `idBuyer` int(11) NOT NULL COMMENT 'idBuyer = idUser khusus level = 1 (buyer)',
  `gambar` varchar(50) NOT NULL COMMENT 'Foto kertas salinan transfer atau screenshot gambar jika transfer via mobile',
  `uploadDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `diskon`
--

CREATE TABLE `diskon` (
  `id` int(11) NOT NULL,
  `idSeller` int(11) NOT NULL COMMENT 'idSeller= idUser khusus level = 2 (seller)',
  `idProduk` int(11) NOT NULL,
  `persenDiskon` tinyint(3) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 = Active, 0 = Expired',
  `startDate` date NOT NULL,
  `expiryDate` date NOT NULL,
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gambarProduk`
--

CREATE TABLE `gambarProduk` (
  `id` int(11) NOT NULL,
  `idProduk` int(11) NOT NULL,
  `fileName` varchar(60) NOT NULL,
  `statusGambar` tinyint(1) NOT NULL COMMENT '1 utama, 0 pelengkap',
  `uploadDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gambarReview`
--

CREATE TABLE `gambarReview` (
  `id` int(11) NOT NULL,
  `idReview` int(11) NOT NULL,
  `filename` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `iklan`
--

CREATE TABLE `iklan` (
  `id` int(11) NOT NULL,
  `idAdmin` int(11) NOT NULL COMMENT 'idAdmin = idUser khusus level = 3 (admin sistem)',
  `title` varchar(50) NOT NULL,
  `keterangan` varchar(150) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `link` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `layout` tinyint(1) NOT NULL,
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jasaPengiriman`
--

CREATE TABLE `jasaPengiriman` (
  `id` int(11) NOT NULL,
  `namaJasa` varchar(35) NOT NULL,
  `jenisPengiriman` tinyint(1) NOT NULL COMMENT '1 = Regular, 2 = One Day'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `namaKategori` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id` int(11) NOT NULL,
  `idKota` int(11) NOT NULL,
  `namaKecamatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kelurahan`
--

CREATE TABLE `kelurahan` (
  `id` int(11) NOT NULL,
  `idKecamatan` int(11) NOT NULL,
  `namaKelurahan` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `id` int(11) NOT NULL,
  `namaKota` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kupon`
--

CREATE TABLE `kupon` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL COMMENT 'idUser yang memposting kupon',
  `judul` varchar(45) NOT NULL,
  `keterangan` varchar(300) NOT NULL,
  `kodeKupon` varchar(45) DEFAULT NULL,
  `startDate` datetime NOT NULL,
  `expiryDate` datetime NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 = Active, 0 = Expired',
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `listPembelian`
--

CREATE TABLE `listPembelian` (
  `id` int(11) NOT NULL,
  `idTransaksiPembelian` int(11) NOT NULL,
  `idTransaksiPengiriman` int(11) NOT NULL,
  `idProduk` int(11) NOT NULL,
  `quantity` smallint(5) NOT NULL,
  `hargaBerlaku` float NOT NULL,
  `idDiskonBerlaku` int(11) DEFAULT NULL,
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `merek`
--

CREATE TABLE `merek` (
  `id` int(11) NOT NULL,
  `namaMerek` varchar(45) NOT NULL,
  `createdBy` int(11) NOT NULL COMMENT 'createdBy = idUser',
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `metodePengirimanStore`
--

CREATE TABLE `metodePengirimanStore` (
  `id` int(11) NOT NULL,
  `idStore` int(11) NOT NULL,
  `idJasaPengiriman1` tinyint(1) NOT NULL,
  `idJasaPengiriman2` tinyint(1) DEFAULT NULL,
  `idJasaPengiriman3` tinyint(1) DEFAULT NULL,
  `idJasaPengiriman4` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notifikasi`
--

CREATE TABLE `notifikasi` (
  `id` int(11) NOT NULL,
  `idStore` int(11) NOT NULL,
  `idBuyer` int(11) NOT NULL COMMENT 'idBuyer = idUser khusus level = 1 (buyer)',
  `idProduk` int(11) NOT NULL,
  `pesan` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id` int(11) NOT NULL,
  `idTransaksiPembelian` int(11) NOT NULL,
  `kodeUnik` float NOT NULL,
  `totalPembayaran` float NOT NULL,
  `metodePembayaran` tinyint(1) NOT NULL COMMENT '1 = COD, 2 = Transfer',
  `idAkunBank` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 = Gagal/Dibatalkan, 1 = Belum terverifikasi, 2 = Terverifikasi',
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengirimanProduk`
--

CREATE TABLE `pengirimanProduk` (
  `id` int(11) NOT NULL,
  `idTransaksiPengiriman` int(11) NOT NULL,
  `noResi` varchar(30) DEFAULT NULL,
  `statusOneDay` tinyint(1) NOT NULL COMMENT '0 = Unactive, 1 = Belum dikirim, 2 = Pencarian kurir, 3 = Sedang dikirim, 4 = Pesanan diterima',
  `statusRegular` tinyint(1) NOT NULL COMMENT '0 = Unactive, 1 = Belum dikirim, 2 = Sedang dikirim, 3 = Sudah diterima',
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `idKategori` int(11) NOT NULL,
  `idSubkategori` int(11) NOT NULL,
  `idSeller` int(11) NOT NULL COMMENT 'idSeller = idUser khusus level = 2 (seller)',
  `idStore` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `harga` float NOT NULL,
  `keterangan` text NOT NULL,
  `kondisi` tinyint(1) NOT NULL COMMENT '1 = Baru, 0 = Bekas',
  `minPembelian` tinyint(3) NOT NULL DEFAULT '1',
  `beratProduk` smallint(6) DEFAULT NULL COMMENT 'Gram',
  `idMerek` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 = Active, 0 = Unactive',
  `uploadDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `idBuyer` int(11) NOT NULL COMMENT 'idBuyer = idUser khusus level = 1 (buyer)',
  `rating` tinyint(1) NOT NULL COMMENT '1, 2, 3, 4, atau 5',
  `keterangan` varchar(200) NOT NULL,
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `slide`
--

CREATE TABLE `slide` (
  `id` int(11) NOT NULL,
  `idAdmin` int(11) NOT NULL COMMENT 'idAdmin= idUser khusus level = 3 (admin sistem)',
  `title` varchar(50) NOT NULL,
  `keterangan` varchar(150) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `link` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 = Active, 0 = Unactive',
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stok`
--

CREATE TABLE `stok` (
  `id` int(11) NOT NULL,
  `idProduk` int(11) NOT NULL,
  `penambahanStok` smallint(6) NOT NULL,
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `id` int(11) NOT NULL,
  `idSeller` int(11) NOT NULL COMMENT 'idSeller = idUser khusus level = 2 (seller)',
  `namaStore` varchar(50) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `slogan` varchar(60) NOT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `alamat` varchar(150) NOT NULL,
  `idKecamatan` int(11) NOT NULL,
  `idKelurahan` int(11) NOT NULL,
  `kodePos` varchar(5) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 = Active, 0 = Unactive',
  `levelStore` tinyint(1) NOT NULL COMMENT '1 = Official Store (resmi dari koperasi), 2 = Unofficial (seller biasa)',
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subkategori`
--

CREATE TABLE `subkategori` (
  `id` int(11) NOT NULL,
  `idKategori` int(11) NOT NULL,
  `namaSubkategori` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaksiPembelian`
--

CREATE TABLE `transaksiPembelian` (
  `id` int(11) NOT NULL,
  `idBuyer` int(11) NOT NULL COMMENT 'idBuyer = idUser khusus level = 1 (buyer)',
  `idKupon` int(11) DEFAULT NULL,
  `totalHargaProduk` float NOT NULL DEFAULT '0',
  `totalHargaKirim` float NOT NULL DEFAULT '0',
  `totalHargaAkhir` float NOT NULL DEFAULT '0',
  `statusTransaksi` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 = Failed/Dibatalkan, 1 = Sedang Diproses, 2 = Success',
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaksiPengiriman`
--

CREATE TABLE `transaksiPengiriman` (
  `id` int(11) NOT NULL,
  `idTransaksiPembelian` int(11) NOT NULL,
  `idStore` int(11) NOT NULL,
  `idJasaPengiriman` int(11) NOT NULL,
  `biayaPengiriman` float NOT NULL,
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `email` varchar(30) NOT NULL,
  `noHp` varchar(13) NOT NULL,
  `foto` varchar(60) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 active, 0 unactive',
  `level` tinyint(1) NOT NULL COMMENT '1 = Buyer, 2 = Seller, 3 = Admin sistem, 4 = Superadmin',
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userAlamat`
--

CREATE TABLE `userAlamat` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idKecamatan` int(11) NOT NULL,
  `idKelurahan` int(11) NOT NULL,
  `kodePos` varchar(5) NOT NULL,
  `namaPenerima` varchar(60) NOT NULL COMMENT 'khusus buyer',
  `hpPenerima` varchar(13) NOT NULL,
  `statusAlamat` tinyint(1) DEFAULT NULL COMMENT '1 = Utama, 0 = Lainnya',
  `ketAlamat` varchar(20) DEFAULT NULL COMMENT 'ex : kos, rumah, kantor',
  `level` tinyint(1) NOT NULL COMMENT '1 = Alamat Biasa, 0 = Alamat Maps',
  `lat` decimal(10,8) DEFAULT NULL,
  `long` decimal(11,8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `idProduk` int(11) NOT NULL,
  `idBuyer` int(11) NOT NULL COMMENT 'idBuyer = idUser khusus level = 1 (buyer)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akunBank`
--
ALTER TABLE `akunBank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buktiPembayaran`
--
ALTER TABLE `buktiPembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diskon`
--
ALTER TABLE `diskon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gambarProduk`
--
ALTER TABLE `gambarProduk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gambarReview`
--
ALTER TABLE `gambarReview`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iklan`
--
ALTER TABLE `iklan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jasaPengiriman`
--
ALTER TABLE `jasaPengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kupon`
--
ALTER TABLE `kupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `listPembelian`
--
ALTER TABLE `listPembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merek`
--
ALTER TABLE `merek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metodePengirimanStore`
--
ALTER TABLE `metodePengirimanStore`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifikasi`
--
ALTER TABLE `notifikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengirimanProduk`
--
ALTER TABLE `pengirimanProduk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stok`
--
ALTER TABLE `stok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subkategori`
--
ALTER TABLE `subkategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksiPembelian`
--
ALTER TABLE `transaksiPembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksiPengiriman`
--
ALTER TABLE `transaksiPengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userAlamat`
--
ALTER TABLE `userAlamat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akunBank`
--
ALTER TABLE `akunBank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `buktiPembayaran`
--
ALTER TABLE `buktiPembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `diskon`
--
ALTER TABLE `diskon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gambarProduk`
--
ALTER TABLE `gambarProduk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gambarReview`
--
ALTER TABLE `gambarReview`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `iklan`
--
ALTER TABLE `iklan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jasaPengiriman`
--
ALTER TABLE `jasaPengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kelurahan`
--
ALTER TABLE `kelurahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kupon`
--
ALTER TABLE `kupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `listPembelian`
--
ALTER TABLE `listPembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `merek`
--
ALTER TABLE `merek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `metodePengirimanStore`
--
ALTER TABLE `metodePengirimanStore`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notifikasi`
--
ALTER TABLE `notifikasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengirimanProduk`
--
ALTER TABLE `pengirimanProduk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `slide`
--
ALTER TABLE `slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stok`
--
ALTER TABLE `stok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subkategori`
--
ALTER TABLE `subkategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaksiPembelian`
--
ALTER TABLE `transaksiPembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaksiPengiriman`
--
ALTER TABLE `transaksiPengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userAlamat`
--
ALTER TABLE `userAlamat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
