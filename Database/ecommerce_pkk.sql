-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 01 Sep 2019 pada 11.29
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce_pkk`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `akunbank`
--

CREATE TABLE `akunbank` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idBank` int(11) NOT NULL,
  `noRek` varchar(20) NOT NULL,
  `pemilikRek` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `namaBank` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bank`
--

INSERT INTO `bank` (`id`, `namaBank`) VALUES
(1, 'PT. BANK DKI');

-- --------------------------------------------------------

--
-- Struktur dari tabel `buktipembayaran`
--

CREATE TABLE `buktipembayaran` (
  `id` int(11) NOT NULL,
  `idPembayaran` int(11) NOT NULL,
  `idAkunBank` int(11) NOT NULL,
  `idMember` int(11) NOT NULL COMMENT 'idBuyer = idUser khusus level = 1 (buyer)',
  `gambar` varchar(50) NOT NULL COMMENT 'Foto kertas salinan transfer atau screenshot gambar jika transfer via mobile',
  `uploadDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `detailtransaksi`
--

CREATE TABLE `detailtransaksi` (
  `id` int(11) NOT NULL,
  `idTransaksi` int(11) NOT NULL,
  `idProduk` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `hargaSatuan` int(255) NOT NULL,
  `hargaTotal` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detailtransaksi`
--

INSERT INTO `detailtransaksi` (`id`, `idTransaksi`, `idProduk`, `qty`, `hargaSatuan`, `hargaTotal`) VALUES
(1, 1, 9, 1, 85000, 85000),
(2, 1, 6, 2, 15001, 30002),
(3, 2, 5, 2, 20002, 40004),
(4, 2, 9, 1, 85000, 85000),
(5, 3, 4, 1, 18000, 18000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `diskon`
--

CREATE TABLE `diskon` (
  `id` int(11) NOT NULL,
  `idMember` int(11) NOT NULL COMMENT 'idSeller= idUser khusus level = 2 (seller)',
  `idProduk` int(11) NOT NULL,
  `persenDiskon` tinyint(3) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 = Active, 0 = Expired',
  `startDate` date NOT NULL,
  `expiryDate` date NOT NULL,
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `diskon`
--

INSERT INTO `diskon` (`id`, `idMember`, `idProduk`, `persenDiskon`, `status`, `startDate`, `expiryDate`, `createdDate`) VALUES
(1, 1, 4, 10, 1, '2019-08-13', '2019-08-31', '2019-08-13 11:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gambarproduk`
--

CREATE TABLE `gambarproduk` (
  `id` int(11) NOT NULL,
  `idProduk` int(11) NOT NULL,
  `fileName` varchar(60) NOT NULL,
  `statusGambar` tinyint(1) NOT NULL COMMENT '1 utama, 0 pelengkap',
  `uploadDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gambarproduk`
--

INSERT INTO `gambarproduk` (`id`, `idProduk`, `fileName`, `statusGambar`, `uploadDate`) VALUES
(4, 4, 'produk-dummy-alat-rumah-tangga.jpg', 1, '2019-08-12 10:00:00'),
(5, 5, 'produk-dummy-alat-rumah-tangga2.jpg', 1, '2019-08-12 10:00:00'),
(6, 6, 'produk-dummy-alat-rumah-tangga3.jpg', 1, '2019-08-12 10:00:00'),
(7, 7, 'produk-dummy-alat-rumah-tangga4.jpg', 1, '2019-08-12 10:00:00'),
(8, 8, 'produk-dummy-alat-rumah-tangga5.jpg', 1, '2019-08-12 10:00:00'),
(9, 9, 'produk-dummy-alat-rumah-tangga6.jpg', 1, '2019-08-12 10:00:00'),
(10, 10, 'produk-dummy-alat-rumah-tangga7.jpg', 1, '2019-08-12 10:00:00'),
(11, 11, 'produk-dummy-alat-rumah-tangga8.jpg', 1, '2019-08-12 10:00:00'),
(12, 12, 'produk-dummy-bahan-pokok.jpg', 1, '2019-08-12 10:00:00'),
(13, 13, 'produk-dummy-bahan-pokok3.jpg', 1, '2019-08-12 10:00:00'),
(14, 14, 'produk-dummy-bahan-pokok2.jpg', 1, '2019-08-12 10:00:00'),
(15, 15, 'produk-dummy-bahan-pokok4.jpg', 1, '2019-08-12 10:00:00'),
(16, 16, 'produk-dummy-bahan-pokok5.jpg', 1, '2019-08-12 10:00:00'),
(17, 17, 'produk-dummy-bahan-pokok6.jpg', 1, '2019-08-12 10:00:00'),
(18, 18, 'produk-dummy-bahan-pokok7.jpg', 1, '2019-08-12 10:00:00'),
(19, 19, 'produk-dummy-bahan-pokok8.jpg', 1, '2019-08-12 10:00:00'),
(20, 20, 'produk-dummy-makanan.jpg', 1, '2019-08-12 10:00:00'),
(21, 21, 'produk-dummy-makanan2.jpg', 1, '2019-08-12 10:00:00'),
(22, 22, 'produk-dummy-minuman.jpg', 1, '2019-08-12 10:00:00'),
(23, 23, 'produk-dummy-minuman2.jpg', 1, '2019-08-12 10:00:00'),
(24, 24, 'produk-dummy-minuman3.jpg', 1, '2019-08-12 10:00:00'),
(25, 25, 'produk-dummy-minuman4.jpg', 1, '2019-08-12 10:00:00'),
(26, 5, 'produk-dummy-alat-rumah-tangga2_2.jpg', 1, '2019-08-22 13:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gambarreview`
--

CREATE TABLE `gambarreview` (
  `id` int(11) NOT NULL,
  `idReview` int(11) NOT NULL,
  `filename` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `iklan`
--

CREATE TABLE `iklan` (
  `id` int(11) NOT NULL,
  `idAdmin` int(11) NOT NULL COMMENT 'idAdmin = idUser khusus level = 3 (admin sistem)',
  `title` varchar(50) NOT NULL,
  `keterangan` varchar(150) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `link` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `layout` tinyint(1) NOT NULL,
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jasapengiriman`
--

CREATE TABLE `jasapengiriman` (
  `id` int(11) NOT NULL,
  `namaJasa` varchar(35) NOT NULL,
  `jenisPengiriman` tinyint(1) NOT NULL COMMENT '1 = Regular, 2 = One Day'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `namaKategori` varchar(30) NOT NULL,
  `gambar` varchar(150) NOT NULL COMMENT 'untuk gambar kategori index',
  `slug` varchar(150) NOT NULL COMMENT 'url halaman kategori'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `namaKategori`, `gambar`, `slug`) VALUES
(1, 'Alat Rumah Tangga', 'alat-rumah-tangga.jpg', 'alat-rumah-tangga'),
(2, 'Bahan Pokok', 'bahan-pokok.jpg', 'bahan-pokok'),
(3, 'Minuman', 'minuman.jpg', 'minuman'),
(4, 'Makanan', 'makanan.jpg', 'makanan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id` int(11) NOT NULL,
  `idKota` int(11) NOT NULL,
  `namaKecamatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kecamatan`
--

INSERT INTO `kecamatan` (`id`, `idKota`, `namaKecamatan`) VALUES
(1, 1, 'Kramat Jati');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelurahan`
--

CREATE TABLE `kelurahan` (
  `id` int(11) NOT NULL,
  `idKecamatan` int(11) NOT NULL,
  `namaKelurahan` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelurahan`
--

INSERT INTO `kelurahan` (`id`, `idKecamatan`, `namaKelurahan`) VALUES
(1, 1, 'Batu Ampar');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kota`
--

CREATE TABLE `kota` (
  `id` int(11) NOT NULL,
  `namaKota` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kota`
--

INSERT INTO `kota` (`id`, `namaKota`) VALUES
(1, 'Jakarta Timur');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kupon`
--

CREATE TABLE `kupon` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL COMMENT 'idUser yang memposting kupon',
  `judul` varchar(45) NOT NULL,
  `keterangan` varchar(300) NOT NULL,
  `kodeKupon` varchar(45) DEFAULT NULL,
  `startDate` datetime NOT NULL,
  `expiryDate` datetime NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 = Active, 0 = Expired',
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `listpembelian`
--

CREATE TABLE `listpembelian` (
  `id` int(11) NOT NULL,
  `idTransaksiPembelian` int(11) NOT NULL,
  `idTransaksiPengiriman` int(11) NOT NULL,
  `idProduk` int(11) NOT NULL,
  `quantity` smallint(5) NOT NULL,
  `hargaBerlaku` float NOT NULL,
  `idDiskonBerlaku` int(11) DEFAULT NULL,
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `merek`
--

CREATE TABLE `merek` (
  `id` int(11) NOT NULL,
  `namaMerek` varchar(45) NOT NULL,
  `createdBy` int(11) NOT NULL COMMENT 'createdBy = idUser',
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `merek`
--

INSERT INTO `merek` (`id`, `namaMerek`, `createdBy`, `createdDate`) VALUES
(1, 'Katulistiwa', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `metodepengirimanstore`
--

CREATE TABLE `metodepengirimanstore` (
  `id` int(11) NOT NULL,
  `idStore` int(11) NOT NULL,
  `idJasaPengiriman1` tinyint(1) NOT NULL,
  `idJasaPengiriman2` tinyint(1) DEFAULT NULL,
  `idJasaPengiriman3` tinyint(1) DEFAULT NULL,
  `idJasaPengiriman4` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `notifikasi`
--

CREATE TABLE `notifikasi` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idBuyer` int(11) NOT NULL COMMENT 'idBuyer = idUser khusus level = 1 (buyer)',
  `idProduk` int(11) NOT NULL,
  `pesan` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id` int(11) NOT NULL,
  `idTransaksiPembelian` int(11) NOT NULL,
  `kodeUnik` float NOT NULL,
  `totalPembayaran` float NOT NULL,
  `metodePembayaran` tinyint(1) NOT NULL COMMENT '1 = COD, 2 = Transfer',
  `idAkunBank` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 = Gagal/Dibatalkan, 1 = Belum terverifikasi, 2 = Terverifikasi',
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengirimanproduk`
--

CREATE TABLE `pengirimanproduk` (
  `id` int(11) NOT NULL,
  `idTransaksiPengiriman` int(11) NOT NULL,
  `noResi` varchar(30) DEFAULT NULL,
  `statusOneDay` tinyint(1) NOT NULL COMMENT '0 = Unactive, 1 = Belum dikirim, 2 = Pencarian kurir, 3 = Sedang dikirim, 4 = Pesanan diterima',
  `statusRegular` tinyint(1) NOT NULL COMMENT '0 = Unactive, 1 = Belum dikirim, 2 = Sedang dikirim, 3 = Sudah diterima',
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `idKategori` int(11) NOT NULL,
  `idSubkategori` int(11) NOT NULL,
  `idMember` int(11) NOT NULL COMMENT 'idSeller = idUser khusus level = 2 (seller)',
  `idStore` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `harga` float NOT NULL,
  `keterangan` text NOT NULL,
  `kondisi` tinyint(1) NOT NULL COMMENT '1 = Baru, 0 = Bekas',
  `minPembelian` tinyint(3) NOT NULL DEFAULT '1',
  `beratProduk` smallint(6) DEFAULT NULL COMMENT 'Gram',
  `idMerek` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 = Active, 0 = Unactive',
  `uploadDate` datetime NOT NULL,
  `sku` varchar(35) DEFAULT NULL,
  `stok` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(150) NOT NULL COMMENT 'url detail produk'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`id`, `idKategori`, `idSubkategori`, `idMember`, `idStore`, `judul`, `harga`, `keterangan`, `kondisi`, `minPembelian`, `beratProduk`, `idMerek`, `status`, `uploadDate`, `sku`, `stok`, `slug`) VALUES
(4, 1, 1, 1, 1, 'Mama Lemon', 20000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 500, 1, 1, '2019-08-11 20:00:00', 'mama-lemon1', 100, 'mama-lemon'),
(5, 1, 1, 1, 1, 'Sunlight', 20002, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 500, 1, 1, '2019-08-11 20:00:00', 'sunlight1', 100, 'sunlight'),
(6, 1, 1, 1, 1, 'Super Pell Fresh Apple', 15001, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 500, 1, 1, '2019-08-11 20:00:00', 'super-pell-fresh-apple1', 100, 'super-pell-fresh-apple'),
(7, 1, 1, 1, 1, 'Super Pell Gold', 15001, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 500, 1, 1, '2019-08-11 20:00:00', 'super-pell-gold', 100, 'super-pell-gold'),
(8, 1, 1, 1, 1, 'Sapu Ijuk', 30001, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 500, 1, 1, '2019-08-11 20:00:00', 'sapu-ijuk1', 100, 'sapu-ijuk'),
(9, 1, 1, 1, 1, 'Jemuran Besi Kecil', 85000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 500, 1, 1, '2019-08-11 20:00:00', 'jemuran-besi-kecil1', 100, 'jemuran-besi-kecil'),
(10, 1, 1, 1, 1, 'Pel Peras Otomatis', 250000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 1000, 1, 1, '2019-08-11 20:00:00', 'pel-peras-otomatis1', 100, 'pel-peras-otomatis'),
(11, 1, 1, 1, 1, 'Sarung Tangan Cuci Piring', 100000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 1000, 1, 1, '2019-08-11 20:00:00', 'sarung-tangan-cuci-piring1', 100, 'sarung-tangan-cuci-piring'),
(12, 2, 2, 2, 2, 'Beras Rojolele 10kg', 250000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 10000, 1, 1, '2019-08-11 20:00:00', 'beras-rojolele-10kg1', 100, 'beras-rojolele-10kg'),
(13, 2, 2, 2, 2, 'Beras Pandan Wangi 10kg', 250000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 10000, 1, 1, '2019-08-11 20:00:00', 'beras-pandan-wangi-10kg1', 100, 'beras-pandan-wangi-10kg'),
(14, 2, 2, 2, 2, 'Gulaku Premium 1kg', 15000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 1000, 1, 1, '2019-08-11 20:00:00', 'gulaku-premium-1kg1', 100, 'gulaku-premium-1kg'),
(15, 2, 2, 2, 2, 'Gulaku Tebu Alami 100% 1kg', 15000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 1000, 1, 1, '2019-08-11 20:00:00', 'gulaku-tebu-alami-1kg1', 100, 'gulaku-tebu-alami-1kg'),
(16, 2, 2, 2, 2, 'Tepung Terigu Segitiga Biru 1kg', 15000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 1000, 1, 1, '2019-08-11 20:00:00', 'tepung-terigu-segitiga-biru-1kg1', 100, 'tepung-terigu-segitiga-biru-1kg'),
(17, 2, 2, 2, 2, 'Tepung Terigu Sania 1kg', 15000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 1000, 1, 1, '0000-00-00 00:00:00', 'tepung-terigu-sania-1kg1', 100, 'tepung-terigu-sania-1kg'),
(18, 2, 2, 2, 2, 'Minyak Goreng Sunco 1kg', 20000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 1000, 1, 1, '2019-08-11 20:00:00', 'minyak-goreng-sunco-1kg1', 100, 'minyak-goreng-sunco-1kg'),
(19, 2, 2, 2, 2, 'Minyak Goreng Bimoli 1kg', 20000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 1000, 1, 1, '2019-08-11 20:00:00', 'minyak-goreng-bimoli-1kg1', 100, 'minyak-goreng-bimoli-1kg'),
(20, 4, 4, 4, 4, 'Indomie Soto 40pcs', 85000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 5000, 1, 1, '2019-08-11 20:00:00', 'indomie-soto-40pcs1', 100, 'indomie-soto-40pcs'),
(21, 4, 4, 4, 4, 'Indomie Goreng 40pcs', 85000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 5000, 1, 1, '2019-08-11 20:00:00', 'indomie-goreng-40pcs1', 100, 'indomie-goreng-40pcs'),
(22, 3, 3, 3, 3, 'Ultra Milk Fullcream 1000ml', 20000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 1000, 1, 1, '2019-08-11 20:00:00', 'ultra-milk-fullcream-1000ml1', 100, 'ultra-milk-fullcream-1000ml'),
(23, 3, 3, 3, 3, 'Ultra Milk Coklat 1000ml', 20000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 1000, 1, 1, '2019-08-11 20:00:00', 'ultra-milk-coklat-1000ml1', 100, 'ultra-milk-coklat-1000ml'),
(24, 3, 3, 3, 3, 'Frisian Flag Kental Manis', 5000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 400, 1, 1, '2019-08-11 20:00:00', 'frisian-flag-kental-manis1', 100, 'frisian-flag-kental-manis'),
(25, 3, 3, 3, 3, 'Dancow Fortigro Coklat 800g', 20000, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 1, 1000, 1, 1, '2019-08-11 20:00:00', 'dancow-fortigro-coklat-800g1', 100, 'dancow-fortigro-coklat-800g');

-- --------------------------------------------------------

--
-- Struktur dari tabel `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `idMember` int(11) NOT NULL COMMENT 'idBuyer = idUser khusus level = 1 (buyer)',
  `idProduk` int(11) NOT NULL,
  `rating` tinyint(1) NOT NULL COMMENT '1, 2, 3, 4, atau 5',
  `keterangan` varchar(200) NOT NULL,
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `review`
--

INSERT INTO `review` (`id`, `idMember`, `idProduk`, `rating`, `keterangan`, `createdDate`) VALUES
(1, 1, 4, 5, 'produk original, rekomended seller!', '2019-07-11 11:00:00'),
(2, 1, 4, 5, 'produk original, rekomended seller!', '2019-07-11 11:00:00'),
(3, 1, 4, 4, 'produk nya bagus makasih gan!', '2019-07-11 11:00:00'),
(4, 1, 5, 1, 'produk original', '2019-08-13 11:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `slide`
--

CREATE TABLE `slide` (
  `id` int(11) NOT NULL,
  `idAdmin` int(11) NOT NULL COMMENT 'idAdmin= idUser khusus level = 3 (admin sistem)',
  `title` varchar(50) NOT NULL,
  `keterangan` varchar(150) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `link` varchar(100) NOT NULL,
  `kategori` tinyint(1) NOT NULL COMMENT '1=slider utama, 2= slider kecil',
  `status` tinyint(1) NOT NULL COMMENT '1 = Active, 0 = Unactive',
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `slide`
--

INSERT INTO `slide` (`id`, `idAdmin`, `title`, `keterangan`, `gambar`, `link`, `kategori`, `status`, `createdDate`) VALUES
(1, 1, 'slider 1', 'slider 1', 'test1.png', '#', 1, 1, '2019-08-22 00:00:00'),
(2, 2, 'slider 2', 'slider 2', 'test2.png', '#', 1, 1, '2019-08-22 00:00:00'),
(3, 3, 'slider 3', 'slider 3', 'test3.png', '#', 1, 1, '2019-08-22 00:00:00'),
(4, 1, 'swipe slide 1', 'swipe slide 1', 'testswipe1.png', '#', 2, 1, '2019-08-22 00:00:00'),
(5, 1, 'swipe slide 2', 'swipe slide 2', 'testswipe2.png', '#', 2, 1, '2019-08-22 00:00:00'),
(6, 1, 'swipe slide 3', 'swipe slide 3', 'testswipe3.png', '#', 2, 1, '2019-08-22 00:00:00'),
(7, 1, 'swipe slide 4', 'swipe slide 4', 'testswipe4.png', '#', 2, 1, '2019-08-22 00:00:00'),
(8, 1, 'swipe slide 1', 'swipe slide 1', 'testswipe1.png', '#', 2, 1, '2019-08-22 00:00:00'),
(9, 1, 'swipe slide 2', 'swipe slide 2', 'testswipe2.png', '#', 2, 1, '2019-08-22 00:00:00'),
(10, 1, 'swipe slide 3', 'swipe slide 3', 'testswipe3.png', '#', 2, 1, '2019-08-22 00:00:00'),
(11, 1, 'swipe slide 4', 'swipe slide 4', 'testswipe4.png', '#', 2, 1, '2019-08-22 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `store`
--

CREATE TABLE `store` (
  `id` int(11) NOT NULL,
  `idMember` int(11) NOT NULL COMMENT 'idSeller = idUser khusus level = 2 (seller)',
  `namaStore` varchar(50) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `slogan` varchar(60) NOT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `alamat` varchar(150) NOT NULL,
  `idKota` int(11) NOT NULL,
  `idKecamatan` int(11) NOT NULL,
  `idKelurahan` int(11) NOT NULL,
  `kodePos` varchar(5) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 = Active, 0 = Unactive',
  `levelStore` tinyint(1) NOT NULL COMMENT '1 = Official Store (resmi dari koperasi), 2 = Unofficial (seller biasa)',
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `store`
--

INSERT INTO `store` (`id`, `idMember`, `namaStore`, `logo`, `slogan`, `deskripsi`, `alamat`, `idKota`, `idKecamatan`, `idKelurahan`, `kodePos`, `status`, `levelStore`, `createdDate`) VALUES
(1, 1, 'Koperasi Teratai', 'gosend.jpg', 'ga maju ya ga abadi', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a', 'Jl. Raya Condet No.69', 1, 1, 1, '13570', 1, 1, '2019-08-17 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `subkategori`
--

CREATE TABLE `subkategori` (
  `id` int(11) NOT NULL,
  `idKategori` int(11) NOT NULL,
  `namaSubkategori` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `subkategori`
--

INSERT INTO `subkategori` (`id`, `idKategori`, `namaSubkategori`) VALUES
(1, 1, 'Makanan'),
(2, 1, 'Minuman'),
(3, 1, 'Sayur'),
(4, 1, 'Bahan Pokok'),
(5, 2, 'Baju');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tag_list`
--

CREATE TABLE `tag_list` (
  `id` int(11) NOT NULL,
  `tag` varchar(30) DEFAULT NULL,
  `id_produk` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `nomorTransaksi` varchar(128) NOT NULL,
  `tanggalTransaksi` date NOT NULL,
  `nama` varchar(128) NOT NULL,
  `noHP` varchar(15) NOT NULL,
  `alamatTujuan` text NOT NULL,
  `totalTransfer` int(11) NOT NULL,
  `buktiTransfer` varchar(128) NOT NULL,
  `uploadDateTF` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '1=pembayaran berhasil, 0=proses verifikasi'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id`, `nomorTransaksi`, `tanggalTransaksi`, `nama`, `noHP`, `alamatTujuan`, `totalTransfer`, `buktiTransfer`, `uploadDateTF`, `status`) VALUES
(1, 'PKK000241', '0000-00-00', 'Ahmad Faqih Al Fahmy', '089689885579', 'Jl. H. Latif No. 42L Batu Ampar, Kramat Jati, Jakarta Timur. 13810', 115297, '', '0000-00-00 00:00:00', 0),
(2, 'PKK355832', '0000-00-00', 'Ahmad Faqih Al Fahmy', '089689885579', 'Jl. H. Latif No. 42L Batu Ampar, Kramat Jati, Jakarta Timur. 13810', 125695, 'bukti_transfer_PKK355832.jpeg', '2019-09-01 00:00:00', 0),
(3, 'PKK367537', '0000-00-00', 'Ahmad Faqih Al Fahmy', '089689885579', 'Jl. H. Latif No. 42L Batu Ampar, Kramat Jati, Jakarta Timur. 13810', 18274, 'bukti_transfer_PKK3675371.jpg', '2019-09-01 00:00:00', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksipembelian`
--

CREATE TABLE `transaksipembelian` (
  `id` int(11) NOT NULL,
  `idMember` int(11) NOT NULL COMMENT 'idBuyer = idUser khusus level = 1 (buyer)',
  `idKupon` int(11) DEFAULT NULL,
  `totalHargaProduk` float NOT NULL DEFAULT '0',
  `totalHargaKirim` float NOT NULL DEFAULT '0',
  `totalHargaAkhir` float NOT NULL DEFAULT '0',
  `statusTransaksi` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 = Failed/Dibatalkan, 1 = Sedang Diproses, 2 = Success',
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksipengiriman`
--

CREATE TABLE `transaksipengiriman` (
  `id` int(11) NOT NULL,
  `idTransaksiPembelian` int(11) NOT NULL,
  `idStore` int(11) NOT NULL,
  `idJasaPengiriman` int(11) NOT NULL,
  `biayaPengiriman` float NOT NULL,
  `createdDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `email` varchar(30) NOT NULL,
  `noHp` varchar(13) NOT NULL,
  `foto` varchar(60) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 active, 0 unactive',
  `level` tinyint(1) NOT NULL COMMENT '1 = Member, 3 = Admin sistem, 4 = Superadmin',
  `createdDate` datetime NOT NULL,
  `token` varchar(6) NOT NULL COMMENT 'token untuk verifikasi aktifasi akun'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `email`, `noHp`, `foto`, `status`, `level`, `createdDate`, `token`) VALUES
(1, 'faqih', 'faqih', '', 'faqih@faqih.com', '081314718374', 'faqih.jpg', 1, 1, '2019-08-17 15:00:00', 'YtRs');

-- --------------------------------------------------------

--
-- Struktur dari tabel `useralamat`
--

CREATE TABLE `useralamat` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idKota` int(11) NOT NULL,
  `idKecamatan` int(11) NOT NULL,
  `idKelurahan` int(11) NOT NULL,
  `kodePos` varchar(5) NOT NULL,
  `detailAlamat` text NOT NULL,
  `namaPenerima` varchar(60) NOT NULL COMMENT 'khusus buyer',
  `hpPenerima` varchar(13) NOT NULL,
  `statusAlamat` tinyint(1) DEFAULT NULL COMMENT '1 = Utama, 0 = Lainnya',
  `ketAlamat` varchar(20) DEFAULT NULL COMMENT 'ex : kos, rumah, kantor',
  `level` tinyint(1) NOT NULL COMMENT '1 = Alamat Biasa, 0 = Alamat Maps',
  `lat` decimal(10,8) DEFAULT NULL,
  `long` decimal(11,8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `useralamat`
--

INSERT INTO `useralamat` (`id`, `idUser`, `idKota`, `idKecamatan`, `idKelurahan`, `kodePos`, `detailAlamat`, `namaPenerima`, `hpPenerima`, `statusAlamat`, `ketAlamat`, `level`, `lat`, `long`) VALUES
(1, 1, 1, 1, 1, '13810', 'Jl. H. Latif No. 42L', 'Ahmad Faqih Al Fahmy', '089689885579', 1, 'rumah', 1, NULL, NULL),
(2, 1, 1, 1, 1, '13520', 'Jl. Taman Mini Pintu 2 Atas No. 69', 'Ahmad Faqih Al Fahmy', '089689885579', 0, 'kos', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `idProduk` int(11) NOT NULL,
  `idMember` int(11) NOT NULL COMMENT 'idBuyer = idUser khusus level = 1 (buyer)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `akunbank`
--
ALTER TABLE `akunbank`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `buktipembayaran`
--
ALTER TABLE `buktipembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `detailtransaksi`
--
ALTER TABLE `detailtransaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `diskon`
--
ALTER TABLE `diskon`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `gambarproduk`
--
ALTER TABLE `gambarproduk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `gambarreview`
--
ALTER TABLE `gambarreview`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `iklan`
--
ALTER TABLE `iklan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `jasapengiriman`
--
ALTER TABLE `jasapengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kupon`
--
ALTER TABLE `kupon`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `listpembelian`
--
ALTER TABLE `listpembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `merek`
--
ALTER TABLE `merek`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `metodepengirimanstore`
--
ALTER TABLE `metodepengirimanstore`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `notifikasi`
--
ALTER TABLE `notifikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pengirimanproduk`
--
ALTER TABLE `pengirimanproduk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `subkategori`
--
ALTER TABLE `subkategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tag_list`
--
ALTER TABLE `tag_list`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaksipembelian`
--
ALTER TABLE `transaksipembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaksipengiriman`
--
ALTER TABLE `transaksipengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `useralamat`
--
ALTER TABLE `useralamat`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `akunbank`
--
ALTER TABLE `akunbank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `buktipembayaran`
--
ALTER TABLE `buktipembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `detailtransaksi`
--
ALTER TABLE `detailtransaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `diskon`
--
ALTER TABLE `diskon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `gambarproduk`
--
ALTER TABLE `gambarproduk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `gambarreview`
--
ALTER TABLE `gambarreview`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `iklan`
--
ALTER TABLE `iklan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `jasapengiriman`
--
ALTER TABLE `jasapengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `kelurahan`
--
ALTER TABLE `kelurahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `kota`
--
ALTER TABLE `kota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `kupon`
--
ALTER TABLE `kupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `listpembelian`
--
ALTER TABLE `listpembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `merek`
--
ALTER TABLE `merek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `metodepengirimanstore`
--
ALTER TABLE `metodepengirimanstore`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `notifikasi`
--
ALTER TABLE `notifikasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pengirimanproduk`
--
ALTER TABLE `pengirimanproduk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT untuk tabel `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `slide`
--
ALTER TABLE `slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `store`
--
ALTER TABLE `store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `subkategori`
--
ALTER TABLE `subkategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tag_list`
--
ALTER TABLE `tag_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `transaksipembelian`
--
ALTER TABLE `transaksipembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `transaksipengiriman`
--
ALTER TABLE `transaksipengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `useralamat`
--
ALTER TABLE `useralamat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
