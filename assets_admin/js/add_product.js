var num = 0;


$(document).ready(function() {
    
    kategori();
    pilihKategori();
    merek();
    
    // Image
    document.getElementById('pro-image').addEventListener('change', readImage, false);
    
    $( ".preview-images-zone" ).sortable();
    
    $(document).on('click', '.image-cancel', function() {
        let no = $(this).data('no');
        $(".preview-image.preview-show-"+no).remove();
    });
});

$('#add').submit(function (e) { 
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: urlApi+'ProdukController/in',
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        dataType: "json",
        beforeSend : function () {
          $("#btnJualBarang").attr("disabled", true);  
          $("#loadingJual").show();  
        },
        success: function (response) {
            $("#btnJualBarang").attr("disabled", false);  
            $("#loadingJual").hide();
            clearInput();  
        }
    });

    setTimeout(() => {
        $("#btnJualBarang").attr("disabled", false);  
        $("#loadingJual").hide();
    }, 14000);
});


function readImage() {
    $('.preview-images-zone').html('');
    if (window.File && window.FileList && window.FileReader) {
        var files = event.target.files; //FileList object
        var output = $(".preview-images-zone");
        
        for (let i = 0; i < files.length; i++) {
            var file = files[i];
            if (!file.type.match('image')) continue;
            
            var picReader = new FileReader();
            
            picReader.addEventListener('load', function (event) {
                var picFile = event.target;
                var html =  '<div class="preview-image preview-show-' + num + '">' +
                // '<div class="image-cancel" data-no="' + num + '">x</div>' +
                '<div class="image-zone"><img id="pro-img-' + num + '" src="' + picFile.result + '"></div>' +
                // '<div class="tools-edit-image"><a href="javascript:void(0)" data-no="' + num + '" class="btn btn-light btn-edit-image">edit</a></div>' +
                '</div>';
                
                output.append(html);
                num = num + 1;
            });
            
            picReader.readAsDataURL(file);
        }
    } else {
        console.log('Browser not support');
    }
}

//Action
function pilihKategori() {  
    $('select[name=kategori]').change(function (e) { 
        e.preventDefault();
        subkategori(this.value);
    });
}

// ClearInput

function clearInput() {
    $('.preview-images-zone').html('');
    $('input[name=produk]').val('');
    $('select[name=kategori]').val('');
    $('select[name=subkategori]').val('');
    $('select[name=merek]').val('');
    $('input[name=tag]').val('');
    $('input[name=berat]').val('');
    $('input[name=sku]').val('');
    $('input[name=stok]').val('');
    $('input[name=harga]').val('');
    $('input[name=minPembelian]').val('');
    $('textarea[name=keterangan]').text('');
    $('textarea[name=keterangan]').val('');
 }

// GET API
function kategori() {
    $.ajax({
        type: "GET",
        url: urlApi+'kategoriController/get',
        dataType: "json",
        success: function (r) {
            r.forEach(v => {
                $('select[name=kategori]').append('<option value='+v.id+'>'+v.namaKategori+'</option>'); 
            });
        }
    });
}

function subkategori(id='') {
    $('select[name=subkategori]').html('');
    $.ajax({
        type: "GET",
        url: urlApi+'subkategoriController/get/0/'+id,
        dataType: "json",
        success: function (r) {
            $('select[name=subkategori]').html('<option value="">-- Pilih Subkategori --</option>');
            r.forEach(v => {
                $('select[name=subkategori]').append('<option value='+v.id+'>'+v.namaSubkategori+'</option>'); 
            });
        }
    });
}

function merek() {
    $('select[name=merek]').html('');
    $.ajax({
        type: "GET",
        url: urlApi+'merekController/get/',
        dataType: "json",
        success: function (r) {
            $('select[name=merek]').html('<option value="">-- Pilih Merek --</option>');
            r.forEach(v => {
                $('select[name=merek]').append('<option value='+v.id+'>'+v.namaMerek+'</option>'); 
            });
        }
    });
}

