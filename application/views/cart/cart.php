<!--SECTION 1-->
<div class="container bg-white pb-10 pt-10 mt-100">
    <!-- SLIDE -->
    <?php $this->load->view('incl/slide');?>
    <!-- CLOSED SLIDE -->
    <!-- Swipe Slide -->
    <?php $this->load->view('incl/swipe_slide');?>
    <!-- Closed Swipe Slide -->
</div>
<!--SECTION 1-->
<!--BREADCUM SECTION-->
<nav aria-label="breadcrumb" class="mt-95">
    <ol class="breadcrumb fs14px">
        <li class="breadcrumb-item"><a href="#">Beranda</a></li>
        <li class="breadcrumb-item active" aria-current="page">Keranjang Belanja</li>
    </ol>
</nav>
<!--BREADCUM SECTION-->

<!--PRODUCT LIST SECTION-->
<div class="container boxContents mb-lg-4 pb-3">
    <div class="row">
        <div class="col-8">
            <div class="product-detail_spec">
                <div class="product-detail_header font-weight-bold">
                    Daftar Keranjang Belanja
                </div>

                <ul class="cart-detail_list list-group list-group-flush">
                    <li class="cart-detail_lis_item list-group-item">
                        <div class="row">
                            <div class="col-1">
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="cart1">
                                    <label class="custom-control-label" for="cart1"></label>
                                </div>
                            </div>
                            <div class="col-2">
                                <a href="detail-product.html" class="mr-25"><img src="assets/img/produk/testprod4.jpg" class="border rounded shadow-sm w-100" alt="..."></a>
                            </div>
                            <div class="col-8">
                                <div class="media mb-10">
                                    <div class="media-body">
                                        <span class="mt-0 mb-1 h6 d-block"><a href="#" class="text-dark">Minyak Zaitun 350 ML</a></span>
                                        <span class="text-lapak font-weight-bold">Rp70.000</span>
                                        <div class="input-group w-50">
                                  <span class="input-group-btn">
                                      <button type="button" class="btn btn-outline-info rounded-0 btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                                          <span class="fa fa-minus"></span>
                                      </button>
                                  </span>
                                            <input type="text" name="quant[1]" class="form-control input-number" value="1" min="1" max="10">
                                            <span class="input-group-btn">
                                  <button type="button" class="btn btn-outline-info rounded-0 btn-number" data-type="plus" data-field="quant[1]">
                                      <span class="fa fa-plus"></span>
                                  </button>
                                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1">
                                <button class="btn" style="padding: 1px 6px;"><i class="fas fa-trash"></i></button>
                            </div>
                        </div>
                    </li>
                    <li class="cart-detail_lis_item list-group-item">
                        <div class="row">
                            <div class="col-1">
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="cart2">
                                    <label class="custom-control-label" for="cart2"></label>
                                </div>
                            </div>
                            <div class="col-2">
                                <a href="detail-product.html" class="mr-25"><img src="assets/img/produk/testprod4.jpg" class="border rounded shadow-sm w-100" alt="..."></a>
                            </div>
                            <div class="col-8">
                                <div class="media mb-10">
                                    <div class="media-body">
                                        <span class="mt-0 mb-1 h6 d-block"><a href="#" class="text-dark">Minyak Zaitun 350 ML</a></span>
                                        <span class="text-lapak font-weight-bold">Rp70.000</span>
                                        <div class="input-group w-50">
                                  <span class="input-group-btn">
                                      <button type="button" class="btn btn-outline-info rounded-0 btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                                          <span class="fa fa-minus"></span>
                                      </button>
                                  </span>
                                            <input type="text" name="quant[1]" class="form-control input-number" value="1" min="1" max="10">
                                            <span class="input-group-btn">
                                  <button type="button" class="btn btn-outline-info rounded-0 btn-number" data-type="plus" data-field="quant[1]">
                                      <span class="fa fa-plus"></span>
                                  </button>
                                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1">
                                <button class="btn" style="padding: 1px 6px;"><i class="fas fa-trash"></i></button>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-4">
            <div class="product-detail_spec">
                <div class="product-detail_header font-weight-bold">
                    Ringkasan Belanja
                </div>
                <ul class="list-group cart-detail_pricingin">
                    <li class="list-group-item cart-detail_pricingin_list">
                        <div class="cart-detail_pricingin_list_label">
                            <span class="text-left float-left clearfix">Rp 70.000</span>
                        </div>
                        <div class="text-right">
                            1x
                        </div>
                    </li>
                    <li class="list-group-item cart-detail_pricingin_list">
                        <div class="cart-detail_pricingin_list_label">
                            <span class="text-left float-left clearfix">Rp 70.000</span>
                        </div>
                        <div class="text-right">
                            1x
                        </div>
                    </li>
                </ul>
                <hr>
                <div class="cart-detail_pricingin_list_labelin">
                    <span class="text-left float-left clearfix font-weight-bold">Total Belanja</span>
                </div>
                <div class="text-right font-weight-bold">
                    Rp 140.000
                </div>
                <a href="pembayaran.html" class="btn btn-prod product-detail_button_buynow mt-4">Bayar Sekarang</a>
            </div>
        </div>
    </div>
</div>
<!--PRODUCT LIST SECTION-->

<!--FOOTER-->
<footer class="footer mt-20">
    <div class="container bottom_border">
        <div class="row">
            <div class=" col-sm-4 col-md  col-6 col">
                <h5 class="headin5_amrc col_white_amrc pt2">Tentang Kami</h5>
                <!--FOOTER MENU 1-->
                <ul class="footer_ul_amrc">
                    <li><a href="#">Tentang PKK</a></li>
                    <li><a href="#">Aturan Pengguna</a></li>
                    <li><a href="#">Kebijakan Privasi</a></li>
                    <li><a href="#">Karir</a></li>
                    <li><a href="#">Hubungi Kami</a></li>
                </ul>
                <!--FOOTER MENU 1-->
            </div>

            <div class=" col-sm-4 col-md  col-6 col">
                <h5 class="headin5_amrc col_white_amrc pt2">Layanan Pelanggan</h5>
                <!--FOOTER MENU 2-->
                <ul class="footer_ul_amrc">
                    <li><a href="#">Pusat Bantuan</a></li>
                    <li><a href="#">FAQ (Tanya Jawab)</a></li>
                    <li><a href="#">Lacak Pengiriman</a></li>
                    <li><a href="#">Pengembalian Barang & Dana</a></li>
                    <li><a href="#">Cara Pembelian & Pembayaran</a></li>
                </ul>
                <!--FOOTER MENU 2-->
            </div>


            <div class=" col-sm-4 col-md  col-12 col">
                <h5 class="headin5_amrc col_white_amrc pt2">Jelajahi Store</h5>
                <!--FOOTER MENU 3-->
                <ul class="footer_ul_amrc">
                    <li><a href="#">Semua Kategori</a></li>
                    <li><a href="#">Daftar Brand</a></li>
                    <li><a href="#">Rekomendasi Produk</a></li>
                </ul>
                <!--FOOTER MENU 3-->
            </div>


            <div class=" col-sm-4 col-md  col-6 col">
                <h5 class="headin5_amrc col_white_amrc pt2">Pembayaran</h5>
                <!--FOOTER MENU 4-->
                <ul class="footer_ul_amrc">
                    <li><img src="assets/img/other/bankdki.jpg" width="90"></li>
                </ul>

                <h5 class="headin5_amrc col_white_amrc">Pengiriman</h5>
                <ul class="footer_ul_amrc">
                    <li><img src="assets/img/other/gosend.jpg" width="80"></li>
                </ul>
                <!--FOOTER MENU 4-->
            </div>

        </div>
    </div>