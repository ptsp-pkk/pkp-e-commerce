
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?=$title;?></title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?=base_url('assets_admin/');?>vendors/iconfonts/mdi/css/materialdesignicons.css">
    <!-- endinject -->
    <!-- vendor css for this page -->
    <!-- End vendor css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="<?=base_url('assets_admin/');?>css/shared/style.css">
    <!-- endinject -->
    <!-- Layout style -->
    <link rel="stylesheet" href="<?=base_url('assets_admin/');?>css/demo_1/style.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">

      <?php $uri = $this->uri->segment(3);
      if ($uri == 'add-new'){ ?>
          <link rel="stylesheet" href="<?=base_url('assets_admin/');?>css/shared/jquery-ui.min.css">
          <link rel="stylesheet" href="<?=base_url('assets_admin/');?>css/admin_stylesheet.css">
      <?php } ?>
      <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <!-- Layout style -->
<!--    <link rel="shortcut icon" href="../asssets/images/favicon.ico" />-->
  </head>
  <body class="header-fixed">
    <!-- partial:partials/_header.html -->
    <?php $this->load->view('admin/incl/nav');?>
    <!-- partial -->
    <div class="page-body">
      <!-- partial:partials/_sidebar.html -->
      <?php $this->load->view('admin/incl/sidebar');?>
      <!-- partial -->
      <div class="page-content-wrapper">
        <div class="page-content-wrapper-inner">
          <div class="content-viewport">
              <?php $this->load->view($link_view);?>
          </div>
        </div>
        <!-- content viewport ends -->
        <!-- partial:partials/_footer.html -->
        <?php $this->load->view('admin/incl/footer');?>
        <!-- partial -->
      </div>
      <!-- page content ends -->
    </div>
    <!--page body ends -->
    <!-- SCRIPT LOADING START FORM HERE /////////////-->
    <!-- plugins:js -->
    <script src="<?=base_url('assets_admin/');?>vendors/js/core.js"></script>
    <!-- endinject -->
    <!-- Vendor Js For This Page Ends-->
    <script src="<?=base_url('assets_admin/');?>vendors/apexcharts/apexcharts.min.js"></script>
    <script src="<?=base_url('assets_admin/');?>vendors/chartjs/Chart.min.js"></script>
    <script src="<?=base_url('assets_admin/');?>js/charts/chartjs.addon.js"></script>
    <!-- Vendor Js For This Page Ends-->
    <!-- build:js -->
    <script src="<?=base_url('assets_admin/');?>js/all.js"></script>
    <script src="<?=base_url('assets_admin/');?>js/template.js"></script>
    <script src="<?=base_url('assets_admin/');?>js/dashboard.js"></script>

        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>



    <?php $uri = $this->uri->segment(3);
    if ($uri == 'add-new'){ ?>
        <script src="<?=base_url('assets_admin/');?>js/jquery-ui.min.js"></script>
        <script src="<?=base_url('assets_admin/');?>js/add_product.js"></script>
    <?php } ?>

    <!-- endbuild -->
  </body>
</html>