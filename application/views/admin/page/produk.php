<div class="row mb-4">
  <div class="col-md-12">
    <button onclick="window.location.href = '<?=site_url();?>seller/product/add-new';" type="button" id="add" class="btn btn-success has-icon">
      <i class="mdi mdi-plus"></i>
      Tambah
    </button>

    <button type="button" id="export" data-toggle="modal" data-target="#exportModal" class="btn btn-info has-icon">
      <i class="mdi mdi-export"></i>
      Export Data Produk
    </button>

  </div>


</div>
<style type="text/css">
 .left    { text-align: left;}
 .right   { text-align: right;}
 .center  { text-align: center;}
 .justify { text-align: justify;}
</style>

<div class="grid">
  <p class="grid-header">Daftar Produk di Toko Anda</p>
  <div class="data-table-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="data-table-list">
            <div class="table-responsive">
              <table id="tabel" class="table table-hover" style="width:100% !important;">
                <thead class="thead-dark center">
                  <tr>
                    <th>Produk</th>
                    <th>Harga</th>
                    <th>Stok</th>
                    <th>Kondisi</th>
                    <th>Berat (Gram)</th>
                    <th>Min Pembelian</th>
                    <!-- <th>Merek</th> -->
                  </tr>
                </thead>
                <tbody>
                  <!-- <tr>
                    <td>Beras</td>
                    <td>Rp. 250.051</td>
                    <td class="center">100</td>
                    <td class="center">Bekas</td>
                    <td class="center">1</td>
                    <td class="center">1000</td>
                    <td>Indomay</td>
                    <td id="edit" data-toggle="modal" data-target="#editModal" class="center mdi mdi-border-color"></td>
                  </tr>  -->
                  <!-- <tr>
                    <td>Piring Cantik</td>
                    <td>Rp. 53.000</td>
                    <td class="center">50</td>
                    <td class="center">Baru</td>
                    <td class="center">1</td>
                    <td class="center">200</td>
                    <td>Anonymous</td>
                    <td id="edit" data-toggle="modal" data-target="#editModal" class="center mdi mdi-border-color"></td>
                  </tr>
                  <tr>
                    <td>Lemari Plastik Rakitan</td>
                    <td>Rp. 750.000</td>
                    <td class="center">7</td>
                    <td class="center">Baru</td>
                    <td class="center">1</td>
                    <td class="center">5000</td>
                    <td>IndoPlastik</td>
                    <td id="edit" data-toggle="modal" data-target="#editModal" class="center mdi mdi-border-color"></td>
                  </tr> -->
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



  <!-- Add Modal -->
<!--  <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">-->
<!--    <div class="modal-dialog" role="document">-->
<!--      <div class="modal-content">-->
<!--        <div class="modal-header">-->
<!--          <h5 class="modal-title" id="exampleModalLabel">Tambah Produk</h5>-->
<!--          <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--            <span aria-hidden="true">&times;</span>-->
<!--          </button>-->
<!--        </div>-->
<!--        <div class="modal-body">-->
<!--          <div class="row">-->
<!--            <div class="col-md-12">-->
<!--              <div class="form-group">-->
<!--                <p>Nama Produk </p>-->
<!--                <input type="text" class="form-control" nama="produk" id="nama" >-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-12">-->
<!--              <div class="form-group">-->
<!--                <p>Upload Gambar </p>-->
<!--                <div class="custom-file">-->
<!--                  <input type="file" name="file" class="custom-file-input" id="customFile">-->
<!--                  <label class="custom-file-label" for="customFile">Pilih</label>-->
<!--                </div>-->
<!--              </div>-->
<!--            </div>-->
<!--            -->
<!--            <div class="col-md-6">-->
<!--              <div class="form-group">-->
<!--                <p>Harga </p>-->
<!--                <input type="number" class="form-control" name="harga" id="harga" >-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-6">-->
<!--              <div class="form-group">-->
<!--                <p>Stok </p>-->
<!--                <input type="number" class="form-control" name="stok" id="stok" >-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-6">-->
<!--              <div class="form-group">-->
<!--                <p>Berat Produk <span style="font-size:10px;">(Gram)</span></p>-->
<!--                <input type="number" class="form-control" name="berat" id="berat" >-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-6">-->
<!--              <div class="form-group">-->
<!--                <p>Minimal Pembelian </p>-->
<!--                <input type="number" min='1' class="form-control" name="minPembelian" id="minPembelian" value='1'>-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-6">-->
<!--              <div class="form-group">-->
<!--                <p>Kategori </p>-->
<!--                <select name="kategori" class="custom-select">-->
<!--                  <option selected="">-- Pilih Kategori --</option>-->
<!--                </select>-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-6">-->
<!--              <div class="form-group">-->
<!--                <p>Sub Kategori </p>-->
<!--                <select name="subkategori" class="custom-select">-->
<!--                  <option selected="">-- Pilih Sub Kategori --</option>-->
<!--                </select>-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-6">-->
<!--              <div class="form-group">-->
<!--                <p>Merek </p>-->
<!--                <select name="merek" class="custom-select">-->
<!--                  <option selected="">-- Pilih Merek --</option>-->
<!--                </select>-->
<!--              </div>-->
<!--            </div>-->
<!--            -->
<!--            <div class="col-md-6">-->
<!--              <div class="form-group">-->
<!--                <p>Kondisi </p>-->
<!--                <div class="form-inline">-->
<!--                  <div class="radio mb-3">-->
<!--                    <label class="radio-label mr-4">-->
<!--                      <input name="kondisi" type="radio" checked="">Baru<i class="input-frame"></i>-->
<!--                    </label>-->
<!--                  </div>-->
<!--                  <div class="radio mb-3">-->
<!--                    <label class="radio-label">-->
<!--                      <input name="kondisi" type="radio">Bekas <i class="input-frame"></i>-->
<!--                    </label>-->
<!--                  </div>-->
<!--                </div>-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-12">-->
<!--              <div class="form-group">-->
<!--                <p>Keterangan </p>-->
<!--                <textarea name="keterangan" class="form-control" placeholder="Jelaskan deskripsi produk yang akan dijual"></textarea>-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--          </div>-->
<!--          <div class="modal-footer">-->
<!--            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>-->
<!--            <button type="button" class="btn btn-primary">Simpan</button>-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--    </div>-->
<!---->
<!--    Add Modal -->
<!--  <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">-->
<!--    <div class="modal-dialog" role="document">-->
<!--      <div class="modal-content">-->
<!--        <div class="modal-header">-->
<!--          <h5 class="modal-title" id="exampleModalLabel">Edit Produk</h5>-->
<!--          <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--            <span aria-hidden="true">&times;</span>-->
<!--          </button>-->
<!--        </div>-->
<!--        <div class="modal-body">-->
<!--          <div class="row">-->
<!--            <div class="col-md-12">-->
<!--              <div class="form-group">-->
<!--                <p>Nama Produk </p>-->
<!--                <input type="text" class="form-control" nama="produk" id="nama" >-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-12">-->
<!--              <div class="form-group">-->
<!--                <p>Upload Gambar </p>-->
<!--                <div class="custom-file">-->
<!--                  <input type="file" name="file" class="custom-file-input" id="customFile">-->
<!--                  <label class="custom-file-label" for="customFile">Pilih</label>-->
<!--                </div>-->
<!--              </div>-->
<!--            </div>-->
<!--            -->
<!--            <div class="col-md-6">-->
<!--              <div class="form-group">-->
<!--                <p>Harga </p>-->
<!--                <input type="number" class="form-control" name="harga" id="harga" >-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-6">-->
<!--              <div class="form-group">-->
<!--                <p>Stok </p>-->
<!--                <input type="number" class="form-control" name="stok" id="stok" >-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-6">-->
<!--              <div class="form-group">-->
<!--                <p>Berat Produk <span style="font-size:10px;">(Gram)</span></p>-->
<!--                <input type="number" class="form-control" name="berat" id="berat" >-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-6">-->
<!--              <div class="form-group">-->
<!--                <p>Minimal Pembelian </p>-->
<!--                <input type="number" min='1' class="form-control" name="minPembelian" id="minPembelian" value='1'>-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-6">-->
<!--              <div class="form-group">-->
<!--                <p>Kategori </p>-->
<!--                <select name="kategori" class="custom-select">-->
<!--                  <option selected="">-- Pilih Kategori --</option>-->
<!--                </select>-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-6">-->
<!--              <div class="form-group">-->
<!--                <p>Sub Kategori </p>-->
<!--                <select name="subkategori" class="custom-select">-->
<!--                  <option selected="">-- Pilih Sub Kategori --</option>-->
<!--                </select>-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-6">-->
<!--              <div class="form-group">-->
<!--                <p>Merek </p>-->
<!--                <select name="merek" class="custom-select">-->
<!--                  <option selected="">-- Pilih Merek --</option>-->
<!--                </select>-->
<!--              </div>-->
<!--            </div>-->
<!--            -->
<!--            <div class="col-md-6">-->
<!--              <div class="form-group">-->
<!--                <p>Kondisi </p>-->
<!--                <div class="form-inline">-->
<!--                  <div class="radio mb-3">-->
<!--                    <label class="radio-label mr-4">-->
<!--                      <input name="kondisi" type="radio" checked="">Baru<i class="input-frame"></i>-->
<!--                    </label>-->
<!--                  </div>-->
<!--                  <div class="radio mb-3">-->
<!--                    <label class="radio-label">-->
<!--                      <input name="kondisi" type="radio">Bekas <i class="input-frame"></i>-->
<!--                    </label>-->
<!--                  </div>-->
<!--                </div>-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-12">-->
<!--              <div class="form-group">-->
<!--                <p>Keterangan </p>-->
<!--                <textarea name="keterangan" class="form-control" placeholder="Jelaskan deskripsi produk yang akan dijual"></textarea>-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--          </div>-->
<!--          <div class="modal-footer">-->
<!--            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>-->
<!--            <button type="button" class="btn btn-primary">Update</button>-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--    </div>-->
<!---->
<!--    <Export Modal -->
<!--    <div class="modal fade" id="ExportModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">-->
<!--      <div class="modal-dialog" role="document">-->
<!--        <div class="modal-content">-->
<!--          <div class="modal-header">-->
<!--            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>-->
<!--            <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--              <span aria-hidden="true">&times;</span>-->
<!--            </button>-->
<!--          </div>-->
<!--          <div class="modal-body">-->
<!--            ...-->
<!--          </div>-->
<!--          <div class="modal-footer">-->
<!--            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
<!--            <button type="button" class="btn btn-primary">Save changes</button>-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--    </div>-->


<script>

  $(document).ready(function () {
    showtable();
  });

  function showtable() {
    $('#tabel').DataTable({
        // Processing indicator
        "destroy": true,
        "searching": true,
        "processing": true,
        // DataTables server-side processing mode
        "serverSide": true,
        "scrollX": true,
        // Initial no order.
        "order": [],
        // Load data from an Ajax source
        "ajax": {
            "url": "<?= base_url("api/backend/produkController/dt"); ?>",
            "type": "POST",
        },
        //Set column definition initialisation properties
        "columnDefs": [{
            "targets": [0],
            "orderable": false
        }]
    });
  }
</script>