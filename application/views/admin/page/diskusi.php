<div class="grid">
  <p class="grid-header">Diskusi Produk</p>
  <div class="table-responsive">
    <table class="table table-hover table-sm">
      <thead>
        <tr class="solid-header">
            <th>Pembeli</th>
            <th>Produk</th>
            <th>Tanggal</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="pl-md-0">
                <small class="text-black font-weight-medium d-block">Justin Curtis</small>
                <span class="text-gray">
                  <span class="status-indicator rounded-indicator small bg-success"></span>Ready stock gan?</span>
              </td>
              <td>
                <small>Hardisk</small>
            </td>
            <td> Just Now </td>
        </tr>
        <tr>
            <td class="pl-md-0">
                <small class="text-black font-weight-medium d-block">Charlie Hawkins</small>
                <span class="text-gray">Ada warna merah gak...</span>
              </td>
              <td>
                <small>Tas Punggung Pria</small>
            </td>
            <td> Mar 04, 2018 11:37am </td>
        </tr>
        <tr>
            <td class="pl-md-0">
                <small class="text-black font-weight-medium d-block">Cia Richards</small>
                <span class="text-gray">
                  <span></span>Ok makasih ya</span>
              </td>
              <td>
                <small>5673467743</small>
            </td>
            <td> Feb 21, 2018 8:34am </td>
        </tr>
    </tbody>
</table>
</div>
<a class="border-top px-3 py-2 d-block text-gray" href="#">
    <small class="font-weight-medium"><i class="mdi mdi-chevron-down mr-2"></i>View All</small>
</a></div>