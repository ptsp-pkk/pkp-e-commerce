<form action="javascript:void(0)" enctype='multipart/form-data' id="add" method="post">
<div class="row">
    <div class="col-lg-12 equel-grid">
        <div class="grid">
            <div class="grid-header">
                <span class="h5">Upload Foto</span>
                <span class="text-muted d-block">Format gambar .jpg .jpeg .png dan ukuran minimum 300 x 300px (Untuk gambar optimal gunakan ukuran minimum 700 x 700 px)</span>
            </div>
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="preview-images-zone">
                    </div>

                    <div class="text-center mt-3">
                        <fieldset class="form-group mb-0">
                            <a href="javascript:void(0)" onclick="$('#pro-image').click()" class="btn btn-outline-info">+ Pilih Foto Produk</a>
                            <input type="file" id="pro-image" name="pro-image[]" style="display:none" class="form-control" multiple>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 equel-grid">
        <div class="grid">
            <div class="grid-header">
                <span class="h5">Informasi Produk</span>
            </div>
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="form-group">
                        <p class="h6">name Produk </p>
                        <input type="text" class="form-control" required name="produk" id="name" required>
                    </div>
                    <div class="form-group form-row">
                        <div class="col">
                            <p class="h6">Kategori </p>
                            <select name="kategori" class="custom-select">
                                <option selected="" value="">-- Pilih Kategori --</option>
                            </select>
                        </div>
                        <div class="col">
                            <p class="h6">Sub Kategori </p>
                            <select name="subkategori" class="custom-select">
                                <option selected="">-- Pilih Sub Kategori --</option>
                            </select>
                        </div>
                    </div>
                    <!--                    <div class="form-group">-->
                    <!--                        <p class="h6">Etalase</p>-->
                    <!--                        <input type="text" class="form-control" name="etalase" id="etalase">-->
                    <!--                    </div>-->
                    <div class="form-group form-row">
                        <div class="col">
                            <p class="h6">Tag <span class="text-muted">(Gunakan tanda koma(,) sebagai pemisah)</span></p>
                            <input type="text" class="form-control" name="tag" id="tag">
                        </div>
                        <div class="col">
                            <p class="h6">SKU <span class="text-muted">(Stock Keeping Unit)</span></p>
                            <input type="text" class="form-control" name="sku" required id="sku">
                        </div>
                    </div>
                    <div class="form-group">
                        <p class="h6">Merek </p>
                        <select name="merek" class="custom-select">
                            <option value="">-- Pilih Merek --</option>
                        </select>
                    </div>
                    <div class="form-group form-row">
                        <div class="col">
                            <p class="h6">Berat Produk <span style="font-size:10px;">(Gram)</span></p>
                            <input type="number" class="form-control" required name="berat" id="berat" >
                        </div>
                        <div class="col">
                            <p class="h6">Stok </p>
                            <input type="number" class="form-control" name="stok" required id="stok" >
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <div class="col">
                            <p class="h6">Harga </p>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Rp</div>
                                </div>
                                <input type="number" class="form-control" required name="harga" id="harga" >
                            </div>
                        </div>
                        <div class="col">
                            <p class="h6">Minimal Pembelian </p>
                            <input type="number" min='1' class="form-control" name="minPembelian" id="minPembelian" value='1'>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 equel-grid">
        <div class="grid">
            <div class="grid-header">
                <span class="h5">Deskripsi Produk</span>
            </div>
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="form-group">
                        <p class="h6">Kondisi </p>
                        <div class="form-inline">
                            <div class="radio mb-3">
                                <label class="radio-label mr-4">
                                    <input name="kondisi" type="radio" value="1"  checked="">Baru<i class="input-frame"></i>
                                </label>
                            </div>
                            <div class="radio mb-3">
                                <label class="radio-label">
                                    <input name="kondisi" type="radio" value="2">Bekas <i class="input-frame"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <p class="h6">Keterangan </p>
                            <textarea name="keterangan" class="form-control" placeholder="Jelaskan deskripsi produk yang akan dijual" rows="3"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="mt-3">
    <a href="<?=site_url();?>min/admin/produk" class="btn btn-default w-25">Batal</a>
    <button type="submit" class="btn btn-primary w-25 float-right" id="btnJualBarang">Jual Barang <span id="loadingJual" style="display:none;margin-left:5px;" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span></button>
</div>
</form>