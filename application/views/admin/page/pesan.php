<div class="grid">
  <p class="grid-header">Pesan dari Pembeli</p>
  <div class="table-responsive">
    <table class="table table-hover table-sm">
      <thead>
        <tr class="solid-header">
            <th>Pembeli</th>
            <th>Nomor Pemesanan</th>
            <th>Tanggal</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="pl-md-0">
                <small class="text-black font-weight-medium d-block">Justin Curtis</small>
                <span class="text-gray">
                  <span class="status-indicator rounded-indicator small bg-success"></span>Kenapa belum dikirim ya pesanannya...</span>
              </td>
              <td>
                <small>8523537435</small>
            </td>
            <td> Just Now </td>
        </tr>
        <tr>
            <td class="pl-md-0">
                <small class="text-black font-weight-medium d-block">Charlie Hawkins</small>
                <span class="text-gray">Ok makasih ya bang</span>
              </td>
              <td>
                <small>9537537436</small>
            </td>
            <td> Mar 04, 2018 11:37am </td>
        </tr>
        <tr>
            <td class="pl-md-0">
                <small class="text-black font-weight-medium d-block">Nina Bates</small>
                <span class="text-gray">
                  <span class="status-indicator rounded-indicator small bg-warning"></span>Chat Berakhir</span>
              </td>
              <td>
                <small>7533567437</small>
            </td>
            <td> Mar 13, 2018 9:41am </td>
        </tr>
        <tr>
            <td class="pl-md-0">
                <small class="text-black font-weight-medium d-block">Cia Richards</small>
                <span class="text-gray">
                  <span></span>OK deh</span>
              </td>
              <td>
                <small>5673467743</small>
            </td>
            <td> Feb 21, 2018 8:34am </td>
        </tr>
    </tbody>
</table>
</div>
<a class="border-top px-3 py-2 d-block text-gray" href="#">
    <small class="font-weight-medium"><i class="mdi mdi-chevron-down mr-2"></i>View All</small>
</a></div>