<div class="row mb-4">


</div>
<style type="text/css">
 .left    { text-align: left;}
 .right   { text-align: right;}
 .center  { text-align: center;}
 .justify { text-align: justify;}
 .padding { padding: 5px 10px 10px;}
 .padding2 { padding: 10px 0px 20px;}
</style>


<div class="grid">
  <p class="grid-header">Daftar Pesanan Anda</p>
  <div class="data-table-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="data-table-list">
            <div class="table-responsive">
              <table id="data-table-basic" class="table table-hover">
                <thead class="thead-dark center">
                  <tr>
                    <th>Pemesan</th>
                    <th>Produk</th>
                    <th>Jumlah</th>
                    <th>Catatan</th>
                    <th>Alamat Pengiriman</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="center">Mellina</td>
                    <td class="center">Piring Cantik</td>
                    <td class="center">55</td>
                    <td class="center">Extra bubble wrap</td>
                    <td class="center">Jl. Arista II, Pondok Kelapa</td>
                    <td class="center"><button type="button" id="add" data-toggle="modal" data-target="#addModal" class="btn btn-primary has-icon">Terima/Tolak</button></td>
                  </tr>
                  <tr>
                    <td class="center">Mellina</td>
                    <td class="center">Piring Cantik</td>
                    <td class="center">55</td>
                    <td class="center">Double cream cheese</td>
                    <td class="center">Depok</td>
                    <td class="center"><button type="button" id="add" data-toggle="modal" data-target="#addModal" class="btn btn-success has-icon">Diterima</button></td>
                  </tr>
                  <tr>
                    <td class="center">Mellina</td>
                    <td class="center">Piring Cantik</td>
                    <td class="center">55</td>
                    <td class="center">Extra micin</td>
                    <td class="center">Medan</td>
                    <td class="center"><button type="button" id="add" data-toggle="modal" data-target="#addModal" class="btn btn-warning has-icon">Ditolak</button></td>
                  </tr>
                  <tr>
                    <td class="center">Mellina</td>
                    <td class="center">Piring Cantik</td>
                    <td class="center">55</td>
                    <td class="center">Extra mozarella</td>
                    <td class="center">Bekasi</td>
                    <td class="center">Dibatalkan</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Add Modal -->
  <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Detail Pesanan</h5>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <div class="padding row">

              <div class="card">
                <div class="photo">
                  <img class="profile-img img-lg rounded-circle" src="https://www.hometown.in/media/product/80/5853/60407/1-product_500.jpg">
                </div>
                <div class="padding2">
                  <h1>Piring Cantik</h1>
                  <h4>Rp. 25000</h4>
                </div>
                <table class="padding" style="width: 81.5407%;">
                  <tbody>
                    <tr>
                      <td>Pembeli</td>
                      <th>Mellina</th>
                    </tr>
                    <tr>
                      <td>Alamat</td>
                      <th>Jl. Arista II, Komp. Billy Moon, Pondok Kelapa</th>
                    </tr>
                    <tr>
                      <td>Pengiriman</td>
                      <th>Gojek</th>
                    </tr>
                    <tr>
                      <td>Pembayaran</td>
                      <th>Cash On Delivery</th>
                    </tr>
                    <tr>
                      <td>Total Barang</td>
                      <th>10</th>
                    </tr>
                    <tr>
                      <td>Total Harga</td>
                      <th>Rp. 250000</th>
                    </tr>
                    <tr>
                      <td>Biaya Pengiriman</td>
                      <th>Rp. 30000</th>
                    </tr>
                    <tr>
                      <td>Total Pembayaran</td>
                      <th>Rp. 280000</th>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline-success">Terima</button>
              <button type="button" class="btn btn-outline-danger">Tolak</button>
              <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Export Modal -->
<div class="modal fade" id="ExportModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
