<div class="row">
    <div class="col-lg-12 equel-grid">
        <div class="grid">
            <div class="grid-header">
                <span class="h5">Upload Gambar Profil Toko</span>
                <span class="text-muted d-block">Format gambar .jpg .jpeg .png dan ukuran minimum 300 x 300px (Untuk gambar optimal gunakan ukuran minimum 700 x 700 px)</span>
            </div>
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="preview-images-zone">
                    </div>

                    <div class="text-center mt-3">
                        <fieldset class="form-group mb-0">
                            <a href="javascript:void(0)" onclick="$('#pro-image').click()" class="btn btn-outline-info">+ Pilih Gambar Profil Toko</a>
                            <input type="file" id="pro-image" name="pro-image" style="display: none;" class="form-control" multiple>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 equel-grid">
        <div class="grid">
            <div class="grid-header">
                <span class="h5">Informasi Toko</span>
            </div>
            <div class="grid-body">
                <div class="item-wrapper">
                    <div class="form-group form-row">
                        <div class="col">
                            <p class="h6">Nama Toko</p>
                            <input type="text" class="form-control" placeholder="Isi nama toko Anda (max 50 karakter)" nama="namatoko" id="namatoko">
                        </div>
                        <div class="col">
                            <p class="h6">Slogan Toko</p>
                            <input type="text" class="form-control" placeholder="Isi slogan toko Anda (max 60 karakter)" nama="slogan" id="slogan">
                        </div>
                    </div>
                    <div class="form-group">
                        <p class="h6">Status Toko </p>
                        <div class="form-inline">
                            <div class="radio mb-3">
                                <label class="radio-label mr-4">
                                    <input name="levelstore" type="radio" checked="">Official<i class="input-frame"></i>
                                </label>
                            </div>
                            <div class="radio mb-3">
                                <label class="radio-label">
                                    <input name="levelstore" type="radio">Unofficial<i class="input-frame"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <p class="h6">Deskripsi</p>
                            <textarea name="deskripsi" class="form-control" placeholder="Isi deskripsi mengenai toko Anda" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="mt-3">
                        <a href="#" class="btn btn-primary w-25 float-right">Save / Edit</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 equel-grid">
        <div class="grid">
            <div class="grid-header">
                <span class="h5">Informasi Dukungan Pengiriman</span>
            </div>
            <button type="button" id="add" data-toggle="modal" data-target="#addModal" title="Tambah Metode Pengiriman" class="btn btn-success has-icon">
              <i class="mdi mdi-plus"></i>Tambah Pengiriman</button>
              <div class="data-table-list">
                <div class="table-responsive">
                  <table id="data-table-basic" class="table table-hover">
                    <thead class="thead-dark center">
                      <tr>
                        <th class="center">Nama Jasa Pengiriman</th>
                        <th class="center">Jenis Pengiriman</th>
                    </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="center">Go Jek</td>
                    <td class="center">One Day</td>
                </tr>
                <tr>
                    <td class="center">JNE</td>
                    <td class="center">Regular</td>
                </tr>
                <tr>
                    <td class="center">Pos Indonesia</td>
                    <td class="center">Regular</td>
                </tr>
            </table>
        </div>
    </div>
</div>
</div>
</div>

<!--    Add Modal --> 
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Metode Pengiriman</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row showcase_row_area">
          <div class="col-md-3 showcase_text_area">
            <label>One Day</label>
          </div>
          <div class="col-md-9 showcase_content_area">
            <div class="form-group">
              <div class="checkbox">
                <label>
                  <input type="checkbox" class="form-check-input"> Go-Jek <i class="input-frame"></i>
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <input type="checkbox" class="form-check-input"> Grab <i class="input-frame"></i>
                </label>
              </div>
            </div>
          </div>
        </div>
        <div class="row showcase_row_area">
          <div class="col-md-3 showcase_text_area">
            <label>Regular</label>
          </div>
          <div class="col-md-9 showcase_content_area">
            <div class="form-group">
              <div class="checkbox">
                <label>
                  <input type="checkbox" class="form-check-input"> JNE <i class="input-frame"></i>
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <input type="checkbox" class="form-check-input"> POS Indonesia <i class="input-frame"></i>
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <input type="checkbox" class="form-check-input"> TIKI <i class="input-frame"></i>
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <input type="checkbox" class="form-check-input"> J&T <i class="input-frame"></i>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>
</div>

