<div class="sidebar">
  <div class="user-profile">
    <div class="display-avatar animated-avatar">
      <img class="profile-img img-lg rounded-circle" src="<?=base_url('assets_admin/');?>images/profile/male/image_1.png" alt="profile image">
    </div>
    <div class="info-wrapper">
      <p class="user-name">Allen Clerk</p>
      <h7 class="display-income">OFFICIAL STORE</h7></br>
      <h7>Koperasi ABC Jakarta Selatan</h7>
    </div>
  </div>
  <ul class="navigation-menu">
    <li class="nav-category-divider">MAIN</li>
    <li>
      <a href="<?=site_url('min/admin/dashboard')?>">
        <span class="link-title">Dashboard</span>
        <i class="mdi mdi-gauge link-icon"></i>
      </a>
    </li>
    <li>
      <a href="#sample-pages" data-toggle="collapse" aria-expanded="false">
        <span class="link-title">Profil</span>
        <i class="mdi mdi-account-circle link-icon"></i>
      </a>
      <ul class="collapse navigation-submenu" id="sample-pages">
        <li>
          <a href="<?=site_url('min/admin/profil_seller')?>">
            <span class="link-title">Profil Seller</span>
          </a>
        </li>
        <li>
          <a href="<?=site_url('min/admin/profilstore')?>">
            <span class="link-title">Profil Store</span>
          </a>
        </li>
      </ul>
    </li>
    <li>
      <a href="#produks" data-toggle="collapse" aria-expanded="false">
        <span class="link-title">Produk</span>
        <i class="mdi mdi-basket link-icon"></i>
      </a>
      <ul class="collapse navigation-submenu" id="produks">
        <li>
          <a href="<?=site_url('min/admin/produk')?>">Daftar Produk</a>
        </li>
        <li>
          <a href="pages/ui-components/tables.html">Review Pembeli</a>
        </li>
        <li>
          <a href="pages/ui-components/typography.html">Wishlist Produk</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="#pesan" data-toggle="collapse" aria-expanded="false">
        <span class="link-title">Pesan</span>
        <i class="mdi mdi-message-reply-text link-icon"></i>
      </a>
      <ul class="collapse navigation-submenu" id="pesan">
        <li>
          <a href="<?=site_url('min/admin/pesan')?>">Pesan Pembeli</a>
        </li>
        <li>
          <a href="<?=site_url('min/admin/diskusi')?>">Diskusi Produk</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="#penjualan" data-toggle="collapse" aria-expanded="false">
        <span class="link-title">Penjualan</span>
        <i class="mdi mdi-basket link-icon"></i>
      </a>
      <ul class="collapse navigation-submenu" id="penjualan">
        <li>
          <a href="<?=site_url('min/admin/daftar_psnan')?>">Daftar Pesanan</a>
        </li>
        <li>
          <a href="pages/ui-components/tables.html">Konfirmasi Pengiriman</a>
        </li>
        <li>
          <a href="pages/ui-components/tables.html">Status Pengiriman</a>
        </li>
        <li>
          <a href="pages/ui-components/typography.html">Pembayaran</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="<?=site_url('min/admin/transaksi')?>">
        <span class="link-title">Transaksi</span>
        <i class="mdi mdi-credit-card link-icon"></i>
      </a>
    </li>
    <li>
      <a href="<?=site_url('min/admin/pengaturan')?>">
        <span class="link-title">Pengaturan</span>
        <i class="mdi mdi-wrench link-icon"></i>
      </a>
    </li>
    <li>
      <a href="<?=site_url('min/admin/hubungi')?>">
        <span class="link-title">Hubungi Kami</span>
        <i class="mdi mdi-phone link-icon"></i>
      </a>
    </li>

  </ul>
  
</div>