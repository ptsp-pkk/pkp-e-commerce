
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?=$title;?></title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?=base_url('assets_admin/')?>vendors/iconfonts/mdi/css/materialdesignicons.css" />
    <link rel="stylesheet" href="<?=base_url('assets_admin/')?>vendors/css/vendor.addons.css" />
    <!-- endinject -->
    <!-- vendor css for this page -->
    <!-- End vendor css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="<?=base_url('assets_admin/')?>css/shared/style.css" />
    <!-- endinject -->
    <!-- Layout style -->
    <link rel="stylesheet" href="<?=base_url('assets_admin/')?>css/demo_1/style.css">
    <!-- Layout style -->
    <link rel="shortcut icon" href="<?=base_url('assets_admin/')?>images/favicon.ico" />
  </head>
  <body onload="loading()">
    <div class="authentication-theme auth-style_1">
      <div class="row">
        <div class="col-12 logo-section">
          <a href="../../index.html" class="logo">
            <img src="<?=base_url('assets_admin/')?>images/logo.svg" alt="logo" />
          </a>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-5 col-md-7 col-sm-9 col-11 mx-auto">
          <div class="grid">
            <div class="grid-body">
              <div class="row">
                <div class="col-lg-7 col-md-8 col-sm-9 col-12 mx-auto form-wrapper">
                  <form action="javascript:void(0)" id="form">
                    <div class="form-group input-rounded">
                      <input type="email" name="email" class="form-control" placeholder="Email" required />
                    </div>
                    <div class="form-group input-rounded">
                      <input type="password" name="password" class="form-control" placeholder="Kata Sandi" required />
                    </div>
                    <div class="form-inline">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" class="form-check-input" />Ingatkan aku <i class="input-frame"></i>
                        </label>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block"> Masuk </button>
                  </form>
                  <div class="signup-link">
                    <p>Tidak punya akun ?</p>
                    <a href="#">Daftar</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="auth_footer">
        <p class="text-muted text-center">© PKK - E-Commerce 2019</p>
      </div>
    </div>

    <script src="<?=base_url('assets/')?>js/jquery-3.3.1.slim.min.js"></script>
    <script>
    
      $(document).ready(function () {
        login();
      });
     
      function login() {
        $('#form').submit(function (e) { 
          e.preventDefault();
          $.ajax({
            type: "POST",
            url: "<?=site_url('min/Login/auth_admin');?>",
            data: $(this).serialize(),
            dataType: "json",
            success: function (res) {
              if (res.status == 1) {
                window.location.assign("Admin/dashboard");
              }else{
                alert(res.msg);
              }
            }
          });
        });
       
       }
    </script>
    <!--page body ends -->
    <!-- SCRIPT LOADING START FORM HERE /////////////-->
    <!-- plugins:js -->
    <script src="<?=base_url('assets_admin/')?>vendors/js/core.js"></script>
    <script src="<?=base_url('assets_admin/')?>vendors/js/vendor.addons.js"></script>
    <!-- endinject -->
    <!-- Vendor Js For This Page Ends-->
    <!-- Vendor Js For This Page Ends-->
    <!-- build:js -->
    <script src="<?=base_url('assets_admin/')?>js/template.js"></script>
    <!-- endbuild -->
  </body>
</html>