<!--BREADCUM SECTION-->
<?php $this->load->view('incl/breadcumb');?>
<!--BREADCUM SECTION-->

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="boxContents store-detail">
                <div class="row store-detail_box">
                    <div class="col-md-6 border-right">
                        <div class="media">
                            <img src="<?=base_url();?>assets/img/other/gosend.jpg" width="120" class="mr-3 border rounded" alt="nama merchant">
                            <div class="media-body">
                                <div class="store-detail_title">
                                    <h1 class="mt-0 d-inline-block font-weight-bold text-dark h4 mr-2 vertical-align">Koperasi Teratai</h1>
                                    <span class="text-primary bg-light font-weight-bold fs14px p-1 rounded" data-toggle="tooltip" title="Toko ini dikelola langsung oleh PKK"><i class="fas fa-certificate"></i> Toko Resmi</span>
                                </div>
                                <div class="store-detail_desc">
                                    <p class="store-detail_desc_text">
                                        Belanja produk berkualitas di toko online terpercaya dari seller pkkmelati. Nikmati promo menarik dan layanan terbaik setiap hari!
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
<!--                        <div class="fs14px">Transaksi : <span class="font-weight-bold">1120</span> Kali</div>-->
                        <div class="store-detail_stats">
                            <table class="table table-borderless">
                                <tbody>
                                <tr class="fs14px text-muted">
                                    <td><span><i class="fas fa-map-marker-alt"></i> Lokasi</span></td>
                                    <td><span><i class="fas fa-box"></i> Produk</span></td>
                                    <td><span><i class="fas fa-exchange-alt"></i> Transaksi</span></td>
                                    <td><span><i class="fas fa-calendar-check"></i> Bergabung</span></td>
                                </tr>
                                <tr class="fs14px font-weight-bold">
                                    <td>Jakarta Barat</td>
                                    <td>123</td>
                                    <td>1.213</td>
                                    <td>12 Mei 2019</td>
                                </tr>
                                </tbody>
                            </table>


                            <div class="store-detail_stats_button">
                                <button class="btn btn-outline-info btn-sm w-50"><i class="fas fa-comments"></i> Kirim Pesan</button>

                                <div class="float-right">
                                    <span class="fs12px text-muted mr-3">Bagikan Toko :</span>
                                    <ul class="list-group list-group-horizontal-sm float-right">
                                        <li class="list-socmed_hori">
                                            <button class="btn btn-info btn-sm"><i class="fab fa-facebook-square"></i></button>
                                        </li>
                                        <li class="list-socmed_hori">
                                            <button class="btn btn-info btn-sm"><i class="fab fa-twitter-square"></i></button>
                                        </li>
                                        <li class="list-socmed_hori">
                                            <button class="btn btn-info btn-sm"><i class="fab fa-whatsapp-square"></i></button>
                                        </li>
                                        <li class="list-socmed_hori">
                                            <button class="btn btn-info btn-sm"><i class="fas fa-link"></i></button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<form>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item pl-0 pr-0 bg-transparent">
                                <div class="text-muted font-weight-bold">Catatan Penjual</div>
                                <div class="mt-2 bg-white">
                                    <p class="mb-0 p-2 fs14px border">
                                        Pembayaran di atas jam 16:00 WIB akan di kirim keesokan harinya. Barang 99% Ready stock, jadi langsung di order saja. Terimakasih.
                                    </p>
                                </div>
                            </li>
                            <li class="list-group-item pl-0 pr-0 bg-transparent">
                                <div class="text-muted font-weight-bold">Etalase</div>
                                <div class="p-1">
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="sayurbuah" checked>
                                        <label class="custom-control-label" for="sayurbuah">Sayur dan Buah</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="bahanmakan">
                                        <label class="custom-control-label" for="bahanmakan">Bahan Makanan</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="bajusayur">
                                        <label class="custom-control-label" for="bajusayur">Baju Sayur</label>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9 col-sm-12">
                        <div class="row">
                            <div class="col">
                                <label for="urutan" class="text-muted fs12px">Urutkan :</label>
                                <select class="custom-select mr-sm-2" id="urutan">
                                    <option selected>Paling Sesuai</option>
                                    <option value="1">Termahal</option>
                                    <option value="2">Termurah</option>
                                    <option value="3">Terlaris</option>
                                    <option value="4">Rating Tertinggi</option>
                                </select>
                            </div>
                            <div class="col">
                                <label for="cari_" class="text-muted fs12px">Cari Barang :</label>
                                <input type="text" class="form-control" id="cari_" placeholder="Cari Nama Barang...">
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-3 col-sm-6 mb-2">
                                <div class="product-grid3">
                                    <div class="product-image3">
                                        <a href="<?=site_url();?>product">
                                            <img class="pic-1" src="<?=base_url();?>assets/img/produk/testprod1.jpg">
                                            <!--                        <img class="pic-2" src="<?=base_url();?>assets/img/produk/testprod2.jpg">-->
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                            <li><a href="javascript:void(0);" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                        <span class="product-new-label">Terlaris</span>
                                    </div>
                                    <div class="product-content">
                                        <span class="title h3"><a href="<?=site_url();?>product">Beras Bakar Sagu</a></span>
                                        <div class="price">
                                            Rp60.000
                                            <span>Rp75.000</span>
                                        </div>
                                        <div class="product-product_rating">
                                            <div class="product-rating product-rating_container">
                                                <div class="product-rating__bg">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="product-rating__fg" style="width: 75%;">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </div>
                                            <span class="text-muted">(11)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 mb-2">
                                <div class="product-grid3">
                                    <div class="product-image3">
                                        <a href="<?=site_url();?>product">
                                            <img class="pic-1" src="<?=base_url();?>assets/img/produk/testprod2.jpg">
                                            <!--                        <img class="pic-2" src="<?=base_url();?>assets/img/produk/testprod2.jpg">-->
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                            <li><a href="javascript:void(0);" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                        <span class="product-new-label">Terlaris</span>
                                    </div>
                                    <div class="product-content">
                                        <span class="title h3"><a href="<?=site_url();?>product">Beras Bakar Sagu</a></span>
                                        <div class="price">
                                            Rp60.000
                                            <span>Rp75.000</span>
                                        </div>
                                        <div class="product-product_rating">
                                            <div class="product-rating product-rating_container">
                                                <div class="product-rating__bg">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="product-rating__fg" style="width: 75%;">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </div>
                                            <span class="text-muted">(11)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 mb-2">
                                <div class="product-grid3">
                                    <div class="product-image3">
                                        <a href="<?=site_url();?>product">
                                            <img class="pic-1" src="<?=base_url();?>assets/img/produk/testprod3.jpg">
                                            <!--                        <img class="pic-2" src="<?=base_url();?>assets/img/produk/testprod2.jpg">-->
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                            <li><a href="javascript:void(0);" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                        <span class="product-new-label">Terlaris</span>
                                    </div>
                                    <div class="product-content">
                                        <span class="title h3"><a href="<?=site_url();?>product">Beras Bakar Sagu</a></span>
                                        <div class="price">
                                            Rp60.000
                                            <span>Rp75.000</span>
                                        </div>
                                        <div class="product-product_rating">
                                            <div class="product-rating product-rating_container">
                                                <div class="product-rating__bg">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="product-rating__fg" style="width: 75%;">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </div>
                                            <span class="text-muted">(11)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 mb-2">
                                <div class="product-grid3">
                                    <div class="product-image3">
                                        <a href="<?=site_url();?>product">
                                            <img class="pic-1" src="<?=base_url();?>assets/img/produk/testprod4.jpg">
                                            <!--                        <img class="pic-2" src="<?=base_url();?>assets/img/produk/testprod2.jpg">-->
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                            <li><a href="javascript:void(0);" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                        <span class="product-new-label">Terlaris</span>
                                    </div>
                                    <div class="product-content">
                                        <span class="title h3"><a href="<?=site_url();?>product">Beras Bakar Sagu</a></span>
                                        <div class="price">
                                            Rp60.000
                                            <span>Rp75.000</span>
                                        </div>
                                        <div class="product-product_rating">
                                            <div class="product-rating product-rating_container">
                                                <div class="product-rating__bg">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="product-rating__fg" style="width: 75%;">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </div>
                                            <span class="text-muted">(11)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 mb-2">
                                <div class="product-grid3">
                                    <div class="product-image3">
                                        <a href="<?=site_url();?>product">
                                            <img class="pic-1" src="<?=base_url();?>assets/img/produk/testprod5.jpg">
                                            <!--                        <img class="pic-2" src="<?=base_url();?>assets/img/produk/testprod2.jpg">-->
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                            <li><a href="javascript:void(0);" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                        <span class="product-new-label">Terlaris</span>
                                    </div>
                                    <div class="product-content">
                                        <span class="title h3"><a href="<?=site_url();?>product">Beras Bakar Sagu</a></span>
                                        <div class="price">
                                            Rp60.000
                                            <span>Rp75.000</span>
                                        </div>
                                        <div class="product-product_rating">
                                            <div class="product-rating product-rating_container">
                                                <div class="product-rating__bg">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="product-rating__fg" style="width: 75%;">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </div>
                                            <span class="text-muted">(11)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 mb-2">
                                <div class="product-grid3">
                                    <div class="product-image3">
                                        <a href="<?=site_url();?>product">
                                            <img class="pic-1" src="<?=base_url();?>assets/img/produk/testprod6.jpg">
                                            <!--                        <img class="pic-2" src="<?=base_url();?>assets/img/produk/testprod2.jpg">-->
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                            <li><a href="javascript:void(0);" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                        <span class="product-new-label">Terlaris</span>
                                    </div>
                                    <div class="product-content">
                                        <span class="title h3"><a href="<?=site_url();?>product">Beras Bakar Sagu</a></span>
                                        <div class="price">
                                            Rp60.000
                                            <span>Rp75.000</span>
                                        </div>
                                        <div class="product-product_rating">
                                            <div class="product-rating product-rating_container">
                                                <div class="product-rating__bg">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="product-rating__fg" style="width: 75%;">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </div>
                                            <span class="text-muted">(11)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 mb-2">
                                <div class="product-grid3">
                                    <div class="product-image3">
                                        <a href="<?=site_url();?>product">
                                            <img class="pic-1" src="<?=base_url();?>assets/img/produk/testprod7.jpg">
                                            <!--                        <img class="pic-2" src="<?=base_url();?>assets/img/produk/testprod2.jpg">-->
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                            <li><a href="javascript:void(0);" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                        <span class="product-new-label">Terlaris</span>
                                    </div>
                                    <div class="product-content">
                                        <span class="title h3"><a href="<?=site_url();?>product">Beras Bakar Sagu</a></span>
                                        <div class="price">
                                            Rp60.000
                                            <span>Rp75.000</span>
                                        </div>
                                        <div class="product-product_rating">
                                            <div class="product-rating product-rating_container">
                                                <div class="product-rating__bg">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="product-rating__fg" style="width: 75%;">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </div>
                                            <span class="text-muted">(11)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 mb-2">
                                <div class="product-grid3">
                                    <div class="product-image3">
                                        <a href="<?=site_url();?>product">
                                            <img class="pic-1" src="<?=base_url();?>assets/img/produk/testprod8.jpg">
                                            <!--                        <img class="pic-2" src="<?=base_url();?>assets/img/produk/testprod2.jpg">-->
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                            <li><a href="javascript:void(0);" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                        <span class="product-new-label">Terlaris</span>
                                    </div>
                                    <div class="product-content">
                                        <span class="title h3"><a href="<?=site_url();?>product">Beras Bakar Sagu</a></span>
                                        <div class="price">
                                            Rp60.000
                                            <span>Rp75.000</span>
                                        </div>
                                        <div class="product-product_rating">
                                            <div class="product-rating product-rating_container">
                                                <div class="product-rating__bg">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="product-rating__fg" style="width: 75%;">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </div>
                                            <span class="text-muted">(11)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 mb-2">
                                <div class="product-grid3">
                                    <div class="product-image3">
                                        <a href="<?=site_url();?>product">
                                            <img class="pic-1" src="<?=base_url();?>assets/img/produk/testprod5.jpg">
                                            <!--                        <img class="pic-2" src="<?=base_url();?>assets/img/produk/testprod2.jpg">-->
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                            <li><a href="javascript:void(0);" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                        <span class="product-new-label">Terlaris</span>
                                    </div>
                                    <div class="product-content">
                                        <span class="title h3"><a href="<?=site_url();?>product">Beras Bakar Sagu</a></span>
                                        <div class="price">
                                            Rp60.000
                                            <span>Rp75.000</span>
                                        </div>
                                        <div class="product-product_rating">
                                            <div class="product-rating product-rating_container">
                                                <div class="product-rating__bg">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="product-rating__fg" style="width: 75%;">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </div>
                                            <span class="text-muted">(11)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 mb-2">
                                <div class="product-grid3">
                                    <div class="product-image3">
                                        <a href="<?=site_url();?>product">
                                            <img class="pic-1" src="<?=base_url();?>assets/img/produk/testprod6.jpg">
                                            <!--                        <img class="pic-2" src="<?=base_url();?>assets/img/produk/testprod2.jpg">-->
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                            <li><a href="javascript:void(0);" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                        <span class="product-new-label">Terlaris</span>
                                    </div>
                                    <div class="product-content">
                                        <span class="title h3"><a href="<?=site_url();?>product">Beras Bakar Sagu</a></span>
                                        <div class="price">
                                            Rp60.000
                                            <span>Rp75.000</span>
                                        </div>
                                        <div class="product-product_rating">
                                            <div class="product-rating product-rating_container">
                                                <div class="product-rating__bg">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="product-rating__fg" style="width: 75%;">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </div>
                                            <span class="text-muted">(11)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 mb-2">
                                <div class="product-grid3">
                                    <div class="product-image3">
                                        <a href="<?=site_url();?>product">
                                            <img class="pic-1" src="<?=base_url();?>assets/img/produk/testprod7.jpg">
                                            <!--                        <img class="pic-2" src="<?=base_url();?>assets/img/produk/testprod2.jpg">-->
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                            <li><a href="javascript:void(0);" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                        <span class="product-new-label">Terlaris</span>
                                    </div>
                                    <div class="product-content">
                                        <span class="title h3"><a href="<?=site_url();?>product">Beras Bakar Sagu</a></span>
                                        <div class="price">
                                            Rp60.000
                                            <span>Rp75.000</span>
                                        </div>
                                        <div class="product-product_rating">
                                            <div class="product-rating product-rating_container">
                                                <div class="product-rating__bg">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="product-rating__fg" style="width: 75%;">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </div>
                                            <span class="text-muted">(11)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 mb-2">
                                <div class="product-grid3">
                                    <div class="product-image3">
                                        <a href="<?=site_url();?>product">
                                            <img class="pic-1" src="<?=base_url();?>assets/img/produk/testprod2.jpg">
                                            <!--                        <img class="pic-2" src="<?=base_url();?>assets/img/produk/testprod2.jpg">-->
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                            <li><a href="javascript:void(0);" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                        <span class="product-new-label">Terlaris</span>
                                    </div>
                                    <div class="product-content">
                                        <span class="title h3"><a href="<?=site_url();?>product">Beras Bakar Sagu</a></span>
                                        <div class="price">
                                            Rp60.000
                                            <span>Rp75.000</span>
                                        </div>
                                        <div class="product-product_rating">
                                            <div class="product-rating product-rating_container">
                                                <div class="product-rating__bg">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="product-rating__fg" style="width: 75%;">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </div>
                                            <span class="text-muted">(11)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 mb-2">
                                <div class="product-grid3">
                                    <div class="product-image3">
                                        <a href="<?=site_url();?>product">
                                            <img class="pic-1" src="<?=base_url();?>assets/img/produk/testprod3.jpg">
                                            <!--                        <img class="pic-2" src="<?=base_url();?>assets/img/produk/testprod2.jpg">-->
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                            <li><a href="javascript:void(0);" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                        <span class="product-new-label">Terlaris</span>
                                    </div>
                                    <div class="product-content">
                                        <span class="title h3"><a href="<?=site_url();?>product">Beras Bakar Sagu</a></span>
                                        <div class="price">
                                            Rp60.000
                                            <span>Rp75.000</span>
                                        </div>
                                        <div class="product-product_rating">
                                            <div class="product-rating product-rating_container">
                                                <div class="product-rating__bg">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="product-rating__fg" style="width: 75%;">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </div>
                                            <span class="text-muted">(11)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 mb-2">
                                <div class="product-grid3">
                                    <div class="product-image3">
                                        <a href="<?=site_url();?>product">
                                            <img class="pic-1" src="<?=base_url();?>assets/img/produk/testprod5.jpg">
                                            <!--                        <img class="pic-2" src="<?=base_url();?>assets/img/produk/testprod2.jpg">-->
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                            <li><a href="javascript:void(0);" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                        <span class="product-new-label">Terlaris</span>
                                    </div>
                                    <div class="product-content">
                                        <span class="title h3"><a href="<?=site_url();?>product">Beras Bakar Sagu</a></span>
                                        <div class="price">
                                            Rp60.000
                                            <span>Rp75.000</span>
                                        </div>
                                        <div class="product-product_rating">
                                            <div class="product-rating product-rating_container">
                                                <div class="product-rating__bg">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="product-rating__fg" style="width: 75%;">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </div>
                                            <span class="text-muted">(11)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 mb-2">
                                <div class="product-grid3">
                                    <div class="product-image3">
                                        <a href="<?=site_url();?>product">
                                            <img class="pic-1" src="<?=base_url();?>assets/img/produk/testprod2.jpg">
                                            <!--                        <img class="pic-2" src="<?=base_url();?>assets/img/produk/testprod2.jpg">-->
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                            <li><a href="javascript:void(0);" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                        <span class="product-new-label">Terlaris</span>
                                    </div>
                                    <div class="product-content">
                                        <span class="title h3"><a href="<?=site_url();?>product">Beras Bakar Sagu</a></span>
                                        <div class="price">
                                            Rp60.000
                                            <span>Rp75.000</span>
                                        </div>
                                        <div class="product-product_rating">
                                            <div class="product-rating product-rating_container">
                                                <div class="product-rating__bg">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="product-rating__fg" style="width: 75%;">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </div>
                                            <span class="text-muted">(11)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 mb-2">
                                <div class="product-grid3">
                                    <div class="product-image3">
                                        <a href="<?=site_url();?>product">
                                            <img class="pic-1" src="<?=base_url();?>assets/img/produk/testprod8.jpg">
                                            <!--                        <img class="pic-2" src="<?=base_url();?>assets/img/produk/testprod2.jpg">-->
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                            <li><a href="javascript:void(0);" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                        <span class="product-new-label">Terlaris</span>
                                    </div>
                                    <div class="product-content">
                                        <span class="title h3"><a href="<?=site_url();?>product">Beras Bakar Sagu</a></span>
                                        <div class="price">
                                            Rp60.000
                                            <span>Rp75.000</span>
                                        </div>
                                        <div class="product-product_rating">
                                            <div class="product-rating product-rating_container">
                                                <div class="product-rating__bg">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="product-rating__fg" style="width: 75%;">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </div>
                                            <span class="text-muted">(11)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fas fa-chevron-left"></i></a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>