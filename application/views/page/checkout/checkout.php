DETAIL PEMBELIAN-->
<div class="container boxContents mt-135 mb-lg-4 pb-3">
    <div class="row">
        <div class="col-md-8 col-sm-12">
            <div class="product-detail_spec">
                <div class="product-detail_header font-weight-bold">
                    Detail Data Pembeli
                </div>
                <div class="checkout-detail_users">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><?php if(empty($this->session->userdata('nohp'))) { echo 'Login / Masuk';} else { echo 'Alamat Pengiriman';} ?></a>
                        </li>
                       <!--  <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Beli Tanpa Daftar</a>
                        </li> -->
                    </ul>

                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row checkout-detail_login">
                                <div class="col-12">
                                    <?php if(empty($this->session->userdata('nohp'))) { ?>
                                    <form action="javascript:void(0);" method="post" id="masuknohp">
                                        <div class="form-group">
                                            <label for="inpEmail">Nomor HP</label>
                                            <input type="text" class="form-control rounded-0" id="hp" name="hp" placeholder="Masukkan Nomor HP">
                                        </div>
                                        <button type="submit" class="btn btn-prod border-primary w-100">Masuk</button>
                                    </form>
                                    <?php } else { ?>
                                        <b><p><?=$user_alamat->namaPenerima;?></p></b>
                                        <p><?=$user_alamat->hpPenerima;?></p>
                                        <p><?=$user_alamat->detailAlamat;?></p>
                                        <p><?=$user_alamat->namaKelurahan.', '.$user_alamat->namaKecamatan.', '.$user_alamat->namaKota.', '.$user_alamat->kodePos;?></p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
<!-- 
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row checkout-detail_guest">
                                <div class="col-12">
                                    <form>
                                        <div class="form-group">
                                            <label for="guestName">Nama Lengkap</label>
                                            <input type="text" class="form-control rounded-0" id="guestName" >
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="guestEmail">Alamat Email</label>
                                                <input type="email" class="form-control rounded-0" id="guestEmail" >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="guestNum">No. Telepon/Handphone</label>
                                                <input type="number" class="form-control rounded-0" id="guestNum" >
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="guestProv">Provinsi</label>
                                                <input type="email" class="form-control rounded-0" id="guestProv" >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="guestTown">Kota/Kabupaten</label>
                                                <input type="password" class="form-control rounded-0" id="guestTown" >
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="guestDistric">Kecamatan</label>
                                                <input type="email" class="form-control rounded-0" id="guestDistric" >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="guestPos">Kode Pos</label>
                                                <input type="password" class="form-control rounded-0" id="guestPos" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="guestAddress">Alamat Lengkap</label>
                                            <textarea class="form-control" id="guestAddress" rows="3"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <p>Pilih Lokasi <span class="text-muted fs12px">(Untuk delivery)</span></p>
                                            <a href="#" class="text-danger font-weight-bold">Pilih Koordinat di Peta</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
                <hr>
                <div class="product-detail_header font-weight-bold mt-lg-5">
                    Detail Belanjaan
                </div>
                <div>
                <ul class="cart-detail_list list-group list-group-flush">
                    <?php
                    $cart = $this->cart->contents();
                    foreach ($cart as $item):
                    ?>
                    <!--LOOPING FOR PRODUCT CART HERE-->
                    <li class="cart-detail_lis_item list-group-item">
                        <div class="row">
                            <!-- <div class="col-1">
                                <div class="form-check">
                                    <input class="form-check-input position-static" type="checkbox" ">
                                </div>
                            </div> -->
                            <div class="col-3">
                                <a href="<?=base_url().''.$item['slug'];?>" class="mr-25"><img src="<?=base_url().'assets/img/produk/'.$item['img'];?>" class="border rounded shadow-sm w-100" alt="..."></a>
                            </div>
                            <div class="col-9">
                                <div class="media mb-10">
                                    <div class="media-body">
                                        <span class="mt-0 mb-1 h6 d-block"><a href="<?=base_url().''.$item['slug'];?>" class="text-dark"><?=$item['name'];?></a></span>
                                        <span class="font-weight-bold">Rp <?=number_format($item['subtotal'], 0,",","."); ?></span>
                                        <div class="input-group w-50">
                                  <!-- <span class="input-group-btn">
                                      <button type="button" class="btn btn-outline-info rounded-0 btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                                          <span class="fa fa-minus"></span>
                                      </button>
                                  </span> -->
                                            <input type="number" name="qty" class="form-control input-number" value="<?=$item['qty'];?>" min="1" max="10" step="1" disabled>
                                            <span class="input-group-btn">
                                  <!-- <button type="button" class="btn btn-outline-info rounded-0 btn-number" data-type="plus" data-field="quant[1]">
                                      <span class="fa fa-plus"></span>
                                  </button>
                                </span> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <!--LOOPING FOR PRODUCT CART HERE-->
                    <?php endforeach; ?>
                </ul>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-12">
            <form class="form-horizontal" action="<?=site_url('action_main/toCheckout')?>" method="post">
            <div class="product-detail_spec">
                <div class="product-detail_header font-weight-bold">
                    Ringkasan Belanja
                </div>
                <?php
                    // Create form and send all values in "shopping/update_cart" function.
                    $grand_total = 0;
                    $i = 1;

                    foreach ($cart as $item):
                    $grand_total = $grand_total + $item['subtotal'];
                    endforeach;
                    $total = $grand_total+$kodeUnik;
                    ?>
                <ul class="list-group cart-detail_pricingin">
                    <li class="list-group-item cart-detail_pricingin_list">
                        <div class="cart-detail_pricingin_list_label">
                            <span class="text-left float-left clearfix">Total Harga Barang</span>
                        </div>
                        <div class="text-right">
                            Rp. <?=number_format($grand_total, 0,",","."); ?>
                        </div>
                    </li>
                    <!-- <li class="list-group-item cart-detail_pricingin_list">
                        <div class="cart-detail_pricingin_list_label">
                            <span class="text-left float-left clearfix">Ongkos Kirim</span>
                        </div>
                        <div class="text-right">
                            Rp. 14.000
                        </div>
                    </li> -->
                    <li class="list-group-item cart-detail_pricingin_list">
                        <div class="cart-detail_pricingin_list_label">
                            <span class="text-left float-left clearfix">Kode Unik</span>
                        </div>
                        <div class="text-right">
                            + Rp. <?=$kodeUnik;?>
                        </div>
                    </li>
                </ul>
                <hr>
                <div class="cart-detail_pricingin_list_labelin">
                    <span class="text-left float-left clearfix font-weight-bold">Total Dibayar</span>
                </div>
                <div class="text-right font-weight-bold">
                    Rp. <?=number_format($total, 0,",","."); ?>
                </div>

                <!-- <div class="product-detail_header font-weight-bold mt-lg-5">
                    Pilih Metode Pembayaran
                </div>
                <ul class="list-group cart-detail_pricingin">
                    <li class="list-group-item cart-detail_pricingin_list">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="payTf" name="customRadio" class="custom-control-input">
                            <label class="custom-control-label fs14px" for="payTf">Transfer Bank</label>
                            <img src="<?=base_url();?>assets/img/other/bankdki.jpg" class="img-fluid ml-2" width="50">
                        </div>
                    </li>
                    <li class="list-group-item cart-detail_pricingin_list">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="payCod" name="customRadio" class="custom-control-input">
                            <label class="custom-control-label fs14px" for="payCod">Bayar di Tempat</label>
                            <img src="<?=base_url();?>assets/img/other/gosend.jpg" class="img-fluid ml-2" width="30">
                        </div>
                    </li>
                </ul> -->
                <hr>
                <input type="hidden" name="nama" value="<?=$user_alamat->namaPenerima;?>">
                <input type="hidden" name="noHP" value="<?=$user_alamat->hpPenerima;?>">
                <input type="hidden" name="kodeUnik" value="<?=$kodeUnik;?>">
                <input type="hidden" name="alamatTujuan" value="<?=$user_alamat->detailAlamat.' '.$user_alamat->namaKelurahan.', '.$user_alamat->namaKecamatan.', '.$user_alamat->namaKota.'. '.$user_alamat->kodePos;?>">
                <input type="hidden" name="totalTransfer" value="<?=$total;?>">
                <button type="submit" class="btn btn-prod product-detail_button_buynow">Buat Pesanan</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!--DETAIL PEMBELIAN