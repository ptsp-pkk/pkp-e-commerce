<!--BREADCUM SECTION-->
<?php $this->load->view('incl/breadcumb');?>
<!--BREADCUM SECTION-->

<div class="container">
    <div class="row">
        <div class="col-12">
            <p class="h5">Hasil pencarian <span class="font-weight-bold text-primary">"<?=$key;?>"</span></p>
            <hr>
        </div>
    </div>
</div>

<form>
    <div class="container">
   
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-md-3 col-sm-12 res-desk">
                        <div class="product-detail_header font-weight-bold">
                            <i class="fas fa-filter"></i> Filter Saring
                        </div>

                        <ul class="list-group list-group-flush">
                            <li class="list-group-item pl-0 pr-0 bg-transparent">
                                <div class="text-muted font-weight-bold">Kategori</div>
                                <div class="p-1">
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="sayurbuah" checked>
                                        <label class="custom-control-label" for="sayurbuah">Sayur dan Buah</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="bahanmakan">
                                        <label class="custom-control-label" for="bahanmakan">Bahan Makanan</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="bajusayur">
                                        <label class="custom-control-label" for="bajusayur">Baju Sayur</label>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item pl-0 pr-0 bg-transparent">
                                <div class="text-muted font-weight-bold">Lokasi</div>
                                <div class="p-1">
                                    <div class="form-group">
                                        <select class="custom-select mr-sm-2" id="prov">
                                            <option selected>Pilih Provinsi</option>
                                            <option value="1">DKI Jakarta</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select class="custom-select mr-sm-2" id="daerah">
                                            <option selected>Pilih Daerah</option>
                                            <option value="1">Jakarta Barat</option>
                                            <option value="2">Jakarta Timur</option>
                                            <option value="3">Jakarta Selatan</option>
                                            <option value="4">Jakarta Utara</option>
                                        </select>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item pl-0 pr-0 bg-transparent">
                                <div class="text-muted font-weight-bold">Harga</div>
                                <div class="p-1">
                                    <form>
                                        <div class="input-group mb-2 mr-sm-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">Rp</div>
                                            </div>
                                            <input type="text" class="form-control" id="input-min" placeholder="Minimal">
                                        </div>
                                        <div class="input-group mb-2 mr-sm-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">Rp</div>
                                            </div>
                                            <input type="text" class="form-control" id="input-max" placeholder="Maksimal">
                                        </div>
                                        <button type="submit" class="btn btn-info w-100">Cari</button>
                                    </form>
                                </div>
                            </li>
                            <li class="list-group-item pl-0 pr-0 bg-transparent">
                                <div class="text-muted font-weight-bold">Kondisi</div>
                                <div class="p-1">
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="baru" checked>
                                        <label class="custom-control-label" for="baru">Baru</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="bekas">
                                        <label class="custom-control-label" for="bekas">Bekas</label>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item pl-0 pr-0 bg-transparent">
                                <div class="text-muted font-weight-bold">Rating</div>
                                <div class="p-1">
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="bintang5">
                                        <label class="custom-control-label" for="bintang5"><i class="fa fa-star text-primary"></i> 5</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="bintang4">
                                        <label class="custom-control-label" for="bintang4"><i class="fa fa-star text-primary"></i> 4</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="bintang3">
                                        <label class="custom-control-label" for="bintang3"><i class="fa fa-star text-primary"></i> 3</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="bintang2">
                                        <label class="custom-control-label" for="bintang2"><i class="fa fa-star text-primary"></i> 2</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="bintang1">
                                        <label class="custom-control-label" for="bintang1"><i class="fa fa-star text-primary"></i> 1</label>
                                    </div>
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="bintangan" checked>
                                        <label class="custom-control-label" for="bintangan">Lihat Semua</label>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9 col-sm-12">
                        <div class="form-group w-25 res-desk">
                            <label for="urutan" class="text-muted fs12px">Urutkan :</label>
                            <select class="custom-select mr-sm-2" id="urutan">
                                <option selected>Paling Sesuai</option>
                                <option value="1">Harga Tinggi ke Rendah</option>
                                <option value="2">Harga Rendah ke Tinggi</option>
                                <option value="3">Terlaris</option>
                                <option value="4">Rating Tertinggi</option>
                            </select>
                        </div>
                        <hr>
                        <div class="row">
                            <?php

                            foreach ($produk as $newProduct) {
                                $rating        = $this->front->sum_data('rating','review',array('idProduk'=>$newProduct->id)); 
                                $count_rating  = $this->front->count_data('review',array('idProduk'=>$newProduct->id));
                                $persen_rating = $rating/(5*$count_rating)*100;   
                                $harga_diskon  = $newProduct->harga-($newProduct->harga*($newProduct->persenDiskon/100));
                                ?>
                                <div class="col-lg-3 col-sm-6 mb-2">
                                    <form id="form<?=$newProduct->id;?>" method="post" action="<?php echo base_url();?>action_main/addCart" method="post" accept-charset="utf-8">
                                        <div class="product-grid3">
                                            <div class="product-image3">
                                                <a href="<?=base_url().'product/'.$newProduct->slug;?>">
                                                    <img class="pic-1" src="<?=base_url('assets/img/produk/').$newProduct->fileName?>">
                                                </a>
                                                <ul class="social">
                                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                                    <li><a href="#" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang" onclick="document.getElementById('form<?=$newProduct->id;?>').submit();"><i class="fa fa-shopping-cart"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="product-content">
                                                <span class="title h3"><a href="<?=base_url().'product/'.$newProduct->slug;?>" style="display:block;"><?=$newProduct->judul;?></a></span>
                                                <?php if($newProduct->persenDiskon==0){?>
                                                    <div class="price" style="display:block;">
                                                        Rp <?=number_format($newProduct->harga, 0,",","."); ?>
                                                    </div>
                                                <?php }else{ ?>
                                                    <div class="price" style="display:block;">
                                                        Rp <?=number_format($harga_diskon, 0,",","."); ?>
                                                        <span>Rp <?=number_format($newProduct->harga, 0,",","."); ?></span>
                                                    </div>
                                                <?php } ?>
                                                <?php if($rating>0){?>
                                                    <div class="product-product_rating">
                                                        <div class="product-rating product-rating_container">
                                                            <div class="product-rating__bg">
                                                                <span class="fa fa-star"></span>
                                                                <span class="fa fa-star"></span>
                                                                <span class="fa fa-star"></span>
                                                                <span class="fa fa-star"></span>
                                                                <span class="fa fa-star"></span>
                                                            </div>
                                                            <div class="product-rating__fg" style="width: <?=$persen_rating;?>%;">
                                                                <span class="fa fa-star"></span>
                                                                <span class="fa fa-star"></span>
                                                                <span class="fa fa-star"></span>
                                                                <span class="fa fa-star"></span>
                                                                <span class="fa fa-star"></span>
                                                            </div>
                                                        </div>
                                                        <span class="text-muted">(<?=$count_rating;?>)</span>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <input type="hidden" name="id" value="<?=$newProduct->id;?>" />
                                        <input type="hidden" name="nama" value="<?=$newProduct->judul;?>" />
                                        <input type="hidden" name="harga" value="<?php if($newProduct->persenDiskon==0){ echo $newProduct->harga; }else{ echo $harga_diskon; } ?>" />
                                        <input type="hidden" name="gambar" value="<?=$newProduct->fileName;?>" />
                                        <input type="hidden" name="slug" value="<?=$newProduct->slug?>" />
                                        <input type="hidden" name="redirectto" value="<?=current_url();?>" />
                                        <input type="hidden" name="qty" value="1" />
                                    </form>
                                </div>
                                <?php 
                            }
                            ?>
                        </div>


                        <!-- <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fas fa-chevron-left"></i></a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a>
                                </li>
                            </ul>
                        </nav> -->

                        <?= $this->pagination->create_links('dada'); ?>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</form>