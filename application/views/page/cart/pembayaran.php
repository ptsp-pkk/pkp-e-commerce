        <div class="product-detail_header font-weight-bold">
        	Ringkasan Belanja
        </div>
        <ul class="list-group cart-detail_pricingin">
        	<?php
        	$cart = $this->cart->contents();
                    // Create form and send all values in "shopping/update_cart" function.
        	$grand_total = 0;
        	$i = 1;

        	foreach ($cart as $item):
        		$grand_total = $grand_total + $item['subtotal'];
        		?>
        		<!--LOOPING FOR PRICE CART HERE-->
        		<li class="list-group-item cart-detail_pricingin_list">
        			<div class="cart-detail_pricingin_list_label">
        				<span class="text-left float-left clearfix">Rp <?=number_format($item['price'], 0,",","."); ?></span>
        			</div>
        			<div class="text-right">
        				<?=$item['qty'];?>x
        			</div>
        		</li>
        		<!--LOOPING FOR PRICE CART HERE-->
        	<?php endforeach; ?>
        </ul>
        <hr>
        <div class="cart-detail_pricingin_list_labelin">
        	<span class="text-left float-left clearfix font-weight-bold">Total Belanja</span>
        </div>
        <div class="text-right font-weight-bold">
        	Rp <?=number_format($grand_total, 0,",","."); ?>
        </div>
        <a href="<?=base_url();?>checkout" class="btn btn-prod product-detail_button_buynow mt-4 <?php if($grand_total==0){echo'disabled';}?>">Bayar Sekarang</a>