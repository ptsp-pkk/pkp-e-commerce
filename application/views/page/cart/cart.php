BREADCUM SECTION-->
<?php $this->load->view('incl/breadcumb');?>
<!--BREADCUM SECTION-->

<!--PRODUCT LIST SECTION-->
<div class="container boxContents mb-lg-4 pb-3">
    <div class="row">
        <div class="col-md-8 col-sm-12">
            <div class="product-detail_spec">
                <div class="product-detail_header font-weight-bold" style="padding-left: 0;">
                    Daftar Keranjang Belanja
                </div>
                <form action="<?=base_url()?>action_main/updateCart" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <?php
                    if ($cart = $this->cart->contents())
                    {
                       ?>

                       <ul class="cart-detail_list list-group list-group-flush">
                        <?php
                        foreach ($cart as $item):
                            ?>
                            <input type="hidden" name="cart[<?php echo $item['id'];?>][id]" value="<?php echo $item['id'];?>" />
                            <input type="hidden" name="cart[<?php echo $item['id'];?>][rowid]" value="<?php echo $item['rowid'];?>" />
                            <input type="hidden" name="cart[<?php echo $item['id'];?>][name]" value="<?php echo $item['name'];?>" />
                            <input type="hidden" name="cart[<?php echo $item['id'];?>][price]" value="<?php echo $item['price'];?>" />
                            <input type="hidden" name="cart[<?php echo $item['id'];?>][img]" value="<?php echo $item['img'];?>" />
                            <input type="hidden" name="cart[<?php echo $item['id'];?>][slug]" value="<?php echo $item['slug'];?>" />
                            <input type="hidden" name="cart[<?php echo $item['id'];?>][qty]" value="<?php echo $item['qty'];?>" />
                            <input type="hidden" name="redirectto" value="<?=base_url().'cart/';?>" />
                            <!--LOOPING FOR PRODUCT CART HERE-->
                            <li class="cart-detail_lis_item list-group-item">
                                <div class="row">
                            <!-- <div class="col-1">
                                <div class="form-check">
                                    <input class="form-check-input position-static" type="checkbox" ">
                                </div>
                            </div> -->
                            <div class="col-1">
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" onclick="test('<?=$item['id']?>')" value="<?=$item['subtotal'];?>" id="cart<?=$item['id']?>">
                                    <label class="custom-control-label" for="cart<?=$item['id']?>"></label>
                                </div>
                            </div>
                            <div class="col-3">
                                <a href="<?=base_url().''.$item['slug'];?>" class="mr-25"><img src="<?=base_url().'assets/img/produk/'.$item['img'];?>" class="border rounded shadow-sm w-100" alt="..."></a>
                            </div>
                            <div class="col-7" style="padding: 0;">
                                <div class="media mb-10">
                                    <div class="media-body">
                                        <span class="mt-0 mb-1 h6 d-block"><a href="<?=base_url().''.$item['slug'];?>" class="text-dark"><?=$item['name'];?></a></span>
                                        <span class="font-weight-bold">Rp <?=number_format($item['subtotal'], 0,",","."); ?></span>
                                        <br>
                                        <div style="display: inline-flex;margin-top: 10px;">
                                            <a href="<?= site_url("action_main/deleteCart/".$item['rowid'])?>"class="btn btn-trash"><i class="pe-7s-trash"></i></a>
                                            <div class="input-group" style="width: 70%;">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn  btn-outline-info rounded-0 btn-number"  data-type="minus" data-field="quant[2]">
                                                        <span class="fa fa-minus"></span>
                                                    </button>
                                                </span>
                                                <input type="text" name="quant[2]" class="form-control input-number" value="<?=$item['qty'];?>" min="1" max="100">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-outline-info rounded-0 btn-number" data-type="plus" data-field="quant[2]">
                                                        <span class="fa fa-plus"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-12" style="display: inline-flex;">
                                <a href="<?php echo base_url()?>action_main/deleteCart/<?php echo $item['rowid'];?>"><button class="btn btn-trash"><i class="pe-7s-trash"></i></button></a>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-outline-info rounded-0 btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                                            <span class="fa fa-minus"></span>
                                        </button>
                                    </span>
                                    <input type="number" name="qty" class="form-control input-number" value="<?=$item['qty'];?>" min="1" max="10" step="1">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-outline-info rounded-0 btn-number" data-type="plus" data-field="quant[1]">
                                            <span class="fa fa-plus"></span>
                                        </button>
                                    </span>
                                </div>
                            </div> -->
                        </div>
                    </li>
                    <!--LOOPING FOR PRODUCT CART HERE-->
                <?php endforeach; ?>
            </ul>
            <?php
        }
        else
        {
            echo "<h3>Keranjang Belanja masih kosong</h3>"; 
        }   
        ?>
    </form>
</div>
</div>

<div class="col-md-4 ring-blj">
    <div class="product-detail_spec" id="cartnya">
        <div class="product-detail_header font-weight-bold">
            Ringkasan Belanja
        </div>
        <ul class="list-group cart-detail_pricingin">
            <?php
                    // Create form and send all values in "shopping/update_cart" function.
            $grand_total = 0;
            $i = 1;

            foreach ($cart as $item):
                $grand_total = $grand_total + $item['subtotal'];
                ?>
                <!--LOOPING FOR PRICE CART HERE-->
                <li class="list-group-item cart-detail_pricingin_list">
                    <div class="cart-detail_pricingin_list_label">
                        <span class="text-left float-left clearfix">Rp <?=number_format($item['price'], 0,",","."); ?></span>
                    </div>
                    <div class="text-right">
                        <?=$item['qty'];?>x
                    </div>
                </li>
                <!--LOOPING FOR PRICE CART HERE-->
            <?php endforeach; ?>
        </ul>
        <hr>
        <div class="cart-detail_pricingin_list_labelin">
            <span class="text-left float-left clearfix font-weight-bold">Total Belanja</span>
        </div>
        <div class="text-right font-weight-bold">
            Rp <?=number_format($grand_total, 0,",","."); ?>
        </div>
        <a href="<?=base_url();?>checkout" class="btn btn-prod product-detail_button_buynow mt-4 <?php if($grand_total==0){echo'disabled';}?>">Bayar Sekarang</a>
    </div>
</div>
</div>
</div>
<!--PRODUCT LIST SECTION