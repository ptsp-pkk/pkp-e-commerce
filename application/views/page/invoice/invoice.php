<div class="container mt-100 mb-lg-4 pb-3">
    <div class="row">
        <div class="col-8">
            <div class="product-detail_header bg-white shadow font-weight-bold">
                <i class="fas fa-info-circle"></i> Daftar Pembelian
            </div>

            <ul class="list-group list-group-flush shadow p-0 mb-4">
                <li class="list-group-item">
                    <!--MERCHANT NAME-->
                    <span class="h5 text-danger"> <i class="fas fa-store-alt"></i> Toko Makmur </span>
                </li>
                <li class="list-group-item">
                    <!--ID TRANSAKSI-->
                    <span class="text-muted fs12px">NOMOR TRANSAKSI</span>
                    <span class="d-block">327642347236427</span>
                </li>
                <li class="list-group-item">
                    <span class="text-muted fs12px">STATUS PENGIRIMAN</span>
                    <!--<span class="d-block">Diterima & Selesai</span>-->
                    <a class="d-block text-primary font-weight-bold" data-toggle="collapse" href="#prosesP" role="button" aria-expanded="false" aria-controls="collapseExample" style="letter-spacing: 1px;">
                        DITERIMA & SELESAI <i class="fa fa-chevron-circle-down float-right"></i>
                    </a>
                    <div class="collapse" id="prosesP">
                        <div class="card card-body p-0">
                            <ul class="tl-proses">
                                <li>
                                    <span class="text-primary font-weight-bold">Pesanan di Proses</span>
                                    <span class="d-block fs12px font-weight-bold">20 Feb 2019, 13:10 PM</span>
                                    <p>Pesanan anda sedang di proses Toko.</p>
                                </li>
                                <li>
                                    <span class="text-danger font-weight-bold">Pesanan Gagal di Proses</span>
                                    <span class="d-block fs12px font-weight-bold">22 Feb 2019, 13:10 PM</span>
                                    <p>Pesanan anda gagal diproses karena stok barang kosong.</p>
                                </li>
                                <li>
                                    <span class="text-primary font-weight-bold">Pesanan di Kirim</span>
                                    <span class="d-block fs12px font-weight-bold">21 Feb 2019, 13:10 PM</span>
                                    <p>Pesanan anda sedang di kirim , jangan lupa untuk konfirmasi jika barang sudah diterima.</p>
                                </li>
                                <li>
                                    <span class="text-danger font-weight-bold">Pesanan Gagal di Kirim</span>
                                    <span class="d-block fs12px font-weight-bold">22 Feb 2019, 13:10 PM</span>
                                    <p>Pesanan anda sedang di gagal kirim karena terjadi kendala dijalan. Pesanan dikembalikan.</p>
                                </li>
                                <li>
                                    <span class="text-primary font-weight-bold">Pesanan Diterima</span>
                                    <span class="d-block fs12px font-weight-bold">25 Feb 2019, 13:10 PM</span>
                                    <p>Pesanan anda sudah diterima. Jangan lupa untuk memberikan rating barang.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <span class="text-muted">BARANG</span>
                    <img src="<?=base_url();?>assets/img/produk/testprod7.jpg" width="100" class="float-left img-fluid">
                    <span class="d-block fs12px font-weight-bold">Beras harapan indonesia 5 Kg</span>
                    <span class="d-block fs12px">Jumlah : 1</span>
                    <span class="d-block fs12px">Berat : 5 Kg</span>
                    <span class="d-block fs12px">Harga Barang : Rp. 700.000</span>
                </li>
                <li class="list-group-item">
                    <div class="mt-2">
                        <a href="#" class="w-100 btn btn-outline-info">Konfirmasi Barang di Terima</a>
                        <!--JIKA BARANG SUDAH DI TERIMA DAN DIKLIK MAKAN AKAN BERUBAH MENJADI BUTTON DI BAWAH INI-->
                        <a href="<?=site_url();?>beri-rating" class="w-100 btn btn-outline-info">Beri Rating Barang</a>
                    </div>
                </li>
            </ul>
            <ul class="list-group list-group-flush shadow p-0 mb-4">
                <li class="list-group-item">
                    <!--MERCHANT NAME-->
                    <span class="h5 text-danger"> <i class="fas fa-store-alt"></i> Elektornik Jaya </span>
                </li>
                <li class="list-group-item">
                    <!--ID TRANSAKSI-->
                    <span class="text-muted fs12px">NOMOR TRANSAKSI</span>
                    <span class="d-block">327642347236427</span>
                </li>
                <li class="list-group-item">
                    <span class="text-muted fs12px">STATUS PENGIRIMAN</span>
                    <!--<span class="d-block">Diterima & Selesai</span>-->
                    <a class="d-block text-primary font-weight-bold" data-toggle="collapse" href="#prosesP2" role="button" aria-expanded="false" aria-controls="collapseExample" style="letter-spacing: 1px;">
                        DITERIMA & SELESAI <i class="fa fa-chevron-circle-down float-right"></i>
                    </a>
                    <div class="collapse" id="prosesP2">
                        <div class="card card-body p-0">
                            <ul class="tl-proses">
                                <li>
                                    <span class="text-primary font-weight-bold">Pesanan di Proses</span>
                                    <span class="d-block fs12px font-weight-bold">20 Feb 2019, 13:10 PM</span>
                                    <p>Pesanan anda sedang di proses Toko.</p>
                                </li>
                                <li>
                                    <span class="text-danger font-weight-bold">Pesanan Gagal di Proses</span>
                                    <span class="d-block fs12px font-weight-bold">22 Feb 2019, 13:10 PM</span>
                                    <p>Pesanan anda gagal diproses karena stok barang kosong.</p>
                                </li>
                                <li>
                                    <span class="text-primary font-weight-bold">Pesanan di Kirim</span>
                                    <span class="d-block fs12px font-weight-bold">21 Feb 2019, 13:10 PM</span>
                                    <p>Pesanan anda sedang di kirim , jangan lupa untuk konfirmasi jika barang sudah diterima.</p>
                                </li>
                                <li>
                                    <span class="text-danger font-weight-bold">Pesanan Gagal di Kirim</span>
                                    <span class="d-block fs12px font-weight-bold">22 Feb 2019, 13:10 PM</span>
                                    <p>Pesanan anda sedang di gagal kirim karena terjadi kendala dijalan. Pesanan dikembalikan.</p>
                                </li>
                                <li>
                                    <span class="text-primary font-weight-bold">Pesanan Diterima</span>
                                    <span class="d-block fs12px font-weight-bold">25 Feb 2019, 13:10 PM</span>
                                    <p>Pesanan anda sudah diterima. Jangan lupa untuk memberikan rating barang.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <span class="text-muted">BARANG</span>
                    <img src="<?=base_url();?>assets/img/produk/testprod2.jpg" width="100" class="float-left img-fluid">
                    <span class="d-block fs12px font-weight-bold">Beras harapan indonesia 5 Kg</span>
                    <span class="d-block fs12px">Jumlah : 1</span>
                    <span class="d-block fs12px">Berat : 5 Kg</span>
                    <span class="d-block fs12px">Harga Barang : Rp. 700.000</span>
                </li>
                <li class="list-group-item">
                    <div class="mt-2">
                        <a href="#" class="w-100 btn btn-outline-info">Konfirmasi Barang di Terima</a>
                        <!--JIKA BARANG SUDAH DI TERIMA DAN DIKLIK MAKAN AKAN BERUBAH MENJADI BUTTON DI BAWAH INI-->
                        <a href="<?=site_url();?>beri-rating" class="w-100 btn btn-outline-info">Beri Rating Barang</a>
                    </div>
                </li>
            </ul>
            <ul class="list-group list-group-flush shadow p-0 mb-4">
                <li class="list-group-item">
                    <!--MERCHANT NAME-->
                    <span class="h5 text-danger"> <i class="fas fa-store-alt"></i> Koperasi PKK </span>
                </li>
                <li class="list-group-item">
                    <!--ID TRANSAKSI-->
                    <span class="text-muted fs12px">NOMOR TRANSAKSI</span>
                    <span class="d-block">327642347236427</span>
                </li>
                <li class="list-group-item">
                    <span class="text-muted fs12px">STATUS PENGIRIMAN</span>
                    <!--<span class="d-block">Diterima & Selesai</span>-->
                    <a class="d-block text-primary font-weight-bold" data-toggle="collapse" href="#prosesP3" role="button" aria-expanded="false" aria-controls="collapseExample" style="letter-spacing: 1px;">
                        DITERIMA & SELESAI <i class="fa fa-chevron-circle-down float-right"></i>
                    </a>
                    <div class="collapse" id="prosesP3">
                        <div class="card card-body p-0">
                            <ul class="tl-proses">
                                <li>
                                    <span class="text-primary font-weight-bold">Pesanan di Proses</span>
                                    <span class="d-block fs12px font-weight-bold">20 Feb 2019, 13:10 PM</span>
                                    <p>Pesanan anda sedang di proses Toko.</p>
                                </li>
                                <li>
                                    <span class="text-danger font-weight-bold">Pesanan Gagal di Proses</span>
                                    <span class="d-block fs12px font-weight-bold">22 Feb 2019, 13:10 PM</span>
                                    <p>Pesanan anda gagal diproses karena stok barang kosong.</p>
                                </li>
                                <li>
                                    <span class="text-primary font-weight-bold">Pesanan di Kirim</span>
                                    <span class="d-block fs12px font-weight-bold">21 Feb 2019, 13:10 PM</span>
                                    <p>Pesanan anda sedang di kirim , jangan lupa untuk konfirmasi jika barang sudah diterima.</p>
                                </li>
                                <li>
                                    <span class="text-danger font-weight-bold">Pesanan Gagal di Kirim</span>
                                    <span class="d-block fs12px font-weight-bold">22 Feb 2019, 13:10 PM</span>
                                    <p>Pesanan anda sedang di gagal kirim karena terjadi kendala dijalan. Pesanan dikembalikan.</p>
                                </li>
                                <li>
                                    <span class="text-primary font-weight-bold">Pesanan Diterima</span>
                                    <span class="d-block fs12px font-weight-bold">25 Feb 2019, 13:10 PM</span>
                                    <p>Pesanan anda sudah diterima. Jangan lupa untuk memberikan rating barang.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <span class="text-muted">BARANG</span>
                    <img src="<?=base_url();?>assets/img/produk/testprod3.jpg" width="100" class="float-left img-fluid">
                    <span class="d-block fs12px font-weight-bold">Beras harapan indonesia 5 Kg</span>
                    <span class="d-block fs12px">Jumlah : 1</span>
                    <span class="d-block fs12px">Berat : 5 Kg</span>
                    <span class="d-block fs12px">Harga Barang : Rp. 700.000</span>
                </li>
                <li class="list-group-item">
                    <div class="mt-2">
                        <a href="#" class="w-100 btn btn-outline-info">Konfirmasi Barang di Terima</a>
                        <!--JIKA BARANG SUDAH DI TERIMA DAN DIKLIK MAKAN AKAN BERUBAH MENJADI BUTTON DI BAWAH INI-->
                        <a href="<?=site_url();?>beri-rating" class="w-100 btn btn-outline-info">Beri Rating Barang</a>
                    </div>
                </li>
            </ul>
        </div>

        <div class="col-4 mt-3">
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn btn-link w-100 text-danger" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Informasi Tagihan
                            </button>
                        </h2>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body p-0">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <span class="text-muted fs12px">Status Tagihan</span>
                                    <span class="d-block">Dibayar (17 Nov 2018, 01:40 WIB)</span>
                                </li>
                                <li class="list-group-item">
                                    <span class="text-muted fs12px">Ongkos Kirim</span>
                                    <span class="d-block">Rp0</span>
                                </li>
                                <li class="list-group-item">
                                    <span class="text-muted fs12px">Kode pembayaran</span>
                                    <span class="d-block">Rp325</span>
                                </li>
                                <li class="list-group-item">
                                    <span class="text-muted fs12px font-weight-bold">Total Tagihan Transfer</span>
                                    <span class="d-block font-weight-bold">Rp700.325</span>
                                </li>
                                <li class="list-group-item">
                                    <span class="text-muted fs12px">Metode Pembayaran</span>
                                    <span class="d-block">Transfer Bank</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn btn-link w-100 text-danger collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                Alamat Pengiriman
                            </button>
                        </h2>
                    </div>

                    <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="">
                                <span class="d-block font-weight-bold font-italic">Billy Sumarna</span>
                                <span class="d-block">Jl. Jatijajar 5 RT43/11 No. 21 - Kel. Jatijajar</span>
                                <span class="d-block">Kecamatan Tapos, Kota Depok</span>
                                <span class="d-block">Jawa Barat, 16466</span>
                                <span class="d-block">0896535676736</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bg-white border rounded mt-4 p-10">
                <span class="text-danger font-weight-bold">Petunjuk Pembayaran</span>
                <hr>
                <p class="text-center">
                    <span class="h3 font-weight-bold text-lapak">Rp700.325</span>
                    <span class="d-block fs-12px">Transfer tepat hingga <strong>3 digit</strong> terakhir agar tidak menghambat proses verifikasi.</span>
                </p>
                <span class="text-muted fs12px">Harap transfer ke rekening dibawah ini, selain itu maka jangan melakukan transaksi.</span>
                <ul class="list-group list-group-flush mt-20">
                    <li class="list-unstyled">
                        <dl class="dl-logo-list">
                            <dt>
                                <img src="<?=base_url();?>assets/img/other/bankdki.jpg" class="img-fluid list-logo--payment" width="100" height="100">
                            </dt>
                            <dd>
                                <div>Bank DKI</div>
                                <div>
                                    <strong> 731 025 2527</strong>
                                </div>
                            </dd>
                        </dl>
                    </li>
                </ul>

                <div class="mt-20">
                    <a class="btn btn-md bg-primarys text-white d-block" href="https://api.whatsapp.com/send?phone=628132432543543&text=Halo,%20Saya%20mau%20konfirmasi%20Pembayaran...">Konfirmasi Pembayaran</a>
                </div>
            </div>
        </div>
    </div>
</div>
