<!--BREADCUM SECTION-->
<?php $this->load->view('incl/breadcumb');?>
<!--BREADCUM SECTION-->

<!--DETAIL PRODUCT-->
<div class="container boxContents mb-lg-4">
    <div class="row">
        <div class="col-12">
            <div class="product-detail_header font-weight-bold">
                Daftar Pembelian Kamu
            </div>

            <div class="order_list_filter mb-3">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="urutan1" class="text-muted fs12px">Urutkan :</label>
                            <select class="custom-select mr-sm-2" id="urutan1">
                                <option selected>Terbaru</option>
                                <option value="1">Nilai Terbesar</option>
                                <option value="2">Nilai terkecil</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="urutan2" class="text-muted fs12px">Status Transaksi :</label>
                            <select class="custom-select mr-sm-2" id="urutan2">
                                <option selected>Semua</option>
                                <option value="1">Berhasil</option>
                                <option value="2">Diproses</option>
                                <option value="3">Pengiriman</option>
                                <option value="4">Gagal</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="cari_" class="text-muted fs12px">Cari Transaksi :</label>
                            <input type="text" class="form-control" id="cari_" placeholder="Cari Nama Barang, Nama Toko...">
                        </div>
                    </div>
                </div>
            </div>
            <div class="order_list_content">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item text-dark">
                        <div class="card rounded-0">
                            <div class="card-header">
                                <a href="<?=site_url();?>invoice" class="float-right btn btn-outline-info btn-sm">Lihat Invoice</a>
                                <div class="fs12px text-muted">Tanggal : <span class="font-weight-bold">20 Mei 2019</span></div>
                                <div class="fs12px text-muted">Nomor Invoice : <span class="font-weight-bold">#PKK00168</span></div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="media">
                                            <img src="<?=base_url();?>assets/img/produk/testprod5.jpg" width="70" class="align-self-center mr-3" alt="Produk Leptop">
                                            <div class="media-body">
                                                <a href="<?=site_url();?>product" class="font-weight-bold mt-0">Laptop Uji Coba Sidang Skripsi i5</a>
                                                <p class="text-muted fs14px" style="line-height: 15px;">
                                                    <span class="d-block">Harga : Rp 4.123.000</span>
                                                    <span class="d-block">Qty : 1</span>
                                                    <span class="d-block">Berat : 2Kg</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="fs14px">Toko : <a href="#" class="">Elektronik Jaya</a></div>
                                        <div class="fs14px">Pengiriman : <span class="">Go-Jek Same Day</span></div>
                                        <div class="fs14px">Total Pembayaran : <span class="font-weight-bold">Rp 4.130.000</span></div>
                                        <div class="fs14px">Status Transaksi : <span class="font-weight-bold text-primary">Selesai</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item text-dark">
                        <div class="card rounded-0">
                            <div class="card-header">
                                <a href="<?=site_url();?>invoice" class="float-right btn btn-outline-info btn-sm">Lihat Invoice</a>
                                <div class="fs12px text-muted">Tanggal : <span class="font-weight-bold">20 Jan 2019</span></div>
                                <div class="fs12px text-muted">Nomor Invoice : <span class="font-weight-bold">#PKK00128</span></div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="media">
                                            <img src="<?=base_url();?>assets/img/produk/testprod3.jpg" width="70" class="align-self-center mr-3" alt="Produk Leptop">
                                            <div class="media-body">
                                                <a href="<?=site_url();?>product" class="font-weight-bold mt-0">Action Figure Iron Man I Love You 3000</a>
                                                <p class="text-muted fs14px" style="line-height: 15px;">
                                                    <span class="d-block">Harga : Rp 123.000</span>
                                                    <span class="d-block">Qty : 1</span>
                                                    <span class="d-block">Berat : 1Kg</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="fs14px">Toko : <a href="#" class="">Frans Mainan</a></div>
                                        <div class="fs14px">Pengiriman : <span class="">Go-Jek Same Day</span></div>
                                        <div class="fs14px">Total Pembayaran : <span class="font-weight-bold">Rp 213.000</span></div>
                                        <div class="fs14px">Status Transaksi : <span class="font-weight-bold text-warning">Dalam pengiriman</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item text-dark">
                        <div class="card rounded-0">
                            <div class="card-header">
                                <a href="<?=site_url();?>invoice" class="float-right btn btn-outline-info btn-sm">Lihat Invoice</a>
                                <div class="fs12px text-muted">Tanggal : <span class="font-weight-bold">1 Feb 2018</span></div>
                                <div class="fs12px text-muted">Nomor Invoice : <span class="font-weight-bold">#PKK0018</span></div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="media">
                                            <img src="<?=base_url();?>assets/img/produk/testprod7.jpg" width="70" class="align-self-center mr-3" alt="Produk Leptop">
                                            <div class="media-body">
                                                <a href="<?=site_url();?>product" class="font-weight-bold mt-0">Samsung Galaxy Semesta 99</a>
                                                <p class="text-muted fs14px" style="line-height: 15px;">
                                                    <span class="d-block">Harga : Rp 14.123.000</span>
                                                    <span class="d-block">Qty : 1</span>
                                                    <span class="d-block">Berat : 2Kg</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="fs14px">Toko : <a href="#" class="">Sita Cell</a></div>
                                        <div class="fs14px">Pengiriman : <span class="">Go-Jek Instant</span></div>
                                        <div class="fs14px">Total Pembayaran : <span class="font-weight-bold">Rp 14.200.000</span></div>
                                        <div class="fs14px">Status Transaksi : <span class="font-weight-bold text-danger">Diproses</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item text-dark">
                        <div class="card rounded-0">
                            <div class="card-header">
                                <a href="<?=site_url();?>invoice" class="float-right btn btn-outline-info btn-sm">Lihat Invoice</a>
                                <div class="fs12px text-muted">Tanggal : <span class="font-weight-bold">1 Feb 1998</span></div>
                                <div class="fs12px text-muted">Nomor Invoice : <span class="font-weight-bold">#PKK001</span></div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="media">
                                            <img src="<?=base_url();?>assets/img/produk/testprod1.jpg" width="70" class="align-self-center mr-3" alt="Produk Leptop">
                                            <div class="media-body">
                                                <a href="<?=site_url();?>product" class="font-weight-bold mt-0">Baju Soekarna Sablon Bandung</a>
                                                <p class="text-muted fs14px" style="line-height: 15px;">
                                                    <span class="d-block">Harga : Rp 43.000</span>
                                                    <span class="d-block">Qty : 1</span>
                                                    <span class="d-block">Berat : 1Kg</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="fs14px">Toko : <a href="#" class="">Kambing Distro</a></div>
                                        <div class="fs14px">Pengiriman : <span class="">Go-Jek Instant</span></div>
                                        <div class="fs14px">Total Pembayaran : <span class="font-weight-bold">Rp 50.000</span></div>
                                        <div class="fs14px">Status Transaksi : <span class="font-weight-bold text-dark">Menunggu Pembayaran</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--DETAIL PRODUCT-->