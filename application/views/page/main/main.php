<!--SECTION 1-->
<div class="container bg-white pb-10 pt-10 mt-100">
    <!-- SLIDE -->
    <?php $this->load->view('incl/slide');?>
    <!-- CLOSED SLIDE -->
    <!-- Swipe Slide -->
    <?php $this->load->view('incl/swipe_slide');?>
    <!-- Closed Swipe Slide -->
</div>
<!--SECTION 1-->

<!--SECTION 2-->
<div class="container bg-white pb-10 pt-10 mt-20">
    <div class="row">
        <div class="col-12">
            <div class="sections">
                <div class="section-header">
                    <div class="section-header_title">
                        <span>KATEGORI</span>
                        <a href="#" class="float-right text-muted fs12px mt-05">Semua Kategori</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-sm-12">
            <div class="section-content">
                <ul class="home-cat_group">
                    <li class="home-cat_list">
                        <div class="home-cat_list_group">
                            <a href="#" class="home-cat_list_item">
                                <img src="<?=base_url();?>assets/img/category/alat-listrik.png" class="img-fluid" width="150">
                            </a>
                            <a href="#" class="home-cat_list_item">
                                <img src="<?=base_url();?>assets/img/category/alat-pakai.png" class="img-fluid" width="150">
                            </a>
                        </div>
                    </li>
                    <li class="home-cat_list">
                        <div class="home-cat_list_group">
                            <a href="#" class="home-cat_list_item">
                                <img src="<?=base_url();?>assets/img/category/alat-rumah-tangga.png" class="img-fluid" width="150">
                            </a>
                            <a href="#" class="home-cat_list_item">
                                <img src="<?=base_url();?>assets/img/category/alat-tulis-kantor.png" class="img-fluid" width="150">
                            </a>
                        </div>
                    </li>
                    <li class="home-cat_list">
                        <div class="home-cat_list_group">
                            <a href="#" class="home-cat_list_item">
                                <img src="<?=base_url();?>assets/img/category/bahan-pokok.png" class="img-fluid" width="150">
                            </a>
                            <a href="#" class="home-cat_list_item">
                                <img src="<?=base_url();?>assets/img/category/bumbu.png" class="img-fluid" width="150">
                            </a>
                        </div>
                    </li>
                    <li class="home-cat_list">
                        <div class="home-cat_list_group">
                            <a href="#" class="home-cat_list_item">
                                <img src="<?=base_url();?>assets/img/category/deodorant.png" class="img-fluid" width="150">
                            </a>
                            <a href="#" class="home-cat_list_item">
                                <img src="<?=base_url();?>assets/img/category/kebutuhanl-bayi.png" class="img-fluid" width="150">
                            </a>
                        </div>
                    </li>
                    <li class="home-cat_list">
                        <div class="home-cat_list_group">
                            <a href="#" class="home-cat_list_item">
                                <img src="<?=base_url();?>assets/img/category/makanan-bayi.png" class="img-fluid" width="150">
                            </a>
                            <a href="#" class="home-cat_list_item">
                                <img src="<?=base_url();?>assets/img/category/minuman.png" class="img-fluid" width="150">
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-4 col-disc">
            <a href="#" class="home-cat_list_item">
                <img src="<?=base_url();?>assets/img/banner/testban1.jpg" class="img-fluid">
            </a>
        </div>
    </div>
</div>
<!--SECTION 2-->

<!--SECTION 3-->
<div class="container bg-white pb-10 pt-10 mt-20">
    <div class="row">
        <div class="col-12">

            <div class='helper'>
                <i class="fa fa-cart-plus"></i>
            </div>

            <div class="sections">
                <div class="section-header mb-3">
                    <div class="section-header_title">
                        <span>PRODUK TERBARU</span>
<!--                        <a href="#" class="float-right text-muted fs12px mt-05">Semua Kategori</a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?php
        $new         = $this->front->product_newHome();

        foreach ($new as $newProduct) {
        $rating        = $this->front->sum_data('rating','review',array('idProduk'=>$newProduct->id)); 
        $count_rating  = $this->front->count_data('review',array('idProduk'=>$newProduct->id));
        $persen_rating = $rating/(5*$count_rating)*100;   
        $harga_diskon  = $newProduct->harga-($newProduct->harga*($newProduct->persenDiskon/100));
        ?>
        <div class="col-lg-3 col-sm-6 mb-2">
            <form id="form<?=$newProduct->id;?>" method="post" action="<?php echo base_url();?>action_main/addCart" method="post" accept-charset="utf-8">
                <div class="product-grid3">
                    <div class="product-image3">
                        <a href="<?=base_url().'product/'.$newProduct->slug;?>">
                            <img class="pic-1" src="<?=base_url('assets/img/produk/').$newProduct->fileName?>">
                        </a>
                        <ul class="social">
                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang" onclick="document.getElementById('form<?=$newProduct->id;?>').submit();"><i class="fa fa-shopping-cart"></i></a></li>
                        </ul>
                    </div>
                    <div class="product-content">
                        <span class="title h3"><a href="<?=base_url().'product/'.$newProduct->slug;?>" style="display:block;"><?=$newProduct->judul;?></a></span>
                        <?php if($newProduct->persenDiskon==0){?>
                        <div class="price" style="display:block;">
                            Rp <?=number_format($newProduct->harga, 0,",","."); ?>
                        </div>
                        <?php }else{ ?>
                        <div class="price" style="display:block;">
                            Rp <?=number_format($harga_diskon, 0,",","."); ?>
                            <span>Rp <?=number_format($newProduct->harga, 0,",","."); ?></span>
                        </div>
                        <?php } ?>
                        <?php if($rating>0){?>
                        <div class="product-product_rating">
                            <div class="product-rating product-rating_container">
                                <div class="product-rating__bg">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                                <div class="product-rating__fg" style="width: <?=$persen_rating;?>%;">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                            </div>
                            <span class="text-muted">(<?=$count_rating;?>)</span>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <input type="hidden" name="id" value="<?=$newProduct->id;?>" />
                <input type="hidden" name="nama" value="<?=$newProduct->judul;?>" />
                <input type="hidden" name="harga" value="<?php if($newProduct->persenDiskon==0){ echo $newProduct->harga; }else{ echo $harga_diskon; } ?>" />
                <input type="hidden" name="gambar" value="<?=$newProduct->fileName;?>" />
                <input type="hidden" name="slug" value="<?=$newProduct->slug?>" />
                <input type="hidden" name="redirectto" value="<?=current_url();?>" />
                <input type="hidden" name="qty" value="1" />
            </form>
        </div>
        <?php 
        }
        ?>
    </div>
</div>
<!--SECTION 3-->

<!--SECTION 4-->
<div class="container-fluid bg-primarys pb-10 pt-10 mt-20">
    <div class="row">
        <div class="col-12">
            <div class="text-center">
                <p class="text-white mb-0">
                    <span class="h3 font-weight-bold">GRAND OPENING WEBSITE KOPERASI PKK TERMURAH & TERBAIK</span>
                    <span class="h5 d-block">DI REKOMENDASIKAN UNTUK DAERAH DKI JAKARTA</span>
                </p>
            </div>
        </div>
    </div>
</div>
<!--SECTION 4-->

<?php 
foreach ($category_product_limit as $k) { ?>
<!--SECTION 5-->
<div class="container bg-white pb-10 pt-10 mt-20">
    <div class="row">
        <div class="col-12">
            <div class="sections">
                <div class="section-header mb-3">
                    <div class="section-header_title">
                        <span><?=$k->namaKategori;?></span>
                        <a href="<?=base_url('kategori/').$k->slug;?>" class="float-right text-muted fs12px mt-05">Semua Produk</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?php
        $cat         = $this->front->product_catHome($k->id);

        foreach ($cat as $catProduct) {
        $rating        = $this->front->sum_data('rating','review',array('idProduk'=>$catProduct->id)); 
        $count_rating  = $this->front->count_data('review',array('idProduk'=>$catProduct->id));
        $persen_rating = $rating/(5*$count_rating)*100;   
        $harga_diskon  = $catProduct->harga-($catProduct->harga*($catProduct->persenDiskon/100));
        ?>
        <div class="col-lg-3 col-sm-6 mb-2">
            <form id="form<?=$catProduct->id;?>" method="post" action="<?php echo base_url();?>action_main/addCart" method="post" accept-charset="utf-8">
                <div class="product-grid3">
                    <div class="product-image3">
                        <a href="<?=base_url().'product/'.$catProduct->slug;?>">
                            <img class="pic-1" src="<?=base_url('assets/img/produk/').$catProduct->fileName?>">
                        </a>
                        <ul class="social">
                            <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                            <li><a href="#" onclick="document.getElementById('form<?=$catProduct->id;?>').submit();" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang"><i class="fa fa-shopping-cart"></i></a></li>
                        </ul>
                    </div>
                    <div class="product-content">
                        <span class="title h3"><a href="<?=base_url().'product/'.$catProduct->slug;?>" style="display:block;"><?=$catProduct->judul;?></a></span>
                        <?php if($catProduct->persenDiskon==0){?>
                        <div class="price" style="display:block;">
                            Rp <?=number_format($catProduct->harga, 0,",","."); ?>
                        </div>
                        <?php }else{ ?>
                        <div class="price" style="display:block;">
                            Rp <?=number_format($harga_diskon, 0,",","."); ?>
                            <span>Rp <?=number_format($catProduct->harga, 0,",","."); ?></span>
                        </div>
                        <?php } ?>
                        <?php if($rating>0){?>
                        <div class="product-product_rating">
                            <div class="product-rating product-rating_container">
                                <div class="product-rating__bg">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                                <div class="product-rating__fg" style="width: <?= $persen_rating;?>%;">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                            </div>
                            <span class="text-muted">(<?=$count_rating;?>)</span>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <input type="hidden" name="id" value="<?=$catProduct->id;?>" />
                <input type="hidden" name="nama" value="<?=$catProduct->judul;?>" />
                <input type="hidden" name="harga" value="<?php if ($catProduct->persenDiskon==0){ echo $catProduct->harga; }else{ echo $harga_diskon; }?>" />
                <input type="hidden" name="gambar" value="<?=$catProduct->fileName;?>" />
                <input type="hidden" name="slug" value="<?=$catProduct->slug?>" />
                <input type="hidden" name="redirectto" value="<?=current_url();?>" />
                <input type="hidden" name="qty" value="1" />
            </form>
        </div>
        <?php 
        }
        ?>
    </div>
</div>
<!--SECTION-->
<?php } ?>