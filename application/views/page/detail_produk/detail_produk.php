<!--BREADCUM SECTION-->
<?php $this->load->view('incl/breadcumb');?>
<!--BREADCUM SECTION-->

<!--DETAIL PRODUCT-->
<div class="container boxContents mb-lg-4">
    <div class="row">
        <div class="col-md-3 col-sm-12 col-img-detail">
            <form id="form<?=$newProduct->id;?>" method="post" action="<?php echo base_url();?>action_main/addCart" method="post" accept-charset="utf-8">
                <div class="product-detail_pict">
                    <div class="product-detail_pict_one mb-10">
                        <div>
                            <img src="<?=base_url('assets/img/produk/').$productDetail->fileName;?>" class="img-fluid w-100 product-detail_pict_one_img">
                        </div>
                    </div>
                    <div class="product-detail_pict_slide">
                        <div>
                            <img src="<?=base_url('assets/img/produk/').$productDetail->fileName;?>" class="img-fluid product-detail_pict_slide_img">
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-6 col-sm-12">
            <div class="row product-detail_sdesc">
                <h1 class="product-detail_sdesc_title w-100 mb-0"><?=$productDetail->judul;?></h1>
                <div class="res-harga">
                    <?php if($productDetail->persenDiskon==0) { ?>
                        <p class="product-detail_price" style="font-size: 20px;">Rp <?=number_format($productDetail->harga, 0,",","."); ?></p>
                    <?php }else{ ?>
                        <p class="product-detail_pricebreak" style="font-size: 16px;">Rp <?=number_format($productDetail->harga, 0,",","."); ?></p>
                        <p class="product-detail_price" style="font-size: 20px;">Rp <?=number_format($harga_diskon, 0,",","."); ?></p>  
                        <a href="<?=site_url();?>" class="btn btn-prod product-detail_button_addwhis"><i class="far fa-heart"></i></a>
                    <?php } ?>
                </div>
                <?php if($rating>0){?>
                    <div class="productt-product_rating">
                        <div class="product-rating product-rating_container">
                            <div class="product-rating__bg">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                            <div class="product-rating__fg" style="width: <?= $persen_rating;?>%;">
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                            </div>
                        </div>
                        <span class="text-muted">(<?=$count_rating;?>)</span>
                    </div>
                <?php } ?>

                <p class="product-detail_sdesc_short product-detail_sdesc_short res-mobile"><?=substr($productDetail->keterangan,0,150);?>...<a href="#detailPRODUCT">Selanjutnya</a></p>
                <div class="row col-category">
                    <div class="col-md-4 col-sm-12">
                        <label class="label-text" style="display: none;">Rincian Produk</label>
                        <p class="product-detail_sdesc_oth">Kategori : <a href="<?=site_url();?>"><?=$productDetail->namaKategori;?></a></p>
                    </div>
                    <div class="col-md-4 col-sm-12 col-hide">
                        <p class="product-detail_sdesc_oth">Berat : <?=$productDetail->beratProduk;?></p>
                    </div>
                    <div class="col-md-4 col-sm-12 col-hide">
                        <p class="product-detail_sdesc_oth">Stok : <?=$productDetail->stok;?></p>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <p class="product-detail_sdesc_oth">SKU : #<?=$productDetail->sku;?></p>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <p class="product-detail_sdesc_oth">Merek : <?=$productDetail->namaMerek;?></p>
                    </div>
                    <div class="col-md-4 col-sm-12 col-hide">
                        <p class="product-detail_sdesc_oth">Dikirim dari : <?=$productDetail->namaKota;?></p>
                    </div>
                </div>
            </div>

            <div class="row product-detail_pricing">
                <div class="col-6 col-harga">
                    <?php if($productDetail->persenDiskon==0) { ?>
                        <p class="product-detail_price">Rp <?=number_format($productDetail->harga, 0,",","."); ?></p>
                    <?php }else{ ?>
                        <p class="product-detail_pricebreak">Rp <?=number_format($productDetail->harga, 0,",","."); ?></p>
                        <p class="product-detail_price">Rp <?=number_format($harga_diskon, 0,",","."); ?></p>  
                    <?php } ?>
                </div>
                <div class="col-6 input-qty">
                    <span class="text-muted">Jumlah Pembelian :</span>
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-outline-info rounded-0 btn-number" disabled="disabled" data-type="minus" data-field="qty">
                                <span class="fa fa-minus"></span>
                            </button>
                        </span>
                        <input type="text" name="qty" class="form-control input-number" value="1" min="1" max="10">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-outline-info rounded-0 btn-number" data-type="plus" data-field="qty">
                                <span class="fa fa-plus"></span>
                            </button>
                        </span>
                    </div>
                </div>
            </div>

            <div class="row product-detail_info">
                <div class="col-3">
                    <div class="product-detail_info_item">
                        <i class="far fa-eye mr-2"></i>
                        <p class="text-center mb-0 fs14px"> Dilihat
                            <span class="product-detail_info_item_view">20rb</span>
                        </p>
                    </div>
                </div>
                <div class="col-3">
                    <div class="product-detail_info_item">
                        <i class="far fa-eye mr-2"></i>
                        <p class="text-center mb-0 fs14px"> Terjual
                            <span class="product-detail_info_item_sold">1.2rb</span>
                        </p>
                    </div>
                </div>
                <div class="col-3">
                    <div class="product-detail_info_item">
                        <i class="far fa-eye mr-2"></i>
                        <p class="text-center mb-0 fs14px"> Kondisi
                            <span class="product-detail_info_item_cond">Baru</span>
                        </p>
                    </div>
                </div>
                <div class="col-3">
                    <div class="product-detail_info_item">
                        <i class="far fa-eye mr-2"></i>
                        <p class="text-center mb-0 fs14px"> Min. Beli
                            <span class="product-detail_info_item_minbuy">1</span>
                        </p>
                    </div>
                </div>
            </div>
            <input type="hidden" name="id" value="<?=$productDetail->id;?>" />
            <input type="hidden" name="nama" value="<?=$productDetail->judul;?>" />
            <input type="hidden" name="harga" value="<?php if($productDetail->persenDiskon==0){ echo $productDetail->harga; }else{ echo $harga_diskon; } ?>" />
            <input type="hidden" name="gambar" value="<?=$productDetail->fileName;?>" />
            <input type="hidden" name="slug" value="<?=$productDetail->slug?>" />
            <input type="hidden" name="redirectto" value="<?=current_url();?>" />
            <div class="row product-detail_button">
                <div class="col-5">
                    <a href="<?=site_url();?>checkout" class="btn btn-prod product-detail_button_buynow">Beli Sekarang</a>
                </div>
                <div class="col-5">
                    <button type="submit" name="submit" class="btn btn-prod product-detail_button_addcart">Tambah ke Keranjang</button>
                </div>
                <div class="col-2">
                    <a href="<?=site_url();?>" class="btn btn-prod product-detail_button_addwhis"><i class="far fa-heart"></i></a>
                </div>
            </div>
            <div class="row col-hide">
                <div class="col-sm-12">
                    <label class="label-text" style="display: none;">Deskripsi</label>
                    <p style="font-size: 14px;"><?=$productDetail->keterangan;?></p>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-12">
                    <p class="text-muted fs12px">Jaminan 100% Aman. Uang pasti kembali. Sistem pembayaran bebas penipuan.</p>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="row p-10">
                <div class="col-md-12 border">
                    <span class="text-muted">Bagikan Produk</span>
                    <div class="mt-05 mb-10">
                        <ul class="list-group list-group-horizontal-sm">
                            <li class="list-socmed_hori">
                                <button class="btn btn-info"><i class="fab fa-facebook-square"></i></button>
                            </li>
                            <li class="list-socmed_hori">
                                <button class="btn btn-info"><i class="fab fa-twitter-square"></i></button>
                            </li>
                            <li class="list-socmed_hori">
                                <button class="btn btn-info"><i class="fab fa-whatsapp-square"></i></button>
                            </li>
                            <li class="list-socmed_hori">
                                <button class="btn btn-info"><i class="fas fa-link"></i></button>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 border mt-20">
                    <span class="text-muted">Informasi Penjual</span>
                    <div class="mt-05 mb-10">
                        <div class="media">
                            <img src="<?=base_url();?>assets/img/other/gosend.jpg" width="60" class="mr-3" alt="nama merchant">
                            <div class="media-body">
                                <a href="#" class="mt-0 font-weight-bold text-dark d-block"><?=$productDetail->namaStore; ?></a>
                                <span class="text-muted fs12px d-block">132 Transaksi | <?=$productDetail->namaKota; ?></span>
                                <?php if($productDetail->levelStore==1) { ?>
                                    <!--Informasi kalo dia resmi / seller-->
                                    <span class="text-success font-weight-bold fs14px d-block"><i class="fas fa-certificate"></i> Toko Resmi</span>
                                <?php } ?>
                                <span class="text-info font-weight-bold fs14px d-block"></i> Toko Seller</span>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-6">
                                <a href="<?=site_url();?>toko" class="btn btn-sm btn-outline-info w-100 fs12px"><i class="fas fa-store-alt"></i> Kunjungi</a>
                            </div>
                            <div class="col-6">
                                <a href="#" class="btn btn-sm btn-outline-info w-100 fs12px"><i class="fas fa-comments"></i> Kirim Pesan</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--DETAIL PRODUCT-->

<!--DESC PRODUCT-->
<div class="container boxContents mb-lg-4 res-mobile">
    <div class="row">
        <div class="col-12">
            <div class="product-detail_spec">
                <div class="product-detail_header">
                    Informasi Produk
                </div>
                <div class="product-detail_spec">
                    <div class="product-detail_spec_item">
                        <label class="product-detail_spec_item_label">Kategori</label>
                        <div class="product-detail_spec_item_cat d-flex align-items-center">
                            <a href="<?=site_url();?>" class="product-detail_spec_item_cat_a"><?=$productDetail->namaKategori;?></a>
                        </div>
                    </div>
                    <div class="product-detail_spec_item">
                        <label class="product-detail_spec_item_label">Berat</label>
                        <div class="product-detail_spec_item_cat d-flex align-items-center">
                            <span class="product-detail_spec_item_cat_a"><?=$productDetail->beratProduk;?> Gram</span>
                        </div>
                    </div>
                    <div class="product-detail_spec_item">
                        <label class="product-detail_spec_item_label">Merek</label>
                        <div class="product-detail_spec_item_cat d-flex align-items-center">
                            <span class="product-detail_spec_item_cat_a"><?=$productDetail->namaMerek;?></span>
                        </div>
                    </div>
                    <div class="product-detail_spec_item">
                        <label class="product-detail_spec_item_label">Stok</label>
                        <div class="product-detail_spec_item_cat d-flex align-items-center">
                            <span class="product-detail_spec_item_cat_a"><?=$productDetail->stok;?></span>
                        </div>
                    </div>
                    <div class="product-detail_spec_item">
                        <label class="product-detail_spec_item_label">Dikirim dari</label>
                        <div class="product-detail_spec_item_cat d-flex align-items-center">
                            <span class="product-detail_spec_item_cat_a"><?=$productDetail->namaKota;?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div id="detailPRODUCT" class="col-12">
            <div class="product-detail_spec pb-lg-4">
                <div class="product-detail_header">
                    Deskripsi Produk
                </div>
                <div class="product-detail_spec">
                    <div class="product-detail_desc">
                        <?=$productDetail->keterangan;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--DESC PRODUCT-->


<?php 
if($review_count>0) { ?>
    <!--REVIEW PRODUCT-->
    <div class="container boxContents mb-lg-4">
        <div class="row">
            <div class="col-lg-12 col-no-padding">
                <div class="product-detail_spec">
                    <div class="product-detail_header">
                        Ulasan Produk
                    </div>
                    <div class="product-detail_review">
                        <?php 
                        foreach($review as $r):
                            $persenRating=$r->rating/5*100;
                            ?>
                            <a href="<?=site_url();?>" class="product-detail_review_item">
                                <div class="card product-detail_review_item_card">
                                    <div class="card-body p-10">
                                        <div class="product-product_rating product-rating float-left">
                                            <div class="product-rating product-rating_container fs-12px">
                                                <div class="product-rating__bg">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="product-rating__fg" style="width: <?=$persenRating;?>%;">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mt-30">
                                            <span class="d-block fs-12px text-muted">Oleh <?=$r->nama.', '.date_format(date_create($r->createdDate), 'd M Y, H:i').' WIB';?></span>
                                            <span class="d-block text-dark"><?=$r->keterangan;?></span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--REVIEW PRODUCT-->
<?php } 

if($productRelated_count>1) {
    ?>
    <!--RELATED PRODUCTS-->
    <div class="container boxContents mb-lg-4">
        <div class="row">
            <div class="col-lg-12 col-no-padding">
                <div class="product-detail_spec">
                    <div class="product-detail_header">
                        Produk Terkait
                    </div>
                    <div class="product-detail_prodrelate">
                        <div class="row product-detail_prodrelate_group">
                            <?php
                            $related         = $this->front->productRelated($productDetail->idKategori,$productDetail->id);
                            foreach ($related as $relatedProduct) {
                                $rating        = $this->front->sum_data('rating','review',array('idProduk'=>$relatedProduct->id)); 
                                $count_rating  = $this->front->count_data('review',array('idProduk'=>$relatedProduct->id));
                                $persen_rating = $rating/(5*$count_rating)*100;   
                                $harga_diskon  = $relatedProduct->harga-($relatedProduct->harga*($relatedProduct->persenDiskon/100));
                                ?>
                                <div class="col-lg-3 col-sm-6 mb-2">
                                    <form id="form<?=$relatedProduct->id;?>" method="post" action="<?php echo base_url();?>action_main/addCart" method="post" accept-charset="utf-8">
                                        <div class="product-grid3">
                                            <div class="product-image3">
                                                <a href="<?=base_url().'product/'.$relatedProduct->slug;?>">
                                                    <img class="pic-1" src="<?=base_url('assets/img/produk/').$relatedProduct->fileName?>">
                                                </a>
                                                <ul class="social">
                                                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Langsung Beli"><i class="fa fa-shopping-bag"></i></a></li>
                                                    <li><a href="#" class="btnAddCart" data-toggle="tooltip" data-placement="top" title="Tambah ke Keranjang" onclick="document.getElementById('form<?=$relatedProduct->id;?>').submit();"><i class="fa fa-shopping-cart"></i></a></li>
                                                </ul>
                                            </div>
                                            <div class="product-content">
                                                <span class="title h3"><a href="<?=base_url().'product/'.$relatedProduct->slug;?>" style="display:block;"><?=$relatedProduct->judul;?></a></span>
                                                <?php if($relatedProduct->persenDiskon==0){?>
                                                    <div class="price" style="display:block;">
                                                        Rp <?=number_format($relatedProduct->harga, 0,",","."); ?>
                                                    </div>
                                                <?php }else{ ?>
                                                    <div class="price" style="display:block;">
                                                        Rp <?=number_format($harga_diskon, 0,",","."); ?>
                                                        <span>Rp <?=number_format($relatedProduct->harga, 0,",","."); ?></span>
                                                    </div>
                                                <?php } ?>
                                                <?php if($rating>0){?>
                                                    <div class="product-product_rating">
                                                        <div class="product-rating product-rating_container">
                                                            <div class="product-rating__bg">
                                                                <span class="fa fa-star"></span>
                                                                <span class="fa fa-star"></span>
                                                                <span class="fa fa-star"></span>
                                                                <span class="fa fa-star"></span>
                                                                <span class="fa fa-star"></span>
                                                            </div>
                                                            <div class="product-rating__fg" style="width: <?=$persen_rating;?>%;">
                                                                <span class="fa fa-star"></span>
                                                                <span class="fa fa-star"></span>
                                                                <span class="fa fa-star"></span>
                                                                <span class="fa fa-star"></span>
                                                                <span class="fa fa-star"></span>
                                                            </div>
                                                        </div>
                                                        <span class="text-muted">(<?=$count_rating;?>)</span>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <input type="hidden" name="id" value="<?=$relatedProduct->id;?>" />
                                        <input type="hidden" name="nama" value="<?=$relatedProduct->judul;?>" />
                                        <input type="hidden" name="harga" value="<?php if($relatedProduct->persenDiskon==0){ echo $relatedProduct->harga; }else{ echo $harga_diskon; } ?>" />
                                        <input type="hidden" name="gambar" value="<?=$relatedProduct->fileName;?>" />
                                        <input type="hidden" name="slug" value="<?=$relatedProduct->slug?>" />
                                        <input type="hidden" name="redirectto" value="<?=current_url();?>" />
                                        <input type="hidden" name="qty" value="1" />
                                    </form>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--RELATED PRODUCTS-->
    <?php } ?>