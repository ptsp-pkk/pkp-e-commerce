<div class="container mt-135">
    <div class="row mb-20">
        <div class="col-12">
            <div>
                <h3 class="text-primary">Beri ulasan untuk produk <span class="font-weight-bold">"Beras harapan indonesia 5 Kg"</span></h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <form id="ratingForm" method="POST">
                <div class="form-group">
                    <button type="button" class="btn btn-warning btn-sm rateButton" aria-label="Left Align">
                        <span class="fa fa-star text-white" aria-hidden="true"></span>
                    </button>
                    <button type="button" class="btn btn-default btn-grey btn-sm rateButton" aria-label="Left Align">
                        <span class="fa fa-star text-white" aria-hidden="true"></span>
                    </button>
                    <button type="button" class="btn btn-default btn-grey btn-sm rateButton" aria-label="Left Align">
                        <span class="fa fa-star text-white" aria-hidden="true"></span>
                    </button>
                    <button type="button" class="btn btn-default btn-grey btn-sm rateButton" aria-label="Left Align">
                        <span class="fa fa-star text-white" aria-hidden="true"></span>
                    </button>
                    <button type="button" class="btn btn-default btn-grey btn-sm rateButton" aria-label="Left Align">
                        <span class="fa fa-star text-white" aria-hidden="true"></span>
                    </button>
                    <input type="hidden" class="form-control" id="rating" name="rating" value="1">
                    <input type="hidden" class="form-control" id="itemId" name="itemId" value="">
                </div>
                <div class="form-group">
                    <label for="titleReview" class="font-weight-bold">Judul Ulasan</label>
                    <input type="text" class="form-control" id="titleReview" name="titleReview" required>
                </div>
                <div class="form-group">
                    <label for="commentReview" class="font-weight-bold">Bagaimana pendapat kamu?</label>
                    <textarea class="form-control" rows="5" id="commentReview" name="commentReview" required></textarea>
                </div>
                <div class="form-group float-right">
                    <a href="<?=site_url();?>invoice" class="btn btn-outline-info" style="height: 40px;cursor: pointer;">Batal</a>
                    <button type="submit" class="btn text-white btn-primary" style="height: 40px;cursor: pointer;" id="saveReview">Berikan Ulasan</button>
                </div>
            </form>
        </div>
    </div>
</div>