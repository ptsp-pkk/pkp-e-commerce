<!--BREADCUM SECTION-->
<?php $this->load->view('incl/breadcumb');?>
<!--BREADCUM SECTION-->

<!--PRODUCT LIST SECTION-->
<div class="container boxContents mb-lg-4 pb-3">
    <div class="row">
        <div class="col-md-12">
            <div class="product-detail_header font-weight-bold text-header">
                Daftar Produk Keinginan
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="media p-3 media-wish">
                <img src="<?=base_url();?>assets/img/produk/testprod4.jpg" alt="#" class="mr-3 img-wish col-wish-res-img">
                <div class="media-body col-wish-res-text">
                    <h4 class="h6">Stiker Dinding Digoo LED Wall Clock Alarm Clock Digital 3D White Snooze 12/24 Hour Display USB</h4>
                    <span class="font-weight-bold" style="color: #dc3545;">Rp70.000</span>
                    <div class="mt-3">
                        <button class="btn btn-trash"><i class="pe-7s-trash"></i></button>
                        <button class="btn btn-cart btn-primary">+<i class="pe-7s-cart"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="media p-3 media-wish">
                <img src="<?=base_url();?>assets/img/produk/testprod1.jpg" alt="#" class="mr-3 img-wish col-wish-res-img">
                <div class="media-body col-wish-res-text">
                    <h4 class="h6"> Digital 3D White Snooze 12/24 Hour Display USB</h4>
                    <span class="font-weight-bold" style="color: #dc3545;">Rp70.000</span>
                    <div class="mt-3">
                        <button class="btn btn-trash"><i class="pe-7s-trash"></i></button>
                        <button class="btn btn-cart btn-primary">+<i class="pe-7s-cart"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
<!--PRODUCT LIST SECTION -->