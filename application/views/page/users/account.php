<!--BREADCUM SECTION-->
<?php $this->load->view('incl/breadcumb');?>
<!--BREADCUM SECTION-->
<style type="text/css">
    .header-wrapper {
        background: linear-gradient(90deg,#395599,#41b7c7);
        flex-basis: 100%;
        flex-grow: 1;
        position: relative;
        margin: auto;
        padding: 12px 18px 12px;
        overflow: hidden;
    }
    .header-wrapper * {
        color: #fff;

    }
    .header-wrapper .title {
        font-size: 15px;
        font-weight: 300;
    }
    .header-wrapper .note {
        color: #ffd14a;
        font-size: 15px;
        margin-bottom: 3px;
        font-weight: bold;
    }
    .btn-ubah{
        border-color: #fff;
        background-color:transparent;
        color: #fff;
        float: right;
        margin-top: -8px;
        font-weight: bold;
    }
    .list-group-item{
        border: 2px solid rgba(0, 0, 0, 0.1);
    }
    .list-akun{
        border: solid 1px #ddd;
        border-right: none;
        border-left: none;
        border-radius: 0;
        font-size: 16px;
        padding: 0px 15px 0 15px;
        height: 55px;
        line-height: 55px;
        max-width: 100%;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
    }
    .list-akun i.icon{
        padding-right: 13px;
        font-size: 20px;
    }
    .list-keluar{
        margin-top:20px;
        border:solid 1px #ddd;
        border-right: none;
        border-left: none;
        border-radius: 0;
        text-align: center;
        color: #ef2136;
    }
    .icon-right{
        float: right;
        line-height: 1.7;
        font-size: 30px;
    }

</style>
<div class="mt-100">
    <div class="header-wrapper" style="border-bottom: solid 1px #ddd;">
        <div class="note">RIZKI</div>
        <div class="title">rizki1398@gmail.com <button class="btn btn-danger btn-ubah">Ubah Profil</button></div>
        <div class="title">085714668923</div>
    </div>
    <label style="font-weight: 600;color: rgba(0, 0, 0, 0.7);padding-left: 15px;margin-top: 20px;">Akun</label>
    <div class="list-group">
        <a href="#" class="list-group-item list-akun">
            <i class="pe-7s-repeat icon"></i> Daftar Transaksi <i class="pe-7s-angle-right icon-right"></i>
        </a>
        <a href="#" class="list-group-item list-akun"><i class="pe-7s-culture icon"></i> Toko Saya <i class="pe-7s-angle-right icon-right"></i></a>
        <a href="#" class="list-group-item list-akun"><i class="pe-7s-box1 icon"></i> Barang Dijual <i class="pe-7s-angle-right icon-right"></i></a>
    </div>
    <div class="list-group">
        <a href="<?=site_url('min/login/logout');?>" class="list-group-item list-keluar">Keluar</a>
    </div>
</div>