<!--BREADCUM SECTION-->
<?php $this->load->view('incl/breadcumb');?>
<!--BREADCUM SECTION-->

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="boxContents store-detail">
                <h1 class="h2 font-weight-bold text-muted mb-0">Bantuan Kami</h1>
                <span class="d-block fs14px text-muted">Pilih topik yang sesuai dengan masalah Anda</span>
                <hr>
                <div class="row">
                    <div class="col-md-2 text-center">
                        <a href="#">
                            <div class="btn-helpp">
                            <span class="fa-stack fa-2x">
                              <i class="fas fa-circle fa-stack-2x"></i>
                              <i class="fas fa-exchange-alt fa-stack-1x fa-inverse"></i>
                            </span>
                                <span class="d-block mt-1 font-weight-bold">Cara Pembelian</span>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2 text-center">
                        <a href="#">
                            <div class="btn-helpp">
                            <span class="fa-stack fa-2x">
                              <i class="fas fa-circle fa-stack-2x"></i>
                              <i class="fas fa-money-bill-wave fa-stack-1x fa-inverse"></i>
                            </span>
                                <span class="d-block mt-1 font-weight-bold">Cara Pembayaran</span>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2 text-center">
                        <a href="#">
                            <div class="btn-helpp">
                            <span class="fa-stack fa-2x">
                              <i class="fas fa-circle fa-stack-2x"></i>
                              <i class="fas fa-box fa-stack-1x fa-inverse"></i>
                            </span>
                                <span class="d-block mt-1 font-weight-bold">Pesanan Saya</span>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2 text-center">
                        <a href="#">
                            <div class="btn-helpp">
                            <span class="fa-stack fa-2x">
                              <i class="fas fa-circle fa-stack-2x"></i>
                              <i class="fas fa-truck fa-stack-1x fa-inverse"></i>
                            </span>
                                <span class="d-block mt-1 font-weight-bold">Pengiriman</span>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2 text-center">
                        <a href="#">
                            <div class="btn-helpp">
                            <span class="fa-stack fa-2x">
                              <i class="fas fa-circle fa-stack-2x"></i>
                              <i class="fas fa-user fa-stack-1x fa-inverse"></i>
                            </span>
                                <span class="d-block mt-1 font-weight-bold">Akun Saya</span>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2 text-center">
                        <a href="<?=site_url();?>bantuan/pertanyaan">
                            <div class="btn-helpp">
                            <span class="fa-stack fa-2x">
                              <i class="fas fa-circle fa-stack-2x"></i>
                              <i class="far fa-question-circle fa-stack-1x fa-inverse"></i>
                            </span>
                                <span class="d-block mt-1 font-weight-bold">QnA</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>