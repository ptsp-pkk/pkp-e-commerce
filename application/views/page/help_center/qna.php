<!--BREADCUM SECTION-->
<?php $this->load->view('incl/breadcumb');?>
<!--BREADCUM SECTION-->

<div class="container-fluid">
    <div class="row mb-3">
        <div class="col-12">
            <div class="boxContents store-detail">
                <a href="#sendQuestion" class="float-right btn btn-outline-info font-weight-bold">Ajukan Pertanyaan</a>
                <h1 class="h2 font-weight-bold text-muted mb-0">Pertanyaan dan Jawaban</h1>
                <span class="d-block fs14px text-muted">Berbagai pertanyaan dan jawaban yang sering diajukan.</span>
                <hr>
                <div class="row">
                    <div class="col-3">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Bagaimana cara menghubungi kurir Go-Jek?</a>
                            <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Syarat dan Ketentuan Pengiriman</a>
                            <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Cek Status Pengiriman</a>
                            <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Ganti Password Akun</a>
                        </div>
                    </div>
                    <div class="col-9">
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                <div class="text-helpp_answer">
                                    <p>
                                        <b>Go-Jek</b> merupakan layanan kurir yang memberlakukan service Instant dan Same Day Delivery. Apabila Anda ingin mengaktifkan <b>Go-Jek</b> sebagai layanan pengiriman dengan sistem pick up, Anda diharapkan membaca dan memahami syarat dan ketentuan yang berlaku sebelum menggunakan layanan kurir yang terkait.
                                    </p>

                                    <p>Anda dapat menghubungi Customer Service <b>Go-Jek</b> melalui Call Center <b>Go-Jek: (021) 50233200</b></p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">...</div>
                            <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">...</div>
                            <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="sendQuestion">
        <div class="col-12">
            <div class="boxContents store-detail">
                <h1 class="h2 font-weight-bold text-muted mb-0">Ajukan Pertanyaan Anda</h1>
                <span class="d-block fs14px text-muted">Harap gunakan bahasa yang baik dan benar.</span>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <form id="qnaForm" method="POST">
                            <div class="form-group">
                                <label for="titleReview" class="font-weight-bold">Pertanyaan</label>
                                <input type="text" class="form-control" id="titleReview" name="titleReview" required>
                            </div>
                            <div class="form-group">
                                <label for="commentReview" class="font-weight-bold">Detail Masalah</label>
                                <textarea class="form-control" rows="5" id="commentReview" name="commentReview" required></textarea>
                            </div>
                            <div class="form-group float-right">
                                <button type="submit" class="btn text-white btn-primary" style="height: 40px;cursor: pointer;" id="postQna">Kirim Pertanyaan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>