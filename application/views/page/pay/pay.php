<div class="container boxContents mt-135 mb-lg-4 pb-3">
    <div class="row">
        <?php 
        $buktitf=get_data('transaksi',array('nomorTransaksi' => $pay->nomorTransaksi),'buktiTransfer');
        if(empty($buktitf)) {?>
        <div class="col-12">
            <div class="product-detail_header font-weight-bold">
                <i class="fas fa-info-circle"></i> Informasi Penting
            </div>
            <ul>
                <li>Jangan lakukan pembayaran dengan <b class="text-primary">nominal yang berbeda</b> dengan yang tertera pada tagihan kamu.</li>
                <li>Jangan lakukan transfer <b class="text-primary">di luar nomor rekening</b> dibawah.</li>
            </ul>
        </div>
        <?php } else { ?>
            <div class="col-12">
                <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading">Selamat!</h4>
                    <p>Upload Bukti Transaksi mu telah berhasil.</p>
                    <hr>
                    <p class="mb-0">Sekarang transaksi mu sedang proses verifikasi pembayaran. Terima kasih</p>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<?php 
if(empty($buktitf)){ ?>
<div class="container boxContents mt-20 mb-lg-4 pb-3">
    <div class="row">
        <div class="col-12">
            <div class="product-detail_header text-center">
                <p class="mb-0">Nomor Transaksi Pembayaran Anda :
                    <a href="#" class="d-block text-primary font-weight-bold h4">#<?=$pay->nomorTransaksi;?></a>
                </p>
            </div>

            <div class="text-center">
                <span class="text-muted font-weight-bold">Pembayaran via Transfer bank</span>

                <p class="mt-3">
                    <span class="font-weight-bold">Total yang Harus di Bayar :</span>
                    <span class="d-block h2 text-primary font-weight-bold mt-2">Rp. <?=number_format($pay->totalTransfer, 0,",",".");?></span>
                    <span class="d-block fs12px text-danger">* Harap Transfer tepat sesuai dengan total transfer agar tidak menghambat proses verifikasi</span>
                </p>

                <div class="mt-4">
                    <ul class="list-group list-group-flush">
                        <li class="list-unstyled">
                            <dl class="dl-logo-list">
                                <dt>
                                    <img src="<?=base_url();?>assets/img/other/bankdki.jpg" class="img-fluid list-logo--payment" width="100" height="100">
                                </dt>
                                <dd>
                                    <div>Bank DKI</div>
                                    <div class="font-weight-bold">993274328646</div>
                                    <div>PKK Commerce</div>
                                </dd>
                            </dl>
                        </li>
                    </ul>
                </div>


                <div class="product-detail_header text-center mt-5">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#buktiTransfer">
                        Upload Bukti Transfer
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<?php echo form_open_multipart('action_main/do_upload');?>
<div class="modal fade" id="buktiTransfer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Form Upload</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label for="exampleFormControlFile1">Upload Bukti Transfer</label>
            <input type="file" name="userfile" size="20" class="form-control-file" required>
            <input type="hidden" name="id_transaksi" value="<?=$pay->nomorTransaksi;?>"> 
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Upload</button>
      </div>
    </div>
  </div>
</div>
</form>
<?php } ?>