<!-- side nav -->
<div id="overlaySide" href="javascript:void(0)" onclick="closeNav()" class="overlay-side"></div>
<div id="mySidenav" class="sidenav"> 
    <div class="sidebar-header" style="background: #eee;">
        <h4>Filter</h4>
    </div>  
    <div class="sidebar-menu">
        <div class="col-sm-12">
            <ul class="list-group list-group-flush">
                <li class="list-group-item pl-0 pr-0 bg-transparent">
                    <div class="text-muted font-weight-bold">Kategori</div>
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="sayurbuah" checked>
                        <label class="custom-control-label" for="sayurbuah">Sayur dan Buah</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="bahanmakan">
                        <label class="custom-control-label" for="bahanmakan">Bahan Makanan</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="bajusayur">
                        <label class="custom-control-label" for="bajusayur">Baju Sayur</label>
                    </div>
                </li>
                <li class="list-group-item pl-0 pr-0 bg-transparent">
                    <div class="text-muted font-weight-bold">Lokasi</div>
                    <div class="form-group">
                        <select class="custom-select mr-sm-2" id="prov">
                            <option selected>Pilih Provinsi</option>
                            <option value="1">DKI Jakarta</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="custom-select mr-sm-2" id="daerah">
                            <option selected>Pilih Daerah</option>
                            <option value="1">Jakarta Barat</option>
                            <option value="2">Jakarta Timur</option>
                            <option value="3">Jakarta Selatan</option>
                            <option value="4">Jakarta Utara</option>
                        </select>
                    </div>
                </li>
                <li class="list-group-item pl-0 pr-0 bg-transparent">
                    <div class="text-muted font-weight-bold">Harga</div>
                    <form>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Rp</div>
                            </div>
                            <input type="text" class="form-control" id="input-min" placeholder="Minimal">
                        </div>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Rp</div>
                            </div>
                            <input type="text" class="form-control" id="input-max" placeholder="Maksimal">
                        </div>
                        <!-- <button type="submit" class="btn btn-info w-100">Cari</button> -->
                    </form>
                </li>
                <li class="list-group-item pl-0 pr-0 bg-transparent">
                    <div class="text-muted font-weight-bold">Rating</div>
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="bintang5">
                        <label class="custom-control-label" for="bintang5"><i class="fa fa-star text-primary"></i> 5</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="bintang4">
                        <label class="custom-control-label" for="bintang4"><i class="fa fa-star text-primary"></i> 4</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="bintang3">
                        <label class="custom-control-label" for="bintang3"><i class="fa fa-star text-primary"></i> 3</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="bintang2">
                        <label class="custom-control-label" for="bintang2"><i class="fa fa-star text-primary"></i> 2</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="bintang1">
                        <label class="custom-control-label" for="bintang1"><i class="fa fa-star text-primary"></i> 1</label>
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="bintangan" checked>
                        <label class="custom-control-label" for="bintangan">Lihat Semua</label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <button class="btn btn-danger btn-block" id="resetFilter" style="bottom: 0;position: absolute;border-radius: 0;width: 50%;">Atur Ulang</button>
    <button class="btn btn-primary btn-block" id="terapkanFilter" style="bottom: 0;position: absolute;border-radius: 0;width: 50%;right: 0;">Terapkan</button>
</div>
<div id="mySidenavAtur" class="sidenav"> 
    <div class="sidebar-header" style="background: #eee;">
        <h4>Atur Berdasarkan</h4>
    </div>  
    <div class="sidebar-menu">
        <div class="col-sm-12">
            <ul class="list-group list-group-flush">
                <li class="list-group-item pl-0 pr-0 bg-transparent">
                    <div class="text-muted font-weight-bold">Kondisi</div>
                    <div class="p-1">
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="checkbox" class="custom-control-input" id="baru" checked>
                            <label class="custom-control-label" for="baru">Baru</label>
                        </div>
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="checkbox" class="custom-control-input" id="bekas">
                            <label class="custom-control-label" for="bekas">Bekas</label>
                        </div>
                    </div>
                </li>
                <li class="list-group-item pl-0 pr-0 bg-transparent">
                    <div class="text-muted font-weight-bold">Urutkan</div>
                    <div class="p-1">
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="checkbox" class="custom-control-input" id="tinggi" checked>
                            <label class="custom-control-label" for="tinggi">Harga Tinggi ke Rendah</label>
                        </div>
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="checkbox" class="custom-control-input" id="rendah">
                            <label class="custom-control-label" for="rendah">Harga Rendah ke Tinggi</label>
                        </div>
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="checkbox" class="custom-control-input" id="terlaris">
                            <label class="custom-control-label" for="terlaris">Terlaris</label>
                        </div>
                        <div class="custom-control custom-checkbox mr-sm-2">
                            <input type="checkbox" class="custom-control-input" id="rating">
                            <label class="custom-control-label" for="rating">Rating Tertinggi</label>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <button class="btn btn-danger btn-block" id="resetAtur" style="bottom: 0;position: absolute;border-radius: 0;width: 50%;">Atur Ulang</button>
    <button class="btn btn-primary btn-block" id="terapkanAtur" style="bottom: 0;position: absolute;border-radius: 0;width: 50%;right: 0;">Terapkan</button>
</div>
<!-- end side nav -->

<div class="fixed-top">
    <!--TOP BAR-->
    <nav class="navbar topBar p-0 bg-grey res-desk">
        <div class="container">
            <ul class="list-inline float-left hidden-sm hidden-xs res-desk">
                <li><span class="text-muted"> Belanja Puas, Harga Pas!</span></li>
            </ul>
            <ul class="topBarNav float-right res-desk">
                <li>
                    <a href="#" class="">
                        <i class="fas fa-truck-moving mr-05"></i>
                        <span class="hidden-xs">Lacak Pengiriman</span>
                    </a>
                </li>
                <li>
                    <a href="<?=site_url();?>bantuan" class="">
                        <i class="fas fa-question-circle mr-05"></i>
                        <span class="hidden-xs">Pusat Bantuan</span>
                    </a>
                </li>
                <?php error_reporting(0); 
                if (empty($this->session->userdata('nohp'))){ ?>
                    <li>
                        <a href="#" data-toggle="modal" data-target="#login">
                            <span class="text-primary font-weight-bold">Masuk</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" data-toggle="modal" data-target="#register">
                            <span class="text-danger font-weight-bold">Daftar</span>
                        </a>
                    </li>
                <?php }else{ ?>
                    <li>
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Selamat Datang, 
                                <span class="font-weight-bold">
                                    <?php if(empty($this->session->userdata('nama'))) { 
                                        echo $this->session->userdata('nohp'); 
                                    }else{ 
                                        echo $this->session->userdata('nama'); 
                                    } ?>        
                                </span>
                            </a>

                            <div class="dropdown-menu fs14px rounded-0 pt-0 pb-0" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="<?=site_url();?>order-list?sesi=login"><i class="fas fa-exchange-alt"></i> Daftar Transaksi</a>
                                <a class="dropdown-item" href="#"><i class="fas fa-store-alt"></i> Toko Saya</a>
                                <a class="dropdown-item" href="#"><i class="fas fa-box"></i> Barang Dijual</a>
                                <a class="dropdown-item border-top" href="<?=site_url('min/login/logout');?>"><i class="fas fa-sign-out-alt"></i> Keluar</a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </nav>
    <?php if ($this->uri->segment(1) == "search"){ ?>
        <nav class="navbar topBar p-0 bg-grey" id="navbar" style="top: 0;transition: top 0.3s;">
        <?php }else{ ?>  
            <nav class="navbar topBar p-0 bg-grey">
            <?php } ?>  
            <?php if ($this->uri->segment(1) == "cart" OR $this->uri->segment(1) == "product" OR $this->uri->segment(1) == "search"){ ?>
                <div class="res-mob">
                    <div class="justify-content-end" style="width: 12%;">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a onclick="goBack()" class="" style="margin-right: 18px;position: relative;font-size: 24px;">
                                    <i class="fa fa-arrow-left"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div id="custom-search-input" style="width: 62%;">
                        <form action="search">
                            <div class="input-group col-md-12" style="padding: 0;">
                                <input autocomplete="off" type="search" name="keyword" class="search-query form-control search_top" placeholder="Cari Barang" />
                                <span class="input-group-btn" style="width:14px;">
                                    <button class="btn btn-danger" type="button">
                                        <span class="pe-7s-search"></span>
                                    </button>
                                </span>
                                <ul class="dropdown-search search_result hide"></ul>
                            </div>
                        </form>
                    </div>
                    <div class="justify-content-end" style="width: 25%;">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a href="#" class="">
                                    <span class="badge-notification" data-badge="0"><i class="pe-7s-bell"></i></span>
                                </a>
                                <a href="<?php echo site_url('cart'); ?>" class="">
                                    <?php if ($this->cart->total_items() != 0 OR $this->cart->total_items() != NULL) { ?>
                                        <span class="badge-notification" data-badge="<?=$this->cart->total_items()?>"><i class="pe-7s-cart"></i></span>
                                    <?php } ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php }else{ ?>
                <div class="res-mob">
                    <div id="custom-search-input">
                        <form action="search">
                            <div class="input-group col-md-12" style="padding-right: 0;">
                                <input autocomplete="off" type="search" name="keyword" class="search-query form-control" placeholder="Cari Barang" />
                                <span class="input-group-btn">
                                    <button class="btn btn-danger" type="button">
                                        <span class="pe-7s-search"></span>
                                    </button>
                                </span>
                            </div>
                            <ul class="dropdown-search search_result hide"></ul>
                        </form>
                    </div>
                    <div class="justify-content-end">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a href="#" class="">
                                    <span class="badge-notification" data-badge="4"><i class="pe-7s-bell"></i></span>
                                </a>
                                <a href="<?php echo site_url('cart'); ?>" class="">
                                    <span class="badge-notification" data-badge="4"><i class="pe-7s-cart"></i></span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
            <?php } ?>
        </nav>
        <?php if ($this->uri->segment(1) == "search"){ ?>
            <div class="filter-mob">
                <div class="navbar navbar-inverse navbar-light" id="filter" style="z-index: 1; background:#FFF;border-bottom: solid 1px #ddd;transition: top 0.3s;">
                    <div class="icon-bar" style="width: 100%;">
                        <a href="#" onclick="openNavAtur();" style="width: 50%;padding: 9px 0;">
                            <p style="font-size: 14px;line-height: 1;margin: 0;"><i class="pe-7s-repeat"></i> Atur Berdasarkan</p>
                        </a>

                        <a href="#" onclick="openNav();" style="width: 50%;padding: 9px 0;border-left: solid 1px #ddd;">
                            <p style="font-size: 14px;line-height: 1;margin: 0;"><i class="pe-7s-edit"></i> Filter</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <?php }else{ ?>
        <div class="res-mob">
            <div id="custom-search-input">
                <form action="<?=site_url('search')?>">
                    <div class="input-group col-md-12" style="padding-right: 0;">
                        <input autocomplete="off" type="search" name="keyword" class="search-query form-control" placeholder="Cari Barang" />
                        <span class="input-group-btn">
                            <button class="btn btn-danger" type="button">
                                <span class="pe-7s-search"></span>
                            </button>
                        </span>
                    </div>
                </div>
            </div>
        <?php } ?>
        <!-- start mobile nav bottom -->
        <?php if ($this->uri->segment(1) == "product"){ ?>
            <nav class="navbar navbar-inverse fixed-bottom" style="z-index: 1;background:#FFF;">
                <div class="row-button-detail">
                    <button class="btn btn-toko" style="width: 12%"><i class="fa fa-store"></i></button>
                    <a onclick="document.getElementById('form<?=$relatedProduct->id;?>').submit();"><button class="btn btn-keranjang" style="width: 45%;">Masukan Keranjang</button></a>
                    <button class="btn btn-belisekarang" style="width: 40%">Beli Sekarang</button>
                </div>
            </nav>   
        <?php }elseif ($this->uri->segment(1) == "cart") { ?>
            <nav class="navbar navbar-inverse fixed-bottom" style="z-index: 1;background:#FFF;">
                <div class="row-button-detail">
                    <button class="btn btn-belisekarang" style="width: 30%;float: right;margin-right: 5px;">Beli (2)</button>
                    <label class="label-total">Total Harga</label>
                    <p class="text-harga">Rp1.000.000</p>
                </div>
            </nav>     
        <?php }else{ ?>
            <nav class="navbar navbar-inverse fixed-bottom" style="z-index: 1;background:#FFF;">
                <div class="icon-bar">
                    <a class="<?php if($this->uri->segment(1)==""){echo "active";}?>" href="<?php echo site_url(''); ?>">
                        <i class="pe-7s-home"></i>
                        <p style="font-size: 10px;line-height: 1;margin: 0;">Home</p>
                    </a>
                    <a class="<?php if($this->uri->segment(1)=="wishlist"){echo "active";}?>" href="<?php echo site_url('wishlist'); ?>">
                        <i class="pe-7s-like"></i>
                        <p style="font-size: 10px;line-height: 1;margin: 0;">Wishlist</p>
                    </a>
                    <a class="<?php if($this->uri->segment(1)=="order-list"){echo "active";}?>" href="<?=site_url();?>order-list?sesi=login">
                        <i class="pe-7s-shopbag"></i>
                        <p style="font-size: 10px;line-height: 1;margin: 0;">Transaksi</p>
                        <span class="badge badge-notify">2</span>
                    </a>
                    <?php if ($this->session->userdata('id')==null) { ?>
                        <a href="#" data-toggle="modal" data-target="#login">
                            <i class="pe-7s-user"></i>
                            <p style="font-size: 10px;line-height: 1;margin: 0;">Masuk</p>
                        </a>
                    <?php }else{ ?>
                        <a href="<?php echo site_url('account'); ?>">
                            <i class="pe-7s-user"></i>
                            <p style="font-size: 10px;line-height: 1;margin: 0;">Akun</p>
                        </a>
                    <?php } ?>
                </div>
        <!-- <div class="message">
      <h1> hello, I'm a hidden message. You found it.</h1>
      <h2>Now Click the Heart button in the bottom to support CSS3</h2>
    </div> -->
    </nav>
    <?php } ?>
    <!--TOP BAR-->
    <!--BOTTOM BAR-->
    <div class="middleBar navbar-default res-desk">
        <div class="container">
            <div class="row display-table">
                <div class="col-sm-3 vertical-align text-left hidden-xs">
                    <a href="<?=site_url();?>">
                        <img src="<?=base_url();?>assets/img/logo.png" class="img-fluid" alt="Koperasi PKK Melati Jaya Store">
                        <!--                        <span class="h3">Koperasi PKK</span>-->
                    </a>
                </div>
                <!-- end col -->
                <div class="col-sm-6 vertical-align text-center">
                    <form action="<?=site_url('search')?>">
                        <div class="row grid-space-1">
                            <div class="col-sm-9">
                                <input autocomplete="off" type="search" name="keyword" class="form-control form-control-md search_top" placeholder="Cari kebutuhan mu...">
                                <ul class="dropdown-search search_result hide">
                                </ul>
                            </div>

                        <!--                            <div class="col-sm-3">-->
                            <!--                                <a href="#" class="btn btn-outline-info btn-sm" style="line-height: 14px;">-->
                                <!--                                    <i class="fas fa-map-marked-alt"></i>-->
                                <!--                                    <span class="d-block fs12px">Temukan</span>-->
                                <!--                                </a>-->
                                <!--                            </div>-->
                                <!-- end col -->
                                <!--                            <div class="col-sm-3">-->
                                    <!--                                <select class="form-control form-control-md" name="category">-->
                                        <!--                                    <option value="all">Kategori</option>-->
                                        <!--                                    <optgroup label="Mens">-->
                                            <!--                                        <option value="shirts">Shirts</option>-->
                                            <!--                                        <option value="coats-jackets">Coats & Jackets</option>-->
                                            <!--                                        <option value="underwear">Underwear</option>-->
                                            <!--                                        <option value="sunglasses">Sunglasses</option>-->
                                            <!--                                        <option value="socks">Socks</option>-->
                                            <!--                                        <option value="belts">Belts</option>-->
                                            <!--                                    </optgroup>-->
                                            <!--                                    <optgroup label="Womens">-->
                                                <!--                                        <option value="bresses">Bresses</option>-->
                                                <!--                                        <option value="t-shirts">T-shirts</option>-->
                                                <!--                                        <option value="skirts">Skirts</option>-->
                                                <!--                                        <option value="jeans">Jeans</option>-->
                                                <!--                                        <option value="pullover">Pullover</option>-->
                                                <!--                                    </optgroup>-->
                                                <!--                                    <option value="kids">Kids</option>-->
                                                <!--                                    <option value="fashion">Fashion</option>-->
                                                <!--                                    <optgroup label="Sportwear">-->
                                                    <!--                                        <option value="shoes">Shoes</option>-->
                                                    <!--                                        <option value="bags">Bags</option>-->
                                                    <!--                                        <option value="pants">Pants</option>-->
                                                    <!--                                        <option value="swimwear">Swimwear</option>-->
                                                    <!--                                        <option value="bicycles">Bicycles</option>-->
                                                    <!--                                    </optgroup>-->
                                                    <!--                                    <option value="bags">Bags</option>-->
                                                    <!--                                    <option value="shoes">Shoes</option>-->
                                                    <!--                                    <option value="hoseholds">HoseHolds</option>-->
                                                    <!--                                    <optgroup label="Technology">-->
                                                        <!--                                        <option value="tv">TV</option>-->
                                                        <!--                                        <option value="camera">Camera</option>-->
                                                        <!--                                        <option value="speakers">Speakers</option>-->
                                                        <!--                                        <option value="mobile">Mobile</option>-->
                                                        <!--                                        <option value="pc">PC</option>-->
                                                        <!--                                     </optgroup>-->
                                                        <!--                                </select>-->
                                                        <!--                            </div>-->
                                                        <!-- end col -->
                                                        <div class="col-sm-3">
                                                            <input type="submit" class="btn btn-default btn-block" value="Cari">
                                                        </div>
                                                        <!-- end col -->
                                                    </div>
                                                    <!-- end row -->
                                                </form>
                                            </div>
                                            <!-- end col -->
                                            <div class="col-sm-3 vertical-align header-items hidden-xs middleBarNav">
                                                <div class="header-item mr-10">
                                                    <a href="<?=site_url();?>wishlist" data-toggle="tooltip" data-placement="bottom" title="Daftar Favorit" data-original-title="Wishlist"> <i class="fa fa-heart"></i> <sub class="whisCount" style="display: none;"></sub> </a>
                                                </div>
                                                <div class="header-item dropdown mr-10">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="false"> <i class="fa fa-shopping-basket"></i>
                                                        <?php 
                                                        if ($this->cart->total_items() != 0 OR $this->cart->total_items() != NULL) { ?>
                                                            <sub class="cartCount"><?=$this->cart->total_items()?></sub>
                                                        <?php } ?>
                                                    </a>
                                                    <ul class="dropdown-menu cart wd-250" role="menu">
                                                        <li>
                                                            <div class="cart-items">
                                                                <ol class="items">
                                                                    <?php 

                                                                    $cart= $this->cart->contents();
                                                                    if(empty($cart)) {
                                                                        ?>
                                                                        <p>Keranjang Belanja Kosong</p>
                                                                        <?php
                                                                    } else {
                                                                        $grand_total = 0;
                                                                        foreach ($cart as $item)
                                                                        {
                                                                            $grand_total+=$item['subtotal'];
                                                                            ?>
                                                                            <li>
                                                                                <a href="<?=base_url().'product/'.$item['slug'];?>" class="product-image"> <img src="<?=base_url().'assets/img/produk/'.$item['img'];?>" class="img-fluid"></a>
                                                                                <div class="product-details">
                                                                                    <div class="close-icon"> 
                                                                                        <a href="<?php echo base_url()?>action_main/deleteCart/<?php echo $item['rowid'];?>" class="border-0">X</a> 
                                                                                    </div>
                                                                                    <p class="product-name"> 
                                                                                        <a href="<?=site_url('product/').$item['slug']?>"  class="border-0"><?php echo $item['name']; ?></a> 
                                                                                    </p> 
                                                                                    <strong><?php echo $item['qty']; ?></strong> x 
                                                                                    <span class="price text-primary">Rp <?php echo number_format($item['subtotal'],0,",","."); ?></span> 
                                                                                </div>
                                                                            </li>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </ol>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="cart-footer" >
                                                                <!--<a href="#" class="float-left border-0"><i class="fa fa-cart-plus mr-05"></i></a>-->
                                                                <!--<a href="#" class="float-right border-0"><i class="fa fa-shopping-basket mr-05"></i>Checkout</a>-->
                                                                <div class="row">
                                                                    <div class="col-6">
                                                                        <a href="<?=site_url();?>cart" class=" border-0"><i class="fa fa-cart-plus mr-05"></i> Keranjang</a>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <a href="<?=site_url();?>checkout" class=" border-0"><i class="fa fa-shopping-basket mr-05"></i>Checkout</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="header-item" >
                                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Notifikasi" data-original-title="Wishlist"> <i class="fa fa-bell"></i> <sub style="display:none;"></sub> </a>
                                                </div>
                                            </div>
                                            <!-- end col -->
                                        </div>
                                        <!-- end  row -->
                                    </div>
                                </div>
                                <!--BOTTOM BAR-->
                            </div>
