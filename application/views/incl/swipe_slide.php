<div class="row col-slide">
    <div class="col-12">
        <!-- Swiper -->
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach($swipeSlider as $swipe) { ?>
                <a href="<?=$swipe->link;?>" class="swiper-slide">
                    <img src="<?=base_url('assets/img/banner/').$swipe->gambar;?>" class="img-fluid rounded">
                </a>
                <?php } ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
        </div>
    </div>
</div>