<?php if ($this->uri->segment(1) == "cart" OR $this->uri->segment(1) == "product" OR $this->uri->segment(1) == "account"){
}else{ ?>
<footer class="footer mt-20">
    <!-- <div class="message">
      <h1> hello, I'm a hidden message. You found it.</h1>
      <h2>Now Click the Heart button in the bottom to support CSS3</h2>
    </div> -->
    <div class="container bottom_border">
        <div class="row">
            <div class=" col-sm-4 col-md  col-6 col">
                <h5 class="headin5_amrc col_white_amrc pt2">Tentang Kami</h5>
                <!--FOOTER MENU 1-->
                <ul class="footer_ul_amrc">
                    <li><a href="#">Tentang PKK</a></li>
                    <li><a href="#">Aturan Pengguna</a></li>
                    <li><a href="#">Kebijakan Privasi</a></li>
                    <li><a href="#">Karir</a></li>
                    <li><a href="#">Hubungi Kami</a></li>
                </ul>
                <!--FOOTER MENU 1-->
            </div>

            <div class=" col-sm-4 col-md  col-6 col">
                <h5 class="headin5_amrc col_white_amrc pt2">Layanan Pelanggan</h5>
                <!--FOOTER MENU 2-->
                <ul class="footer_ul_amrc">
                    <li><a href="#">Pusat Bantuan</a></li>
                    <li><a href="#">FAQ (Tanya Jawab)</a></li>
                    <li><a href="#">Lacak Pengiriman</a></li>
                    <li><a href="#">Pengembalian Barang & Dana</a></li>
                    <li><a href="#">Cara Pembelian & Pembayaran</a></li>
                </ul>
                <!--FOOTER MENU 2-->
            </div>


            <div class=" col-sm-4 col-md  col-12 col">
                <h5 class="headin5_amrc col_white_amrc pt2">Jelajahi Store</h5>
                <!--FOOTER MENU 3-->
                <ul class="footer_ul_amrc">
                    <li><a href="#">Semua Kategori</a></li>
                    <li><a href="#">Daftar Brand</a></li>
                    <li><a href="#">Rekomendasi Produk</a></li>
                </ul>
                <!--FOOTER MENU 3-->
            </div>


            <div class=" col-sm-4 col-md  col-6 col">
                <h5 class="headin5_amrc col_white_amrc pt2">Pembayaran</h5>
                <!--FOOTER MENU 4-->
                <ul class="footer_ul_amrc">
                    <li><img src="<?=base_url();?>assets/img/other/bankdki.jpg" width="90"></li>
                </ul>

                <h5 class="headin5_amrc col_white_amrc">Pengiriman</h5>
                <ul class="footer_ul_amrc">
                    <li><img src="<?=base_url();?>assets/img/other/gosend.jpg" width="80"></li>
                </ul>
                <!--FOOTER MENU 4-->
            </div>

        </div>
    </div>


    <div class="container res-foot">
        <!-- <ul class="foote_bottom_ul_amrc">
            <li><a href="#">Home</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Services</a></li>
            <li><a href="#">Pricing</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">Contact</a></li>
        </ul> -->
        <!--foote_bottom_ul_amrc ends here-->
        <p class="text-center" style="margin-top: 20px;">Copyright @2019 | <span class="font-weight-bold">Koperasi PKK Melati</span></p>

        <ul class="social_footer_ul">
            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
            <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
        </ul>
        <!--social_footer_ul ends here-->
    </div>
</footer>
<?php } ?>