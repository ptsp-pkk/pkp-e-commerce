<!-- Start Login -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 368px;">
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: 0;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="fa fa-times" style="color: #999;"></span>
        </button>
      </div>
      <div class="modal-body" style="padding: 32px;padding-top: 5px;">
        <h5 class="modal-title" id="exampleModalLabel" style="margin-bottom:32px;">Masuk 
          <a style="float: right !important;color: #395599;cursor: pointer;font-size: 15px;margin-top: 7px;" onclick="daftarmodal();">Daftar</a></h5>
        <form action="javascript:void(0);" method="post" id="masuknohp">
          <div class="form-group">
            <label for="exampleInputEmail1" style="margin-bottom: 7px;font-size: 12px;line-height: 1.33;color: rgba(0,0,0,.54);">Nomor Handphone</label>
            <input type="text" class="form-control" autocomplete="off" id="exampleInputEmail1" aria-describedby="emailHelp" id="hp" name="hp" placeholder="Masukan Nomor Handphone">
          </div>
          <button type="submit" class="btn btn-primary btn-block" style="margin-top: 20px;background-color: #395599;border-radius: 8px;font-weight: 500;letter-spacing: 1px;height: 40px;">Masuk</button>
        </form>
        <p style="margin-top:25px;font-size: 14px;color: rgba(0,0,0,.54);text-align: center;">Belum punya akun? <a href="" style="font-weight: 600!important;text-decoration: none;color: #395599;" onclick="daftarmodal();">Daftar</a></p>
      </div>
    </div>
  </div>
</div>
<!-- End Login -->
 
<!-- Start Regis -->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 500px;">
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: 0;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="fa fa-times" style="color: #999;"></span>
        </button>
      </div>
      <div class="modal-body" style="padding: 32px;padding-top: 5px;">
        <h5 class="modal-title" id="exampleModalLabel" style="margin-bottom:32px;">Daftar Akun Baru<a style="float: right !important;color: #395599;cursor: pointer;font-size: 15px;margin-top: 7px;" onclick="loginmodal();">Masuk</a></h5>
        <form action="javascript:void(0);" method="post" id="registernohp">
          <div class="form-group">
            <label for="exampleInputEmail1" style="margin-bottom: 7px;font-size: 12px;line-height: 1.33;color: rgba(0,0,0,.54);">Nomor Telp</label>
            <input type="number" name="nohp" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan Nomor Hp(08xx xxxx xxxx)" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1" style="margin-bottom: 7px;font-size: 12px;line-height: 1.33;color: rgba(0,0,0,.54);">Nama</label>
            <input type="text" class="form-control" name="nama" id="exampleInputEmail1" aria-describedby="namaHelp" placeholder="Masukan Nama Anda" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1" style="margin-bottom: 7px;font-size: 12px;line-height: 1.33;color: rgba(0,0,0,.54);">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" autocomplete="off">
          </div>
          <span class="alert alert-danger" id="alertmsg"></span>
          <p style="margin-top:25px;font-size: 14px;color: rgba(0,0,0,.54);text-align: center;">Saya sudah membaca dan setuju dengan <a href="" style="font-weight: 600!important;text-decoration: none;color: #395599;">Syarat & Ketentuan Koperasi PKK</a></p>
          <button type="submit" class="btn btn-primary btn-block" style="margin-top: 20px;background-color: #395599;border-radius: 8px;font-weight: 500;letter-spacing: 1px;height: 40px;">Daftar</button>
        </form>
        <p style="margin-top:25px;font-size: 14px;color: rgba(0,0,0,.54);text-align: center;">Sudah punya akun? <a href="" style="font-weight: 600!important;text-decoration: none;color: #395599;" onclick="loginmodal();">Masuk</a></p>
      </div>
    </div>
  </div>
</div>
<!-- End Regis -->


<!-- Start OTP -->
<div class="modal fade" id="modalotp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 368px;">
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: 0;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="fa fa-times" style="color: #999;"></span>
        </button>
      </div>
      <div class="modal-body" style="padding: 32px;padding-top: 5px;">
        <form action="javascript:void(0);" method="post" id="masukotp">
          <div class="form-group">
            <label for="exampleInputEmail1" style="margin-bottom: 7px;font-size: 12px;line-height: 1.33;color: rgba(0,0,0,.54);">Kode OTP</label>
            <input type="text" class="form-control" autocomplete="off" id="exampleInputEmail1" aria-describedby="emailHelp" id="hp" name="code" placeholder="Masukan Kode OTP">
            <input type="hidden" id="req" name="request">
            <input type="hidden" id="nohp" name="hp">
          </div>
          <button type="submit" class="btn btn-primary btn-block" style="margin-top: 20px;background-color: #395599;border-radius: 8px;font-weight: 500;letter-spacing: 1px;height: 40px;">Masuk</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- End OTP -->