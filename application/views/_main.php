<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?=$title;?></title>
    <meta name="description" content="">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta content="yes" name="mobile-web-app-capable">
    <meta content="#750000" name="theme-color">
    <meta content="PKKCommerce" name="apple-mobile-web-app-title">
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta content="#750000" name="msapplication-navbutton-color">

    <!--    <link rel="apple-touch-icon" sizes="57x57" href="<?=base_url();?>assets/images/favicon/apple-icon-57x57.png">-->
    <!--    <link rel="apple-touch-icon" sizes="60x60" href="<?=base_url();?>assets/images/favicon/apple-icon-60x60.png">-->
    <!--    <link rel="apple-touch-icon" sizes="72x72" href="<?=base_url();?>assets/images/favicon/apple-icon-72x72.png">-->
    <!--    <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url();?>assets/images/favicon/apple-icon-76x76.png">-->
    <!--    <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url();?>assets/images/favicon/apple-icon-114x114.png">-->
    <!--    <link rel="apple-touch-icon" sizes="120x120" href="<?=base_url();?>assets/images/favicon/apple-icon-120x120.png">-->
    <!--    <link rel="apple-touch-icon" sizes="144x144" href="<?=base_url();?>assets/images/favicon/apple-icon-144x144.png">-->
    <!--    <link rel="apple-touch-icon" sizes="152x152" href="<?=base_url();?>assets/images/favicon/apple-icon-152x152.png">-->
    <!--    <link rel="apple-touch-icon" sizes="180x180" href="<?=base_url();?>assets/images/favicon/apple-icon-180x180.png">-->
    <!--    <link rel="icon" type="image/png" sizes="192x192"  href="<?=base_url();?>assets/images/favicon/android-icon-192x192.png">-->
    <!--    <link rel="icon" type="image/png" sizes="32x32" href="<?=base_url();?>assets/images/favicon/favicon-32x32.png">-->
    <!--    <link rel="icon" type="image/png" sizes="96x96" href="<?=base_url();?>assets/images/favicon/favicon-96x96.png">-->
    <!--    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url();?>assets/images/favicon/favicon-16x16.png">-->
    <!--    <link rel="manifest" href="<?=base_url();?>assets/images/favicon/manifest.json">-->
    <meta name="msapplication-TileColor" content="#750000">
    <meta name="msapplication-TileImage" content="<?=base_url();?>assets/images/favicon/ms-icon-144x144.png">
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/vendor/fa5/css/all.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/vendor/slick1.8.1/css/slick.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/vendor/slick1.8.1/css/slick-theme.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/vendor/swiper4.5.0/css/swiper.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/stylesheet-web.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/pe-icon-7-stroke.css">
</head>
<body class="bg-grey">

<!--TOP NAV-->
<?php $this->load->view('incl/top_nav');?>
<!--TOP NAV-->

<?php $this->load->view($link_view);?>

<!--MODAL-->
<?php $this->load->view('incl/modal_login');?>
<!--MODAL-->

<!--FOOTER-->
<?php $this->load->view('incl/footer');?>
<!--FOOTER-->

<!-- JS -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- <script src="<?=base_url();?>assets/js/jquery-3.3.1.slim.min.js"></script> -->
<script src="<?=base_url();?>assets/js/popper.min.js"></script>
<script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?=base_url();?>assets/vendor/slick1.8.1/js/slick.min.js"></script>
<script src="<?=base_url();?>assets/vendor/swiper4.5.0/js/swiper.min.js"></script>

<script type="text/javascript">
    function openNavAtur() {
        document.getElementById("mySidenavAtur").style.width = "85%";
        document.getElementById("overlaySide").style.width = "100%";
    }
    function openNav() {
        document.getElementById("mySidenav").style.width = "85%";
        document.getElementById("overlaySide").style.width = "100%";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("mySidenavAtur").style.width = "0";
        document.getElementById("overlaySide").style.width = "0";
    }
</script>
<script>
function goBack() {
  window.history.back(-1);
}
</script>
<!-- hide nav on scroll -->
<script type="text/javascript">
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
    var currentScrollPos = window.pageYOffset;
    if (prevScrollpos > currentScrollPos) {
        document.getElementById("navbar").style.top = "0px";
        document.getElementById("filter").style.top = "0px";
    } else {
        document.getElementById("navbar").style.top = "-50px";
        document.getElementById("filter").style.top = "-50px";
    }
        prevScrollpos = currentScrollPos;
    }
</script>
<!-- end hide nav on scroll -->

<!--ADD TO CART-->
<script>
    //plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());

    $.ajax({
            'url': '<?=site_url('cart/')?>updateCart',
            'type': 'POST',
            'data': {cart:valueCurrent},
            beforeSend:function(good) {
                
            },
            success:function(good) {

                $("#cartnya").html(good.html);
            }
        });

    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled');
        // ajax
        // alert(valueCurrent);

    } else {
        alert('Maaf Jumlah Tidak boleh kurang dari 1');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled');
        // ajax
        // alert(valueCurrent);
    } else {
        alert('Maaf, Jumlah Tidak boleh dari 99');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    function daftarmodal(argument) {
        $("#register").modal('show');
        $("#login").modal('hide');
    }
    function loginmodal(argument) {
        $("#register").modal('hide');
        $("#login").modal('show');
    }
    var counter=0;
    $(document).ready(function() {
        $('.btnAddCart').click(function() {
            $('.helper').addClass('added');
            counter++;

            var buttonCount = setTimeout(function(){
                $('.cartCount').attr('data-count', counter);
                $('.cartCount').show();
            }, 1000);

            var removeClass = setTimeout(function(){
                $('.helper, .cartCount').removeClass('added');
            }, 1000);
        });
        $("#masuknohp").submit(function(event) {
            var data = new FormData($(this)[0]);
            $.ajax({
                url: '<?=site_url('min/login/auth')?>',
                type: 'POST',
                data: data,
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend:function (dada) {
                    // body...
                },
                success:function(data) {
                    console.log(data.msg);
                    if (data.success == false) {
                        $("#alertmsg").text(""+data.msg+"")
                    }else{
                        $("#login").modal('hide');
                        $("#modalotp").modal('show');
                        $("#nohp").val(data.nohp);
                        $("#req").val(data.req);
                    }
                },
            });
        });

        $("#registernohp").submit(function(event) {
            var data = new FormData($(this)[0]);
            $.ajax({
                url: '<?=site_url('min/register/Action')?>',
                type: 'POST',
                data: data,
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend:function (dada) {
                    // body...
                },
                success:function(data) {
                    console.log(data.msg);
                    if (data.success == false) {
                        $("#alertmsg").html(""+data.msg+"")
                    }else{
                        $("#register").modal('hide');
                        $("#modalotp").modal('show');
                        $("#nohp").val(data.nohp);
                        $("#req").val(data.req);
                    }
                },
            });
        });

        $("#masukotp").submit(function(event) {
            var data = new FormData($(this)[0]);
            $.ajax({
                url: '<?=site_url('min/login/checkkode')?>',
                type: 'POST',
                data: data,
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend:function (dada) {
                    
                },
                success:function(data) {
                    if (data.msg == 'success') {
                        location.reload();
                    }else{
                        alert(data.msg);
                    }
                },
            });
        });
    });
</script>
<!--PORDUCT SLIDE-->
<script>
    $('.product-detail_pict_one').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.product-detail_pict_slide'
    });
    $('.product-detail_pict_slide').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.product-detail_pict_one',
        focusOnSelect: true
    });
</script>
<!--TOOTLTIPS-->
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
<!--SLICK SLIDER-->
<script>
    $(document).ready(function(){
        $('.primary-slider').slick({
            autoplay: true,
            dots: true,
            // centerMode: true,
            // centerPadding: "25%",
            slidesToShow: true,
            // infinite: true,
            prevArrow: '<a href="#" class="carousel-control-prev"><span class="carousel-control-prev-icon rounded-right-10" aria-hidden="true"></span></a>',
            nextArrow: '<a href="#" class="carousel-control-next"><span class="carousel-control-next-icon rounded-left-10" aria-hidden="true"></span></a>'
        });
    });

</script>

<!--SWIPER-->
<script>
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 5,
        spaceBetween: 10,
        // pagination: {
        //     el: '.swiper-pagination',
        //     clickable: true,
        // },
    });
</script>

<!--HOVER MENU-->
<script>
    ! function($, n, e) {
        var o = $();
        $.fn.dropdownHover = function(e) {
            return "ontouchstart" in document ? this : (o = o.add(this.parent()), this.each(function() {
                function t(e) {
                    o.find(":focus").blur(), h.instantlyCloseOthers === !0 && o.removeClass("open"), n.clearTimeout(c), i.addClass("open"), r.trigger(a)
                }
                var r = $(this),
                    i = r.parent(),
                    d = {
                        delay: 100,
                        instantlyCloseOthers: !0
                    },
                    s = {
                        delay: $(this).data("delay"),
                        instantlyCloseOthers: $(this).data("close-others")
                    },
                    a = "show.bs.dropdown",
                    u = "hide.bs.dropdown",
                    h = $.extend(!0, {}, d, e, s),
                    c;
                i.hover(function(n) {
                    return i.hasClass("open") || r.is(n.target) ? void t(n) : !0
                }, function() {
                    c = n.setTimeout(function() {
                        i.removeClass("open"), r.trigger(u)
                    }, h.delay)
                }), r.hover(function(n) {
                    return i.hasClass("open") || i.is(n.target) ? void t(n) : !0
                }), i.find(".dropdown-submenu").each(function() {
                    var e = $(this),
                        o;
                    e.hover(function() {
                        n.clearTimeout(o), e.children(".dropdown-menu").show(), e.siblings().children(".dropdown-menu").hide()
                    }, function() {
                        var t = e.children(".dropdown-menu");
                        o = n.setTimeout(function() {
                            t.hide()
                        }, h.delay)
                    })
                })
            }))
        }, $(document).ready(function() {
            $('[data-hover="dropdown"]').dropdownHover()
        })
    }(jQuery, this);
</script>

<!--SEARCH JS-->
<script>
    var base_url = "<?php echo base_url() ?>";
    $(document).on('click', function () {
        $(".search_result").addClass('hide');
        $(".search_result").removeClass('shows');
    });

    $('.search_top').bind("enterKey",function(e){
        window.location = base_url+'search/'+$(this).val();
    });
    $('.search_top').keyup(function(e){
        if(e.keyCode == 13)
        {
            $(this).trigger("enterKey");
        }
    });

    $(document).on('keyup', '.search_top', function(event) {var search = $(this).val();
        // if (search.length >=3) {
            $(".search_result").addClass('shows').removeClass('hide');
            var list_product = '',
                list_store = '';
            list_all = '';

            list_product += '<span class="dropdown-header">Produk</span>';
            var cover = base_url + 'assets/img/produk/testprod1.jpg';
            var urls = base_url + 'product';

            list_product += '<a href="' + urls + '" class="dropdown-item"><img class="img-fluid rounded mr-2 obj-fit-cover" src="' + cover + '" width="30"><span class="fs14px">Baju Soekarno Sablon Bandung</span></a>';


            list_store += '<hr class="mb-0 mt-1"><span class="dropdown-header">Toko Seller</span>';

            var prof_pict = base_url + 'assets/img/banner/testban1.jpg';

            list_store += '<a href="' + base_url + 'toko" class="dropdown-item"><img class="img-fluid rounded mr-2 obj-fit-cover" src="' + prof_pict + '" width="30"><span class="fs14px">Toko Sampurna</span></a>';

            list_all += '<hr class="mb-1 mt-1"><a href="' + base_url + 'search" class="dropdown-item font-weight-bold text-center">Lihat Semua</a>';
            $(".search_result").html(list_product+list_store+list_all);
        // }
    });
</script>

<!--REVIEW JS-->
<script>
    $(document).ready(function() {

        // implement start rating select/deselect
        $( ".rateButton" ).click(function() {
            if($(this).hasClass('btn-grey')) {
                $(this).removeClass('btn-grey btn-default').addClass('btn-warning star-selected');
                $(this).prevAll('.rateButton').removeClass('btn-grey btn-default').addClass('btn-warning star-selected');
                $(this).nextAll('.rateButton').removeClass('btn-warning star-selected').addClass('btn-grey btn-default');
            } else {
                $(this).nextAll('.rateButton').removeClass('btn-warning star-selected').addClass('btn-grey btn-default');
            }
            $("#rating").val($('.star-selected').length);
        });
    });
</script>

</body>
</html>