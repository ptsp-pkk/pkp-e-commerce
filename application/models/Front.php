<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Model {

	public function product_newHome($limit=8,$start='',$cari='') {
		$this->db->select('produk.id,produk.judul,produk.harga,produk.slug,produk.status,gambarproduk.fileName,kategori.namaKategori,diskon.persenDiskon,review.rating,');
		$this->db->from('produk');

        $this->db->join('kategori', 'kategori.id = produk.idKategori', 'left');
        $this->db->join('gambarproduk', 'produk.id = gambarproduk.idProduk', 'left');
        $this->db->join('diskon', 'diskon.idProduk = produk.id', 'left');
        $this->db->join('review', 'review.idProduk = produk.id', 'left');

        if ($cari != '') {
            $this->db->like('judul', $cari, 'BOTH');
        }

        $this->db->where(array('produk.status' => 1));
        $this->db->group_by('produk.id');
        $this->db->order_by('produk.id','asc');
        $product_new = $this->db->get('',$limit,$start);

        return $product_new->result();
	}

    public function product_like($cari='') {

        if ($cari != '') {
            $this->db->like('judul', $cari, 'BOTH');
        }

        $this->db->where(array('produk.status' => 1));
        $this->db->group_by('produk.id');
        $this->db->order_by('produk.id','asc');
        $product_new = $this->db->get('produk');

        return $product_new;
    }

	public function product_catHome($id) {
		$this->db->select('produk.id,produk.judul,produk.harga,produk.slug,produk.status,gambarproduk.fileName,kategori.namaKategori,diskon.persenDiskon,review.rating,');
		$this->db->from('produk');

        $this->db->join('kategori', 'kategori.id = produk.idKategori', 'left');
        $this->db->join('gambarproduk', 'produk.id = gambarproduk.idProduk', 'left');
        $this->db->join('diskon', 'diskon.idProduk = produk.id', 'left');
        $this->db->join('review', 'review.idProduk = produk.id', 'left');

        $this->db->where(array('produk.status'=>1,'produk.idKategori'=>$id));
        $this->db->group_by('produk.id');
        $this->db->order_by('produk.id','asc');
        $this->db->limit(4);
        $product_cat = $this->db->get('');

        return $product_cat->result();
	}

    public function productDetail($slug) {
        $this->db->select('produk.id,produk.idKategori,produk.judul,produk.harga,produk.keterangan,produk.slug,produk.beratProduk,produk.stok,produk.status,produk.sku,gambarproduk.fileName,kategori.namaKategori,diskon.persenDiskon,review.rating,store.namaStore,store.logo,store.levelStore,kota.namaKota,user.nama,merek.namaMerek');
        $this->db->from('produk');

        $this->db->join('kategori', 'kategori.id = produk.idKategori', 'left');
        $this->db->join('gambarproduk', 'produk.id = gambarproduk.idProduk', 'left');
        $this->db->join('diskon', 'diskon.idProduk = produk.id', 'left');
        $this->db->join('review', 'review.idProduk = produk.id', 'left');
        $this->db->join('merek', 'merek.id = produk.idMerek', 'left');
        $this->db->join('store', 'store.id = produk.idStore', 'left');
        $this->db->join('kota', 'kota.id = store.idKota', 'left');
        $this->db->join('user', 'user.id = review.idMember', 'left');

        $this->db->where(array('produk.status' => 1, 'produk.slug' => $slug));
        $productDetail = $this->db->get('');

        return $productDetail->row();
    }

    public function productRelated($idcat,$idpro) {
        $this->db->select('produk.id,produk.judul,produk.harga,produk.slug,produk.status,gambarproduk.fileName,kategori.namaKategori,diskon.persenDiskon,review.rating,');
        $this->db->from('produk');

        $this->db->join('kategori', 'kategori.id = produk.idKategori', 'left');
        $this->db->join('gambarproduk', 'produk.id = gambarproduk.idProduk', 'left');
        $this->db->join('diskon', 'diskon.idProduk = produk.id', 'left');
        $this->db->join('review', 'review.idProduk = produk.id', 'left');

        $this->db->where(array('produk.status'=>1,'produk.idKategori'=>$idcat,'produk.id !='=>$idpro));
        $this->db->group_by('produk.id');
        $this->db->order_by('produk.id','random');
        $this->db->limit(4);
        $product_cat = $this->db->get('');

        return $product_cat->result();
    }

    public function reviewProduct($id) {
        $this->db->select('review.*,user.nama');
        $this->db->from('review');
        $this->db->join('user','user.id = review.idMember','left');
        $this->db->where(array('idProduk'=>$id));
        $reviewDetail=$this->db->get();
        return $reviewDetail->result();
    }

    public function get_data($table,$array_parameter){
        $get = $this->db->get_where($table,$array_parameter);
        return $get->result();
    }

    public function get_data_row($table,$array_parameter){
        $get = $this->db->get_where($table,$array_parameter);
        return $get->row();
    }

    public function checkoutUser($id){
        $this->db->select('useralamat.*,kota.namaKota,kecamatan.namaKecamatan,kelurahan.namaKelurahan');
        $this->db->from('useralamat');

        $this->db->join('kota', 'kota.id = useralamat.idKota', 'left');
        $this->db->join('kecamatan', 'kecamatan.id = useralamat.idKecamatan', 'left');
        $this->db->join('kelurahan', 'kelurahan.id = useralamat.idKelurahan', 'left');

        $this->db->where(array('useralamat.statusAlamat' => 1, 'useralamat.idUser' => $id));
        $checkoutUser = $this->db->get('');

        return $checkoutUser->row();
    }

	public function count_data($table,$array_parameter){
		$count = $this->db->get_where($table,$array_parameter);
		return $count->num_rows();
	}

	public function sum_data($data,$table,$array_parameter){
		$this->db->select_sum($data);
		$result = $this->db->get_where($table,$array_parameter)->row();  
		return $result->$data;
	}

	public function select_data($data,$table,$array_parameter){
		$this->db->select_sum($data);
		$result = $this->db->get_where($table,$array_parameter);  
		return $result->result();
	}

	public function addCart()
    {
        $data_produk= array('id' => $this->input->post('id'),
                             'name' => $this->input->post('nama'),
                             'price' => $this->input->post('harga'),
                             'img' => $this->input->post('fileName'),
                             'slug' => $this->input->post('slug'),
                             'qty' =>$this->input->post('qty')
                            );
        $this->cart->insert($data_produk);
        redirect($this->input->post('redirectto'));
    }

    public function deleteCart($rowid) 
    {
        if ($rowid=="all")
            {
                $this->cart->destroy();
            }
        else
            {
                $data = array('rowid' => $rowid,
                              'qty' =>0);
                $this->cart->update($data);
            }
        redirect('shopping/tampil_cart');
    }

    public function updateCart()
    {
        $cart_info = $_POST['cart'] ;
        foreach( $cart_info as $id => $cart)
        {
            $rowid = $cart['rowid'];
            $price = $cart['price'];
            $gambar = $cart['gambar'];
            $amount = $price * $cart['qty'];
            $qty = $cart['qty'];
            $data = array('rowid' => $rowid,
                            'price' => $price,
                            'gambar' => $gambar,
                            'amount' => $amount,
                            'qty' => $qty);
            $this->cart->update($data);
        }
        redirect('shopping/tampil_cart');
    }

    public function addDatatoTable($table,$data)
    {
        $this->db->insert($table, $data);
        $id = $this->db->insert_id();
        return (isset($id)) ? $id : FALSE;
    }
}