<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Merek extends CI_Model {
    
    private $t = 'merek'; #tabel
    private $id = 'id'; #field id
    public $see = '*';

    public function se($id='',$where='',$query='')
    {
        $q = false;

        if ($id != '') {
           $this->db->select($this->see);
           $q = $this->db->get_where($this->t, [$this->id => $id]);
        }elseif ($where != '') {
            $this->db->select($this->see);
           $q = $this->db->get_where($this->t, $where);
        }elseif ($query != '') {
            $q = $this->db->query($query);
        }else{
           $this->db->select($this->see);
           $q = $this->db->get($this->t);
        }

        return $q;
    }

    public function in($obj='')
    {
        $q = false;
        if ($obj != '') {
            $q = $this->db->insert($this->t,$obj);
        }

        return $q;
    }

    public function up($obj='',$where='')
    {
        $q = false;

        if ($obj != '') {
            $q = $this->db->update($this->t,$obj,$where);
        }

        return $q;
    }

    public function de($where='')
    {
        $q = false;

        if ($obj != '') {
            $q = $this->db->delete($this->t,$where);
        }

        return $q;
    }
    
}

/* End of file Merek.php */
