<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Model {
    
    private $t = 'produk'; #tabel
    private $id = 'id'; #field id
    public $see = '*';

    public function dt()
    {
        // Definisi
        $condition = '';
        $data = [];

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->t;
        // Set orderable column fields
        $CI->dt->column_order = [null, 'judul', 'harga', 'stok', 'kondisi', 'beratProduk', 'minPembelian'];
        // Set searchable column fields
        $CI->dt->column_search = ['judul', 'harga', 'stok', 'kondisi', 'beratProduk', 'minPembelian'];
        // Set select column fields
        $CI->dt->select = $this->t . '.*';
        // Set default order
        $CI->dt->order = [$this->t . '.id' => 'desc'];

        $condition = [
            ['where',$this->t.'.status','1']
        ];

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            $data[] = array(
                $dt->judul,
                $dt->harga,
                $dt->stok,
                $this->setKondisi($dt->kondisi),
                $dt->beratProduk,
                $dt->minPembelian,
            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

    public function se($id='',$where='',$query='')
    {
        $q = false;

        if ($id != '') {
           $this->db->select($this->see);
           $q = $this->db->get_where($this->t, [$this->id => $id]);
        }elseif ($where != '') {
            $this->db->select($this->see);
           $q = $this->db->get_where($this->t, $where);
        }elseif ($query != '') {
            $q = $this->db->query($query);
        }else{
           $this->db->select($this->see);
           $q = $this->db->get($this->t);
        }

        return $q;
    }

    public function in($obj='')
    {
        $q = false;
        if ($obj != '') {
            $q = $this->db->insert($this->t,$obj);
        }

        return $q;
    }

    public function up($obj='',$where='')
    {
        $q = false;

        if ($obj != '') {
            $q = $this->db->update($this->t,$obj,$where);
        }

        return $q;
    }

    public function de($where='')
    {
        $q = false;

        if ($obj != '') {
            $q = $this->db->delete($this->t,$where);
        }

        return $q;
    }

    private function setKondisi(int $val = null)
    {
        if ($val != null) {
            if ($val == 1) {
                $val = "Baru";
            }else if($val == 2){
                $val = "Bekas";
            }
        }else{
            $val = "Tidak diketahui";
        }

        return $val;
    }
    
}

/* End of file Produk.php */
