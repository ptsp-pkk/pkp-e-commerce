<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Model {

    public  $username = '';
    public  $password = '';

    private $data = [];
    private $status = 0;
    private $msg = '';

    public function auth_admin()
    {
        $U =& get_instance();
        $U->load->model('Users', 'u');

        $user = $U->u->se('',['email' => $this->email,'password' => $this->password,'status' => 1]);
        if ($user->num_rows() > 0) {
            
            $user = $user->row(); #Dijadikan Object
            $this->msg = "Berhasil login";
            $this->status = 1;
            $this->data = [
                'id' => $user->idUser,
                'level' => $user->level
            ];

            // Daftarkan sebagai session
            $this->session->set_userdata( $this->data );
            

        }else{
            $this->msg = "Tidak dapat login, harap periksa kembali email dan password anda.";
        }

        $log = [
            'msg' => $this->msg,
            'status' => $this->status,
            'data' => $this->data
        ];

        return $log;
    }

}

/* End of file Auth.php */

?>