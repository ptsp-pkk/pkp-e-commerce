<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class MPesanan extends CI_Model {

    private $t = "listpembelian";

    public function dt()
    {
        // Definisi
        $condition = '';
        $data = [];

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->t;
        // Set orderable column fields
        $CI->dt->column_order = [null, 'u.nama', 'p.judul', $this->t.'.quantity',$this->t.'.catatan','ua.ketAlamat'];
        // Set searchable column fields
        $CI->dt->column_search = ['u.nama', 'p.judul', $this->t.'.quantity',$this->t.'.catatan','ua.ketAlamat'];
        // Set select column fields
        $CI->dt->select = $this->t . '.quantity,'.$this->t . '.catatan'.',p.judul,u.nama,ua.ketAlamat';
        // Set default order
        $CI->dt->order = [$this->t . '.id' => 'desc'];

        $condition = [
            ['join','transaksipembelian tpemb',$this->t.'.idTransaksiPembelian = tpemb.id','inner'],
            ['join','user u','tpemb.idMember = u.id','inner'],
            ['join','produk p',$this->t.'.idProduk = p.id','inner'],
            ['join','transaksipengiriman tpeng',$this->t.'.idTransaksiPengiriman = tpeng.id','inner'],
            ['join','useralamat ua','tpemb.idMember = ua.idUser','inner'],
        ];

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            $data[] = array(
                $dt->nama,
                $dt->judul,
                $dt->quantity,
                $dt->catatan,
                $dt->ketAlamat,
            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

}

/* End of file MPesanan.php */
