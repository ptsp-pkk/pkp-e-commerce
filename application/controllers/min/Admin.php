<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    private $path = 'admin/_main'; 
    private $path_incl = 'admin/page/'; 

    public function dashboard()
	{
		$data['title'] = 'Dashboard - PKK E-commerce';
		$data['link_view'] = $this->path_incl.'dashboard'; #ubah file view disini
		$this->load->view($this->path,$data);
	}

	public function produk()
	{
		$data['title'] = 'Produk - PKK E-commerce';
		$data['link_view'] = $this->path_incl.'produk'; #ubah file view disini
		$this->load->view($this->path,$data);
	}

	public function add_produk()
	{
		$data['title'] = 'Produk - PKK E-commerce';
		$data['link_view'] = $this->path_incl.'tambah_produk'; #ubah file view disini
		$this->load->view($this->path,$data);
	}

	public function daftar_psnan()
	{
		$data['title'] = 'Daftar Pesanan - PKK E-commerce';
		$data['link_view'] = $this->path_incl.'pesanan'; #ubah file view disini
		$this->load->view($this->path,$data);
	}

	public function pesan()
	{
		$data['title'] = 'Pesan - PKK E-commerce';
		$data['link_view'] = $this->path_incl.'pesan'; #ubah file view disini
		$this->load->view($this->path,$data);
	}

	public function diskusi()
	{
		$data['title'] = 'Diskusi Produk - PKK E-commerce';
		$data['link_view'] = $this->path_incl.'diskusi'; #ubah file view disini
		$this->load->view($this->path,$data);
	}

	public function profilstore()
	{
		$data['title'] = 'Profil Toko Produk - PKK E-commerce';
		$data['link_view'] = $this->path_incl.'profilstore'; #ubah file view disini
		$this->load->view($this->path,$data);
	}

}

/* End of file Admin.php */
