<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    public function Action()
    {
        $nohp = $this->input->post('nohp');
        $cekhp = $this->db->get_where('user',array('noHp'=>$nohp));
        if ($cekhp->num_rows() > 0 ) {
            $json = array('success' => false, 'msg'=>'Nomor Handphone Sudah Terdaftar');
        }else{
            $query = $this->db->insert('user', array(
                'username'=>$this->input->post('nama'),
                'nama'=> $this->input->post('nama'),
                'password'=> $this->input->post('password'),
                'noHp'=> $nohp,
                'createdDate'=>date('Y-m-d H:i:s'),
            ));
            if ($query == TRUE) {
                $cek = $this->db->get_where('user',array('noHp' => $nohp));
                if ($cek->num_rows() > 0 ) {
                    $hp = '62'.substr($this->input->post('hp'),1);
                    $this->load->library('nexmo');
                    $this->nexmo->set_format('json');
                    $to = $hp;
                    $brand = 'PKK MART';
                    $sender_id = 'PKK MART'; //optional
                    $code_length = '6'; //optional 4 or 6
                    $lg = 'en-in'; //optional nexmo default en-us https://docs.nexmo.com/index.php/voice-api/text-to-speech#languages
                    $require_type = null; //optional enabled on request by nexmo
                    $pin_expiry = 300;
                    $response = $this->nexmo->verify_request($to, $brand, $sender_id, $code_length, $lg, $require_type, $pin_expiry);
                    $json = array('success'=>true,'msg'=>'Kode OTP sudah di kirim','nohp'=>$nohp,'req'=>@$response['request_id']);
                }
            }else{
                $josn = array('success' => false, 'msg'=>'Gagal Register');
            }
        }        
        echo json_encode($json);
    }

}

/* End of file Register.php */
/* Location: ./application/controllers/min/Register.php */