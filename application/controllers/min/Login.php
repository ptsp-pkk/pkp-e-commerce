<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    private $path = 'admin/login'; 

    public function index()
    {
      $data['title'] = 'Login - Admin PKK E-commerce';
      $this->load->view($this->path,$data);
  }
  public function auth()
  {
    $nohp = $this->input->post('hp');
    $cek = $this->db->get_where('user',array('noHp' => $nohp));
    if ($cek->num_rows() > 0 ) {
        $hp = '62'.substr($this->input->post('hp'),1);
            // return var_dump($hp);
        $this->load->library('nexmo');
        $this->nexmo->set_format('json');
        $to = $hp;
        $brand = 'PKK MART';
            $sender_id = 'PKK MART'; //optional
            $code_length = '6'; //optional 4 or 6
            $lg = 'en-in'; //optional nexmo default en-us https://docs.nexmo.com/index.php/voice-api/text-to-speech#languages
            $require_type = null; //optional enabled on request by nexmo
            $pin_expiry = 300;
            $response = $this->nexmo->verify_request($to, $brand, $sender_id, $code_length, $lg, $require_type, $pin_expiry);
            // return var_dump($response['status']);
            $val = array('success'=>true,'msg'=>'Kode OTP sudah di kirim','nohp'=>$nohp,'req'=>$response['request_id']);
        }else{
            $val = array('success'=>false,'msg'=>'Tidak ada Nomer Handphone yang terdaftar');
        }
        echo json_encode($val);
    }
    public function checkkode()
    {
        $hp = $this->input->post('hp');
        $code = $this->input->post('code');
        $request_id = $this->input->post('request');
        $response = $this->nexmo->verify_check($request_id, $code);
        $row = $this->db->get_where('user',array('noHp'=>$hp))->row();
        // return var_dump($response['status']);
        if($response['status'] == '16'){
           $val = array('success'=>false,'msg'=>'Kode Salah');
        }else{
            $session = array('id' => $row->id,'nohp'=>$row->noHp);
            $this->session->set_userdata($session);
            $val = array('success'=>true,'msg'=>'success');
        }
        echo json_encode($val);
    }
    public function auth_admin()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        
        $status = 0;
        $log = [];
        $data = [];

        if ($email == '' && $password == '') {
            $msg = 'Mohon untuk memasukan email dan password anda';
        }elseif ($email == '') {
            $msg = 'Mohon untuk memasukan email anda';
        }elseif ($password == '') {
            $msg = 'Mohon untuk memasukan password anda';
        }else{
            $this->load->model('Auth');
            
            // Daftarin ke model auth
            $this->Auth->email = $email;
            $this->Auth->password = $password;

            $auth = $this->Auth->auth_admin();
            
            $msg = $auth['msg'];
            $status = $auth['status'];
            $data = $auth['data'];
        }

        $log = [
            'msg' => $msg,
            'status' => $status,
            'data' => $data,
            'date' => date('Y-m-d H:i:s')
        ];

        echo json_encode($log);
    }

    public function logout() {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('nohp');
        $this->session->unset_userdata('nama');

        redirect(base_url());
    }
}

/* End of file Admin.php */
