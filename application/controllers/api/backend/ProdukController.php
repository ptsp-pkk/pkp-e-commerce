<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProdukController extends CI_Controller {
    
    private $status = 0;
    
    public function __construct()
    {
        parent::__construct();

        // if ($this->session->userdata('id') == '') {
        //     redirect( log_message('error', 'Some variable did not contain a value.'));
        // }

        $this->load->model('Produk','b');        
        $this->load->model('GambarProduk','gp');        
    }

    public function dt()
    {
        echo $this->b->dt();
    }
    
    public function get($id='',$api=1)
    {
        if ($id != '') {
            $q = $this->b->se($id);
        }else{
            $q = $this->b->se();
        }
        
        if ($api == 1) {
            echo json_encode($q->result());
        }else{
            return $q;
        }
    }
    
    public function in(){
        //POST
        $idKategori = $this->input->post('kategori');
        $idSubkategori = $this->input->post('subkategori');
        $idMember = $this->session->userdata('id') == '' ? 1 : $this->session->userdata('id');
        $idStore = $this->session->userdata('idStore') == '' ? 1 : $this->session->userdata('idStore');
        $judul = $this->input->post('produk');
        $harga = $this->input->post('harga');
        $keterangan = $this->input->post('keterangan');
        $kondisi = $this->input->post('kondisi');
        $minPembelian = $this->input->post('minPembelian');
        $beratProduk = $this->input->post('berat');
        $idMerek = $this->input->post('merek');
        $status = 1;
        $uploadDate = date('Y-m-d H:i:s');
        $sku = $this->input->post('sku');
        $sku = $this->input->post('tag');
        $stok = $this->input->post('stok');
        
        $obj = [
                'idKategori' => $idKategori,
                'idSubkategori' => $idSubkategori,
                'idMember' => $idMember,
                'idStore' => $idStore,
                'judul' => $judul,
                'harga' => $harga,
                'keterangan' => $keterangan,
                'kondisi' => $kondisi,
                'minPembelian' => $minPembelian,
                'beratProduk' => $beratProduk,
                'idMerek' => $idMerek,
                'status' => $status,
                'uploadDate' => $uploadDate,
                'sku' => $sku,
                'stok' => $stok,
            ];
        
            
        $log = [
            'msg' => "Gagal Tambah Data",
            'obj' => $obj,
            'status' => 0
        ]; 
                
        $this->b->in($obj);
        $id = $this->db->insert_id();
        
        $cek = $this->get($id,0);
        if ($cek->num_rows() > 0) {
            // Upload Gambar
            $upload = $this->upload();
            if (count($upload) > 0) {
                foreach ($upload as $v) {
                    $gambar = [
                        'idProduk' => $id,
                        'fileName' => $v,
                        'statusGambar' => 1,
                        'uploadDate' => date('Y-m-d H:i:s')

                    ];
                    $this->gp->in($gambar);    
                }
            }

            $log = [
                'msg' => "Berhasil Tambah Data",
                'obj' => $obj
            ];   
        }        
        
        echo json_encode($log);
        
    }
    
    public function up(){
        $id = $this->input->post('id');
        //POST
        // $id = $this->input->post('id');
        // $idKategori = $this->input->post('idKategori');
        // $idSubkategori = $this->input->post('idSubkategori');
        // $idMember = $this->input->post('idMember');
        // $idStore = $this->input->post('idStore');
        // $judul = $this->input->post('judul');
        // $harga = $this->input->post('harga');
        // $keterangan = $this->input->post('keterangan');
        // $kondisi = $this->input->post('kondisi');
        // $minPembelian = $this->input->post('minPembelian');
        // $beratProduk = $this->input->post('beratProduk');
        // $idMerek = $this->input->post('idMerek');
        // $status = $this->input->post('status');
        // $uploadDate = $this->input->post('uploadDate');
        // $sku = $this->input->post('sku');
        
        // $obj = [
        //         'id' => $id,
        //         'idKategori' => $idKategori,
        //         'idSubkategori' => $idSubkategori,
        //         'idMember' => $idMember,
        //         'idStore' => $idStore,
        //         'judul' => $judul,
        //         'harga' => $harga,
        //         'keterangan' => $keterangan,
        //         'kondisi' => $kondisi,
        //         'minPembelian' => $minPembelian,
        //         'beratProduk' => $beratProduk,
        //         'idMerek' => $idMerek,
        //         'status' => $status,
        //         'uploadDate' => $uploadDate,
        //         'sku' => $sku,
        //     ];
        
        // $where = ['id' => $id ];
        
        
        // $q = $this->b->up($obj,$where);
        // if ($q) {
        //     $msg = "Berhasil Ubah Data";
        //     $this->status = 1;
        // }
        
        
        // $log = [
        //     'msg' => $msg,
        //     'obj' => $obj,
        //     'status' => $this->status
        // ];   
        
        // echo json_encode($log);
    }
    
    public function de(){
        
        $id = $this->input->post('id');
        
        $where = [
            'id' => $id        ];
        
        $log = [
            'msg' => "Gagal Hapus Data",
            'obj' => $where,
            'status' => 0
        ];   
        
        
        $cek = $this->get($id,0);
        
        if ($cek->num_rows() > 0) {
            $q = $this->b->de($where);
            if ($q) {
                $cek = $this->get($id,0);
                if ($cek->num_rows() < 1) {
                    $log = [
                        'msg' => "Berhasil Hapus Data",
                        'obj' => $where,
                        'status' => 1
                    ];   
                } 
            }
        }else{
            $log = [
                'msg' => "Data yg ingin dihapus tidak ada",
                'obj' => $where,
                'status' => 0
            ];
        }
        
        echo json_encode($log);
        
    }
    
    public function setActiveStatus($id='')
    {
        $obj = [
            'status' => 1,
        ];
        
        $where = [
            'id' => $id,
        ];
        
        $q = $this->b->up($obj,$where);
        
        $log = [
            'msg' => "Aktifkan data sukses",
            'obj' => $obj,
            'status' => 1
        ];
        
        echo $log;
    }
    
    public function setNonActiveStatus($id='')
    {
        $obj = [
            'status' => 0,
        ];
        
        $where = [
            'id' => $id,
        ];
        
        $q = $this->b->up($obj,$where);
        
        $log = [
            'msg' => "Non Aktifkan data sukses",
            'obj' => $obj,
            'status' => 1
        ];
        
        echo $log;
    }

    private function upload($namaFile='pro-image',$path='./data/produk',$allow='jpg|png|PNG')
    {
        $data = [];

        $this->load->library('upload');
        $number_of_files_uploaded = count($_FILES[$namaFile]['name']);
        // Faking upload calls to $_FILE
        for ($i = 0; $i < $number_of_files_uploaded; $i++) :
          $_FILES['userfile']['name']     = $_FILES[$namaFile]['name'][$i];
          $_FILES['userfile']['type']     = $_FILES[$namaFile]['type'][$i];
          $_FILES['userfile']['tmp_name'] = $_FILES[$namaFile]['tmp_name'][$i];
          $_FILES['userfile']['error']    = $_FILES[$namaFile]['error'][$i];
          $_FILES['userfile']['size']     = $_FILES[$namaFile]['size'][$i];
          $config = array(
            'file_name'     => $_FILES[$namaFile]['name'][$i],
            'allowed_types' => $allow,
            
            /* real path to upload folder ALWAYS */
            'upload_path' => $path
          );
          $this->upload->initialize($config);
          if ( ! $this->upload->do_upload()) :
            $error = array('error' => $this->upload->display_errors());
          else :
            $final_files_data[] = $this->upload->data();
            array_push($data,$_FILES[$namaFile]['name'][$i]);
            // Continue processing the uploaded data
          endif;
        endfor;

        return $data;
    }
    
}

/* End of file Produk.php */
