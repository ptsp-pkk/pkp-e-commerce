<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan extends CI_Controller {
    
    private $status = 0;
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('MPesanan','mp');        
    }
    
    public function dt()
    {
        echo $this->mp->dt();
    }

    public function get($id='',$api=1)
    {
        if ($id != '') {
            $q = $this->b->se($id);
        }else{
            $q = $this->b->se();
        }
        
        if ($api == 1) {
            echo json_encode($q->result());
        }else{
            return $q;
        }
    }
    
    public function in(){
        //POST
                $id = $this->input->post('id');
                $idTransaksiPembelian = $this->input->post('idTransaksiPembelian');
                $idTransaksiPengiriman = $this->input->post('idTransaksiPengiriman');
                $idProduk = $this->input->post('idProduk');
                $quantity = $this->input->post('quantity');
                $hargaBerlaku = $this->input->post('hargaBerlaku');
                $idDiskonBerlaku = $this->input->post('idDiskonBerlaku');
                $createdDate = $this->input->post('createdDate');
        
        $obj = [
                        'id' => $id,
                        'idTransaksiPembelian' => $idTransaksiPembelian,
                        'idTransaksiPengiriman' => $idTransaksiPengiriman,
                        'idProduk' => $idProduk,
                        'quantity' => $quantity,
                        'hargaBerlaku' => $hargaBerlaku,
                        'idDiskonBerlaku' => $idDiskonBerlaku,
                        'createdDate' => $createdDate,
                    ];
        
        $log = [
            'msg' => "Gagal Tambah Data",
            'obj' => $obj,
            'status' => 0
        ];   
        
        $this->b->in($obj);
        $id = $this->db->insert_id();
        
        $cek = $this->get($id,0);
        if ($cek->num_rows() > 0) {
            $log = [
                'msg' => "Berhasil Tambah Data",
                'obj' => $obj
            ];   
        }        
        
        echo json_encode($log);
        
    }
    
    public function up(){
        $id = $this->input->post('id');
        //POST
                $id = $this->input->post('id');
                $idTransaksiPembelian = $this->input->post('idTransaksiPembelian');
                $idTransaksiPengiriman = $this->input->post('idTransaksiPengiriman');
                $idProduk = $this->input->post('idProduk');
                $quantity = $this->input->post('quantity');
                $hargaBerlaku = $this->input->post('hargaBerlaku');
                $idDiskonBerlaku = $this->input->post('idDiskonBerlaku');
                $createdDate = $this->input->post('createdDate');
        
        $obj = [
                        'id' => $id,
                        'idTransaksiPembelian' => $idTransaksiPembelian,
                        'idTransaksiPengiriman' => $idTransaksiPengiriman,
                        'idProduk' => $idProduk,
                        'quantity' => $quantity,
                        'hargaBerlaku' => $hargaBerlaku,
                        'idDiskonBerlaku' => $idDiskonBerlaku,
                        'createdDate' => $createdDate,
                    ];
        
        $where = [
            'id' => $id        ];
        
        
        $q = $this->b->up($obj,$where);
        if ($q) {
            $msg = "Berhasil Ubah Data";
            $this->status = 1;
        }
        
        
        $log = [
            'msg' => $msg,
            'obj' => $obj,
            'status' => $this->status
        ];   
        
        echo json_encode($log);
    }
    
    public function de(){
        
        $id = $this->input->post('id');
        
        $where = [
            'id' => $id        ];
        
        $log = [
            'msg' => "Gagal Hapus Data",
            'obj' => $where,
            'status' => 0
        ];   
        
        
        $cek = $this->get($id,0);
        
        if ($cek->num_rows() > 0) {
            $q = $this->b->de($where);
            if ($q) {
                $cek = $this->get($id,0);
                if ($cek->num_rows() < 1) {
                    $log = [
                        'msg' => "Berhasil Hapus Data",
                        'obj' => $where,
                        'status' => 1
                    ];   
                } 
            }
        }else{
            $log = [
                'msg' => "Data yg ingin dihapus tidak ada",
                'obj' => $where,
                'status' => 0
            ];
        }
        
        echo json_encode($log);
        
    }
    
    public function setActiveStatus($id='')
    {
        $obj = [
            'status' => 1,
        ];
        
        $where = [
            'id' => $id,
        ];
        
        $q = $this->b->up($obj,$where);
        
        $log = [
            'msg' => "Aktifkan data sukses",
            'obj' => $obj,
            'status' => 1
        ];
        
        echo $log;
    }
    
    public function setNonActiveStatus($id='')
    {
        $obj = [
            'status' => 0,
        ];
        
        $where = [
            'id' => $id,
        ];
        
        $q = $this->b->up($obj,$where);
        
        $log = [
            'msg' => "Non Aktifkan data sukses",
            'obj' => $obj,
            'status' => 1
        ];
        
        echo $log;
    }
    
}

/* End of file Listpembelian.php */
