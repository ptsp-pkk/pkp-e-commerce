<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Make extends CI_Controller {
    
    private $path = 'labs/sempel/';
    
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('file');
    }
    
    public function models($nama='',$id='')
    {
        $data = [
            'nama' => $nama,
            'id'=> $id
        ];
        
        $ok = $this->load->view($this->path.'model',$data,TRUE);
        $data =  str_replace('%php','<?php',$ok);
        //    echo copy(APPPATH.'views/labs/model.php',APPPATH.'models/Ok.php');
        // $data = 'Some file data';
        if ( ! write_file(APPPATH.'models/'.ucfirst($nama).'.php', $data))
        {
            echo 'Gagal membuat model '.$nama;
        }
        else
        {
            echo 'Berhasil buat model';
            chmod(APPPATH.'models/'.ucfirst($nama).'.php',0777);
        }
    }
    
    public function controllers($nama='',$id='')
    {
        $fields = $this->db->list_fields($nama);
        $data = [
            'nama' => $nama,
            'id'=> htmlentities($id),
            'fields' => $fields
        ];
        
        $ok = $this->load->view($this->path.'controller',$data,TRUE);
        $data =  str_replace('%php','<?php',$ok);
        //    echo copy(APPPATH.'views/labs/model.php',APPPATH.'models/Ok.php');
        // $data = 'Some file data';

        // echo $data;
        if ( ! write_file(APPPATH.'controllers/api/backend/'.ucfirst($nama).'Controller.php', $data))
        {
            echo 'Gagal membuat controller '.$nama;
        }
        else
        {
            echo 'Berhasil buat controller';
            chmod(APPPATH.'controllers/api/backend/'.ucfirst($nama).'Controller.php',0777);
        }
    }
    
    public function views($nama='',$id='')
    {
        $data = [
            'nama' => $nama,
            'id'=> $id
        ];
        
        $ok = $this->load->view('labs/controllers',$data,TRUE);
        $data =  str_replace('%php','<?php',$ok);
        //    echo copy(APPPATH.'views/labs/model.php',APPPATH.'models/Ok.php');
        // $data = 'Some file data';
        if ( ! write_file(APPPATH.'controllers/File.php', $data))
        {
            echo 'Gagal membuat views '.$nama;
        }
        else
        {
            echo 'Gagal buat views';
        }
    }
    
}

/* End of file Make.php */
