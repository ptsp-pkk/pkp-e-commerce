<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Db extends CI_Controller {
    
    private $path = 'labs/';
    private $nmaID = '';
    
    public function index()
    {
        $this->load->library('api');
        $this->load->model('Produk','p');
        
        $produk  = $this->p;
        $produk->see = 'id';
        echo $this->api->conv($produk->se('1'),1);
    }

    public function listTabel()
    {
        $data['tables'] = $this->db->list_tables();
        
        $this->load->view($this->path.'/list_tabel', $data, FALSE);
    }
    
    public function getTabel(string $tabel = '')
    {
        if ($this->db->table_exists($tabel))
        {
            $fields = $this->db->list_fields($tabel);
            $msg = '';
            $msg .= '[ ';
            foreach ($fields as $field)
            {
                $msg .= '"'.$field.'" => $this->input->post("'.$field.'"),<br>';
            }
            $msg .= ']';

            $data = [
                'id' => $fields[0],
                'component' => $msg
            ];

            $this->load->view($this->path.'/get_tabel', $data, FALSE);
        }else{
            echo "Tabel tidak tersedia";
        }
    }
    
}
/* End of file Db.php */

?>