<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	public function index()
	{
		$data['title'] = 'Selamat Datang Pelanggan - PKK E-commerce';
		$data['link_view'] = 'page/main/main'; #lokasi file main
        $data['halaman'] = base_url(); #lokasi file main
        $data['category_product_limit'] = $this->db->limit(2)->order_by('id','random')->get('kategori')->result();
        $data['slider']                = $this->front->get_data('slide',array('kategori'=>1,'status'=>1));
        $data['swipeSlider']                = $this->front->get_data('slide',array('kategori'=>2,'status'=>1));
        
		$this->load->view('_main',$data);
	}
	
	public function register()
	{
		$data['title'] = 'Halaman Register - PKK E-commerce';
		$data['link_view'] = 'page/users/register';
		$this->load->view('_main',$data);
	}

//	NUMPANG TEST VIEW
    public function product_detail() {
        $seo                       = $this->uri->segment(2);
        $product                   = $this->front->productDetail($seo);
        if(!$product) {
        redirect(base_url(),'refresh');
        }
        $rating                       = $this->front->sum_data('rating','review',array('idProduk'=>$product->id)); 
        $count_rating                 = $this->front->count_data('review',array('idProduk'=>$product->id));
        if($rating>0 && $count_rating>0){
        $data['rating']               = $rating;
        $data['count_rating']         = $count_rating;
        $data['persen_rating']        = $rating/(5*$count_rating)*100;   
        $data['harga_diskon']         = $product->harga-($product->harga*($product->persenDiskon/100));
        }
        $data['image']                = $this->front->get_data('gambarproduk',array('idProduk'=>$product->id,'statusGambar'=>1));
        $data['review']               = $this->front->reviewProduct($product->id);
        $data['review_count']         = $this->front->count_data('review',array('idProduk'=>$product->id)); 
        $data['productRelated_count'] = $this->front->count_data('produk',array('idKategori'=>$product->idKategori,'status'=>1)); 
        $data['productDetail']        = $product;
        $data['title']                = $product->judul.'Product Page - PKK E-commerce';
        $data['link_view']            = 'page/detail_produk/detail_produk'; #lokasi file main
        $this->load->view('_main',$data);
    }

    public function pay() {
        // if(empty($this->session->userdata('nohp'))){
        //     redirect(base_url());
        // }
        $id                        = $this->uri->segment(2);
        $pay                   = $this->front->get_data_row('transaksi',array('nomorTransaksi'=>$id));
        if(!$pay) {
        redirect(base_url(),'refresh');
        }
        $data['pay'] = $pay;
        $data['title'] = 'Pembayaran Untuk Nomor Transaksi '.$pay->nomorTransaksi.' - PKK E-commerce';
        $data['link_view'] = 'page/pay/pay'; #lokasi file main
        $this->load->view('_main',$data);
    }
    public function store_page() {
        $data['title'] = 'Toko Andalan Jaya - PKK E-commerce';
        $data['link_view'] = 'page/store_page/store_page'; #lokasi file main
        // $data['halaman'] = base_url().'cart'; #lokasi file main
        $this->load->view('_main',$data);
    }
	public function cart() {
        $data['title'] = 'Cart Page - PKK E-commerce';
        $data['link_view'] = 'page/cart/cart'; #lokasi file main
        $data['halaman'] = base_url().'cart'; #lokasi file main
        $this->load->view('_main',$data);
    }
	public function checkout() {
        $data['title'] = 'Checkout Page - PKK E-commerce';
        $data['link_view'] = 'page/checkout/checkout'; #lokasi file main
        $data['user_alamat'] = $this->front->checkoutUser($this->session->userdata('id'));
        $data['kodeUnik'] = mt_rand(100, 999);
        $this->load->view('_main',$data);
    }
	public function wishlist() {
        $data['title'] = 'Wishlist Page - PKK E-commerce';
        $data['link_view'] = 'page/wishlist/wishlist'; #lokasi file main
        $this->load->view('_main',$data);
    }
    public function account() {
        $data['title'] = 'Account Page - PKK E-commerce';
        $data['link_view'] = 'page/users/account'; #lokasi file main
        $this->load->view('_main',$data);
    }
	public function search() {

        $this->load->library('pagination');
        
        // Deklarasi Variabel
        $param = '';
        $key = $this->input->get('keyword');

        if ($key != '') {
            $param = "?keyword=".$key;
        }

        //konfigurasi pagination
        $config['base_url'] = site_url('search'.$param); //site url
        $config['total_rows'] = $this->front->product_like($key)->num_rows(); //total row
        $config['per_page'] = 8;  //show record per halaman
        $config["uri_segment"] = 2;  // uri parameter
        $config["num_links"] = $config["total_rows"];
        $config['page_query_string'] = true;
 
        // Membuat Style pagination untuk BootStrap v4
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
 
        $data['page'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;


        $data = [
            'title' =>'Pencarian "'.$key.'" - PKK E-commerce',
            'link_view' => 'page/search_result/search_result',
            'produk' => $this->front->product_newHome($config["per_page"], $data['page'],$key),
            'key' => $key,
        ];
        $this->pagination->initialize($config);
        $this->load->view('_main',$data);
    }
	// public function pay() {
 //        $data['title'] = 'Pembayaran Untuk Transaksi Anda - PKK E-commerce';
 //        $data['link_view'] = 'page/pay/pay'; #lokasi file main
 //        $this->load->view('_main',$data);
 //    }
	public function order_list() {
        $data['title'] = 'Daftar Transaksi Anda - PKK E-commerce';
        $data['link_view'] = 'page/order_list/order_list'; #lokasi file main
        $this->load->view('_main',$data);
    }
	public function invoice() {
        $data['title'] = 'Detail Transaksi Anda - PKK E-commerce';
        $data['link_view'] = 'page/invoice/invoice'; #lokasi file main
        $this->load->view('_main',$data);
    }
	public function post_review() {
        $data['title'] = 'Beri Review Untuk Produk Ini - PKK E-commerce';
        $data['link_view'] = 'page/post_review/post_review'; #lokasi file main
        $this->load->view('_main',$data);
    }
	public function help_center() {
        $data['title'] = 'Pusat Bantuan Pengguna - PKK E-commerce';
        $data['link_view'] = 'page/help_center/help_center'; #lokasi file main
        $this->load->view('_main',$data);
    }
	public function qna() {
        $data['title'] = 'Pertanyaan dan Jawaban - PKK E-commerce';
        $data['link_view'] = 'page/help_center/qna'; #lokasi file main
        $this->load->view('_main',$data);
    }
}
