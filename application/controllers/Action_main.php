<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Action_main extends CI_Controller {

	public function addCart()
    {
        $data_produk= array('id' 	=> $this->input->post('id'),
							'name'  => $this->input->post('nama'),
							'price' => $this->input->post('harga'),
							'img'   => $this->input->post('gambar'),
							'slug'  => $this->input->post('slug'),
							'qty'   => $this->input->post('qty')
                            );
        $this->cart->insert($data_produk);
        redirect($this->input->post('redirectto'));
    }

    public function deleteCart($rowid) 
    {
        if ($rowid=="all")
            {
                $this->cart->destroy();
            }
        else
            {
                $data = array('rowid' => $rowid,
                              'qty' =>0
                            );
                $this->cart->update($data);
            }
        redirect('cart');
    }

    public function updateCart()
    {
        $cart_info = $_POST['cart'] ;
        foreach( $cart_info as $id => $cart)
        {
			$rowid  = $cart['rowid'];
			$price  = $cart['price'];
			$img    = $cart['img'];
			$slug   = $cart['slug'];
			$amount = $price * $cart['qty'];
			$qty    = $cart['qty'];
			$data   = array('rowid'  => $rowid,
							'price'  => $price,
							'img'    => $img,
							'slug'   => $slug,
							'amount' => $amount,
							'qty'    => $qty);
            $this->cart->update($data);
        }
        redirect('main/cart');
    }

    public function toCheckout()
    {
        $nomortransaksi=mt_rand(000001, 999999);
        //-------------------------Input data transaksi--------------------------
        $dataTransaksi = array('nama' => $this->input->post('nama'),
                            'nomorTransaksi' => 'PKK'.$nomortransaksi,
                            'tanggalTransaksi' => date('Y-m-d'),
                            'noHP' => $this->input->post('noHP'),
                            'alamatTujuan' => $this->input->post('alamatTujuan'),
                            'totalTransfer' => $this->input->post('totalTransfer'));
        $idTransaksi = $this->front->addDatatoTable('transaksi',$dataTransaksi);
        //-------------------------Input data detail transaksi-----------------------       
        if ($cart = $this->cart->contents())
            {
                foreach ($cart as $item)
                    {
                        $dataDetail = array('idTransaksi' =>$idTransaksi,
                                        'idProduk' => $item['id'],
                                        'qty' => $item['qty'],
                                        'hargaSatuan' => $item['price'],            
                                        'hargaTotal' => $item['price']*$item['qty']);            
                        $proses = $this->front->addDatatoTable('detailTransaksi',$dataDetail);
                    }
            }
        //-------------------------Hapus shopping cart--------------------------        
        $this->cart->destroy();
        redirect(base_url('pay/PKK').$nomortransaksi);
    }

    public function do_upload()
    {
        $id=$this->input->post('id_transaksi');
        $config['upload_path']          = './assets/img/bukti_transfer/';
        $config['allowed_types']        = 'jpeg|jpg|png';
        $config['max_size']             = 1024;
        $config['file_name']            = 'bukti_transfer_'.$id;

        
        $this->load->library('upload', $config);
        $this->upload->do_upload();
        $image = $this->upload->data();
        $data = array(
        'buktiTransfer'=>$image['file_name'],
        'uploadDateTF'=>date('Y-m-d'),
        'status'=>0
        );
        $this->db->where('nomorTransaksi', $id);
        $this->db->update('transaksi', $data);
        
        redirect(base_url('pay/').$id);
    }


}

/* End of file Action_main.php */
/* Location: ./application/controllers/Action_main.php */

