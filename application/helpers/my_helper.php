<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function get_data($table,$array_parameter,$data)
{
    $ci= & get_instance();
    $cek=$ci->db->get_where($table,$array_parameter)->row_array();
    return $cek[$data];
}
function count_data($table,$array_parameter)
{
    $ci= & get_instance();
    $cek=$ci->db->get_where($table,$array_parameter);
    return $cek->num_rows();
}
function get_data_kategori_produk($data,$id)
{
    $ci= & get_instance();
    $cek=$ci->db->limit(4)->order_by('title','random')->get_where('produk',array('idKategori'=>$data,'idProduk != '.$id));
    return $cek->result();
}